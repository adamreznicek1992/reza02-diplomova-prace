;(function ($) {

	'use strict';

	$('.td--toggle input').on('change', function(e) {
		var enabled = !$(this).parent().hasClass('off');
		
		let params = {
			"id": $(this).attr('data-dish-id'),
			"enabled": enabled
		};

		// console.log(params);

		$('body').addClass('waiting-for-load'); // ajax pending

		// ajax
		let jqXHR = $.post('/admin/recepty/status-aktivity', params)
			.done(function(data) {
				if (data.status == 'ok') {
					console.log('Status aktivity byl nastaven')
				}
				else {
					console.log('Status aktivity nebyl nastaven, jídlo nebylo nalezeno.')
				}

				$('body').removeClass('waiting-for-load'); // ajax pending
			})
			.fail( function(xhr, textStatus, errorThrown) {
				console.log(xhr);
				$('body').removeClass('waiting-for-load'); // ajax pending
    	});

	});
})(jQuery);