;(function ($) {

	'use strict';

	// only if generator page
	if ($('body.page--generator, body.page--generator-edit').length) {
		var $checker = $('#checker');
		var ageGroup = parseInt($checker.attr('data-agegroup'));
		var $checkerBasket = $checker.find('.generator__checker-basket');
		var $checkerNutri = $checker.find('.generator__checker-nutri');
		
		var $activeSelectionBtn = null;

		// hide recommendations if not necessary
		var $allRecommendations = $checker.find('.recommendations');
		for (var i = 0; i < $allRecommendations.length; i++) {
			updateCheckerItem($allRecommendations[i], 0);
		}
		
		// check errors on laod
		function calcErrors() {
			errors = $checker.find('.recommendations:not(".d-none")').length;
			console.log("Počet chyb: " + errors);

			var $listOfErrors = $('.generator__errors');
			$listOfErrors.each(function (index) {
				if ($(this).find('.recommendations').length == $(this).find('.recommendations.d-none').length) {
					$(this).removeClass('active');
				}
				else {
					$(this).addClass('active');
				}
			});
		}
		calcErrors();

		$('.gSave').on('click', function(e) {

			var errorText = '';

			if (errors) {
				errorText = '\n\nNedostatky jídelníčku:';
				var $errors = $checker.find('.recommendations:not(".d-none")');
				for (var i = 0; i < $errors.length; i++) {
					errorText += ('\n' + $($errors[i]).attr('data-type') + ': ' + $($errors[i]).text().replace('\n', ''));
				}
			}

			if (confirm("Přejete si sestavený jídelníček uložit?" +  errorText)) {

				let params = {
					"menu": JSON.stringify(selectedDishes)
				};

				// ajax
				$('body').addClass('waiting-for-load');
				let jqXHR = $.post('/admin/sestaveni-jidelnicku/save-menu/', params)
					.done(function(data) {
						console.log(data);

						// if success
						if (data.code == 200) {
							// redirect
							location = '/admin/archiv';
						}

					$('body').removeClass('waiting-for-load');
				})
				.fail( function(xhr, textStatus, errorThrown) {
					console.log(xhr);
					$('body').removeClass('waiting-for-load');
				});
			}
			else {
				e.preventDefault();
			}

		});

		$('.gSaveEdited').on('click', function(e) {
			if (confirm("Přejete si změny v jídelníčku uložit?")) {

				// console.log(menuIndexInSet);
				// console.log(selectedDishes);

				let params = {
					"menu": JSON.stringify(selectedDishes['week' + menuIndexInSet]),
					"id": menuIdInDB,
					"ageGroup": ageGroup,
				};

				// console.log(JSON.stringify(selectedDishes));

				// ajax
				$('body').addClass('waiting-for-load');
				let jqXHR = $.post('/admin/sestaveni-jidelnicku/edit-menu/', params)
					.done(function(data) {
						console.log(data);

						// if success
						if (data.code == 200) {
							// redirect
							location = '/admin/archiv';
						}

					$('body').removeClass('waiting-for-load');
				})
				.fail( function(xhr, textStatus, errorThrown) {
					console.log(xhr);
					$('body').removeClass('waiting-for-load');
				});
			}
			else {
				e.preventDefault();
			}

		});

		$('.editDish').on('click', function(e) {
			$activeSelectionBtn = $(this);
		});

		$('.modal-submit').on('click', function(e) {
			var $modal = $(this).closest('.modal');
			var $select = $modal.find('select.dishes');
			var values = (Array.isArray($select.val()) ? $select.val() : [$select.val()]);
			// console.log(values);

			var week = $activeSelectionBtn.closest('.week').attr('data-week');
			var day = $activeSelectionBtn.closest('.day').attr('data-day');
			var type = $activeSelectionBtn.attr('data-type');

			// console.log(week);
			// console.log(day);
			// console.log(type);

			// get dish and remove if have to update
			var actualDishId = selectedDishes[week]['days'][day][type].id;
			if (actualDishId && actualDishId.length == 1) {
				updateCheckerAjax(actualDishId[0], -1);
			} 

			// save id to json
			selectedDishes[week]['days'][day][type].id = (Array.isArray(values) ? values : [values]);
			// console.log(selectedDishes);

			// show selected dish name
			var $selectedOptions = $select.find('option:selected');
			var text = '';
			for (var i = 0; i < $selectedOptions.length; i++) {
				if (i == $selectedOptions.length - 1) {
					text += $($selectedOptions[i]).text();
				}
				else {
					text += $($selectedOptions[i]).text() + ', ';
				}
			}

			// edited dish element
			var $dish = $('.table--generator').find('.day[data-day="' + day + '"] .week[data-week="' + week + '"] .dish[data-type="' + type + '"]');

			// update mobile and desktop version
			$dish.text(text);

			// colored after edit
			$dish.closest('.dishRow').addClass('edited');

			// save date of the day
			var date = selectedDishes[week]['days'][day]['date'];

			// check dish
			// ajax
			if ($selectedOptions.length == 1) {
				updateCheckerAjax(values[0], 1, $dish, date);
			}

			// close modal
			$modal.find('.modal-close').click();
		});

		function updateCheckerAjax(id, koef, $dish = null, date = null) {
			$('body').addClass('waiting-for-load');
				let jqXHR = $.get('/admin/sestaveni-jidelnicku/dish/' + id)
					.done(function(data) {
						console.log(data);
						
						if (data.code == 200) {
							// update checker
							updateChecker(data.dish, koef);
							checkDishLastUsed($dish, data.dish.last_used_at, date);
							calcErrors();
						}
						
						$('body').removeClass('waiting-for-load');
					})
					.fail( function(xhr, textStatus, errorThrown) {
						console.log(xhr);
						$('body').removeClass('waiting-for-load');
					});
		}

		function checkDishLastUsed($dish, lastUse, dayOfUse) {

			const limitInDays = 14;

			if ($dish && lastUse && dayOfUse) {

				var lastUseOnlyDate = lastUse.split('T')[0];
				var lastUseTimestamp = new Date(lastUseOnlyDate);
				var dayOfUseTimestamp = new Date(dayOfUse);

				// calculate diff in days
				var diffInTime = dayOfUseTimestamp - lastUseTimestamp;
				var diffInDays = Math.ceil(diffInTime / (1000 * 60 * 60 * 24));
				
				// error element
				var $error = $dish.closest('.dishRow').find('.dishUsedError');
				
				// if more day then limit
				if (Math.abs(diffInDays) <= limitInDays) {
					
					const options = { weekday: 'long', year: 'numeric', month: 'long', day: 'numeric' };

					$error.text('Jídlo bylo použito dne: ' + lastUseTimestamp.toLocaleDateString('cs-CZ', options));

					$error.removeClass('d-none');
				}
				else {
					$error.text('');
					$error.addClass('d-none');
				}
			}
		}

		function updateChecker(data, koef) {

			// console.log(data);

			// basket update
			var $basketTypeEl = $checkerBasket.find('.generator__checker-type[data-type-id="' + data.type_id + '"]');
			if ($basketTypeEl.length) {
				$.each(data['basket' + ageGroup], function(index, value) {
					if (value > 0) {
						var item = $basketTypeEl.find('.recommendations[data-name="' + index + '"]');
						updateCheckerItem(item, value * koef);
					}
				});
			}

			// nutri update
			var $nutriTypeEl = $checkerNutri.find('.generator__checker-type[data-type-id="' + data.type_id + '"]');
			if ($nutriTypeEl.length) {
				var $catEl = $nutriTypeEl.find('.recommendations[data-category-id="' + data.category_id + '"]'); 
				updateCheckerItem($catEl, 1 * koef);
			}
		}

		function updateCheckerItem(item, value) {
			var $actualValue = $(item).find('.actualValue');

			var target = parseFloat($(item).attr('data-value'));
			var actualValue = parseFloat($actualValue.text());
			var restriction = $(item).attr('data-restriction');

			actualValue = actualValue + value;
			$actualValue.text(actualValue);

			switch (restriction) {
				case 'min': 
					if (actualValue < target) {
						$(item).removeClass('d-none');
					}
					else {
						$(item).addClass('d-none');
					}
					break;
				case 'max':
					if (actualValue <= target) {
						$(item).addClass('d-none');
					}
					else {
						$(item).removeClass('d-none');
					}
					break;
				default:
					if (actualValue == target) {
						$(item).addClass('d-none');
					}
					else {
						$(item).removeClass('d-none');
					}
			}
		}
	}	

})(jQuery);