;(function ($) {

	'use strict';

	var likes = {};

	// check the local storage
	if (localStorage.getItem('jidelnicekLikes')) {
		likes = JSON.parse(localStorage.getItem('jidelnicekLikes'));

		var $likes = $('.menu__like');
		for (var i = 0; i < $likes.length; i++) {
			var $el = $($likes[i]);
			if (likes[$el.attr('data-dish-id')]) {
				$el.attr('data-url', $el.attr('data-url-dislike'));
				$el.addClass('active');
			}
		}
	}

	// click to like button
	$('.like').on('click', function(e) {
		var $el = $(this);
		let url = $el.attr('data-url');
		let id = parseInt($el.attr('data-dish-id'));
		let isActive = $el.hasClass('active');

		let params = {
			"id": id
		};

		$('body').addClass('waiting-for-load'); // ajax pending
		
		// ajax
		let jqXHR = $.post(url, params)
			.done(function(data) {
				if (data.status == 'ok') {
					if (isActive) {
						delete likes[id];
						localStorage.setItem('jidelnicekLikes', JSON.stringify(likes));

						$el.attr('data-url', $el.attr('data-url-like'));
						$el.removeClass('active');
					}
					else {
						likes[id] = 1;
						localStorage.setItem('jidelnicekLikes', JSON.stringify(likes));

						$el.attr('data-url', $el.attr('data-url-dislike'));
						$el.addClass('active');
					}
				}

				$('body').removeClass('waiting-for-load'); // ajax pending
			})
			.fail( function(xhr, textStatus, errorThrown) {
				console.log(xhr);
				$('body').removeClass('waiting-for-load'); // ajax pending
    	});
	});

})(jQuery);