;(function ($) {

	'use strict';
	var $selectBg = $('#frm-backgroundForm-background'); 
	if ($selectBg.length) {
		var $images = $('#preview').find('.preview__bg');
		$images.attr('src', '/upload/' + $selectBg.val());
		
		$selectBg.on('change', function(e) {
			$images.attr('src', '/upload/' + $(this).val());
		});
	}

})(jQuery);