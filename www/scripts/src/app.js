
var $header = $('#header');
var headerHeight = $header.height();

// import components
import './components/ie-detection.js';
import './components/like.js';
import './components/generator.js';
import './components/dish.js';
import './components/preview.js';

// scripts here

// actions
$('.action--remove').on('click', function(e) {
  if (!confirm("Opravdu chcete položku odstranit?")) {
    e.preventDefault();
  }
});

$('.action--removeMenu').on('click', function(e) {
  if (!confirm("Opravdu chcete odstranit jídelníček?\nPokud ano, budou odstraněny 4 jídelníčky ze série.")) {
    e.preventDefault();
  }
});

$(function () {
  $('[data-toggle="tooltip"]').tooltip({
    offset: '0, 10',
  })
})


if ($('.generator').length) {
  // select2
  $('.select2el:not(.dishes--additions):not(.dishes--drinks)').select2({
    language: "cs"
  });
  
  $('.dishes--additions, .dishes--drinks').select2({
    language: "cs",
    maximumSelectionLength: 5,
    multiple: true,
  });
}