(function(){function r(e,n,t){function o(i,f){if(!n[i]){if(!e[i]){var c="function"==typeof require&&require;if(!f&&c)return c(i,!0);if(u)return u(i,!0);var a=new Error("Cannot find module '"+i+"'");throw a.code="MODULE_NOT_FOUND",a}var p=n[i]={exports:{}};e[i][0].call(p.exports,function(r){var n=e[i][1][r];return o(n||r)},p,p.exports,r,e,n,t)}return n[i].exports}for(var u="function"==typeof require&&require,i=0;i<t.length;i++)o(t[i]);return o}return r})()({1:[function(require,module,exports){
'use strict';

require('./components/ie-detection.js');

require('./components/like.js');

require('./components/generator.js');

require('./components/dish.js');

require('./components/preview.js');

var $header = $('#header');
var headerHeight = $header.height();

// import components


// scripts here

// actions
$('.action--remove').on('click', function (e) {
  if (!confirm("Opravdu chcete položku odstranit?")) {
    e.preventDefault();
  }
});

$('.action--removeMenu').on('click', function (e) {
  if (!confirm("Opravdu chcete odstranit jídelníček?\nPokud ano, budou odstraněny 4 jídelníčky ze série.")) {
    e.preventDefault();
  }
});

$(function () {
  $('[data-toggle="tooltip"]').tooltip({
    offset: '0, 10'
  });
});

if ($('.generator').length) {
  // select2
  $('.select2el:not(.dishes--additions):not(.dishes--drinks)').select2({
    language: "cs"
  });

  $('.dishes--additions, .dishes--drinks').select2({
    language: "cs",
    maximumSelectionLength: 5,
    multiple: true
  });
}

},{"./components/dish.js":2,"./components/generator.js":3,"./components/ie-detection.js":4,"./components/like.js":5,"./components/preview.js":6}],2:[function(require,module,exports){
'use strict';

;(function ($) {

	'use strict';

	$('.td--toggle input').on('change', function (e) {
		var enabled = !$(this).parent().hasClass('off');

		var params = {
			"id": $(this).attr('data-dish-id'),
			"enabled": enabled
		};

		// console.log(params);

		$('body').addClass('waiting-for-load'); // ajax pending

		// ajax
		var jqXHR = $.post('/admin/recepty/status-aktivity', params).done(function (data) {
			if (data.status == 'ok') {
				console.log('Status aktivity byl nastaven');
			} else {
				console.log('Status aktivity nebyl nastaven, jídlo nebylo nalezeno.');
			}

			$('body').removeClass('waiting-for-load'); // ajax pending
		}).fail(function (xhr, textStatus, errorThrown) {
			console.log(xhr);
			$('body').removeClass('waiting-for-load'); // ajax pending
		});
	});
})(jQuery);

},{}],3:[function(require,module,exports){
'use strict';

;(function ($) {

	'use strict';

	// only if generator page

	if ($('body.page--generator, body.page--generator-edit').length) {

		// check errors on laod
		var calcErrors = function calcErrors() {
			errors = $checker.find('.recommendations:not(".d-none")').length;
			console.log("Počet chyb: " + errors);

			var $listOfErrors = $('.generator__errors');
			$listOfErrors.each(function (index) {
				if ($(this).find('.recommendations').length == $(this).find('.recommendations.d-none').length) {
					$(this).removeClass('active');
				} else {
					$(this).addClass('active');
				}
			});
		};

		var updateCheckerAjax = function updateCheckerAjax(id, koef) {
			var $dish = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : null;
			var date = arguments.length > 3 && arguments[3] !== undefined ? arguments[3] : null;

			$('body').addClass('waiting-for-load');
			var jqXHR = $.get('/admin/sestaveni-jidelnicku/dish/' + id).done(function (data) {
				console.log(data);

				if (data.code == 200) {
					// update checker
					updateChecker(data.dish, koef);
					checkDishLastUsed($dish, data.dish.last_used_at, date);
					calcErrors();
				}

				$('body').removeClass('waiting-for-load');
			}).fail(function (xhr, textStatus, errorThrown) {
				console.log(xhr);
				$('body').removeClass('waiting-for-load');
			});
		};

		var checkDishLastUsed = function checkDishLastUsed($dish, lastUse, dayOfUse) {

			var limitInDays = 14;

			if ($dish && lastUse && dayOfUse) {

				var lastUseOnlyDate = lastUse.split('T')[0];
				var lastUseTimestamp = new Date(lastUseOnlyDate);
				var dayOfUseTimestamp = new Date(dayOfUse);

				// calculate diff in days
				var diffInTime = dayOfUseTimestamp - lastUseTimestamp;
				var diffInDays = Math.ceil(diffInTime / (1000 * 60 * 60 * 24));

				// error element
				var $error = $dish.closest('.dishRow').find('.dishUsedError');

				// if more day then limit
				if (Math.abs(diffInDays) <= limitInDays) {

					var options = { weekday: 'long', year: 'numeric', month: 'long', day: 'numeric' };

					$error.text('Jídlo bylo použito dne: ' + lastUseTimestamp.toLocaleDateString('cs-CZ', options));

					$error.removeClass('d-none');
				} else {
					$error.text('');
					$error.addClass('d-none');
				}
			}
		};

		var updateChecker = function updateChecker(data, koef) {

			// console.log(data);

			// basket update
			var $basketTypeEl = $checkerBasket.find('.generator__checker-type[data-type-id="' + data.type_id + '"]');
			if ($basketTypeEl.length) {
				$.each(data['basket' + ageGroup], function (index, value) {
					if (value > 0) {
						var item = $basketTypeEl.find('.recommendations[data-name="' + index + '"]');
						updateCheckerItem(item, value * koef);
					}
				});
			}

			// nutri update
			var $nutriTypeEl = $checkerNutri.find('.generator__checker-type[data-type-id="' + data.type_id + '"]');
			if ($nutriTypeEl.length) {
				var $catEl = $nutriTypeEl.find('.recommendations[data-category-id="' + data.category_id + '"]');
				updateCheckerItem($catEl, 1 * koef);
			}
		};

		var updateCheckerItem = function updateCheckerItem(item, value) {
			var $actualValue = $(item).find('.actualValue');

			var target = parseFloat($(item).attr('data-value'));
			var actualValue = parseFloat($actualValue.text());
			var restriction = $(item).attr('data-restriction');

			actualValue = actualValue + value;
			$actualValue.text(actualValue);

			switch (restriction) {
				case 'min':
					if (actualValue < target) {
						$(item).removeClass('d-none');
					} else {
						$(item).addClass('d-none');
					}
					break;
				case 'max':
					if (actualValue <= target) {
						$(item).addClass('d-none');
					} else {
						$(item).removeClass('d-none');
					}
					break;
				default:
					if (actualValue == target) {
						$(item).addClass('d-none');
					} else {
						$(item).removeClass('d-none');
					}
			}
		};

		var $checker = $('#checker');
		var ageGroup = parseInt($checker.attr('data-agegroup'));
		var $checkerBasket = $checker.find('.generator__checker-basket');
		var $checkerNutri = $checker.find('.generator__checker-nutri');

		var $activeSelectionBtn = null;

		// hide recommendations if not necessary
		var $allRecommendations = $checker.find('.recommendations');
		for (var i = 0; i < $allRecommendations.length; i++) {
			updateCheckerItem($allRecommendations[i], 0);
		}
		calcErrors();

		$('.gSave').on('click', function (e) {

			var errorText = '';

			if (errors) {
				errorText = '\n\nNedostatky jídelníčku:';
				var $errors = $checker.find('.recommendations:not(".d-none")');
				for (var i = 0; i < $errors.length; i++) {
					errorText += '\n' + $($errors[i]).attr('data-type') + ': ' + $($errors[i]).text().replace('\n', '');
				}
			}

			if (confirm("Přejete si sestavený jídelníček uložit?" + errorText)) {

				var params = {
					"menu": JSON.stringify(selectedDishes)
				};

				// ajax
				$('body').addClass('waiting-for-load');
				var jqXHR = $.post('/admin/sestaveni-jidelnicku/save-menu/', params).done(function (data) {
					console.log(data);

					// if success
					if (data.code == 200) {
						// redirect
						location = '/admin/archiv';
					}

					$('body').removeClass('waiting-for-load');
				}).fail(function (xhr, textStatus, errorThrown) {
					console.log(xhr);
					$('body').removeClass('waiting-for-load');
				});
			} else {
				e.preventDefault();
			}
		});

		$('.gSaveEdited').on('click', function (e) {
			if (confirm("Přejete si změny v jídelníčku uložit?")) {

				// console.log(menuIndexInSet);
				// console.log(selectedDishes);

				var params = {
					"menu": JSON.stringify(selectedDishes['week' + menuIndexInSet]),
					"id": menuIdInDB,
					"ageGroup": ageGroup
				};

				// console.log(JSON.stringify(selectedDishes));

				// ajax
				$('body').addClass('waiting-for-load');
				var jqXHR = $.post('/admin/sestaveni-jidelnicku/edit-menu/', params).done(function (data) {
					console.log(data);

					// if success
					if (data.code == 200) {
						// redirect
						location = '/admin/archiv';
					}

					$('body').removeClass('waiting-for-load');
				}).fail(function (xhr, textStatus, errorThrown) {
					console.log(xhr);
					$('body').removeClass('waiting-for-load');
				});
			} else {
				e.preventDefault();
			}
		});

		$('.editDish').on('click', function (e) {
			$activeSelectionBtn = $(this);
		});

		$('.modal-submit').on('click', function (e) {
			var $modal = $(this).closest('.modal');
			var $select = $modal.find('select.dishes');
			var values = Array.isArray($select.val()) ? $select.val() : [$select.val()];
			// console.log(values);

			var week = $activeSelectionBtn.closest('.week').attr('data-week');
			var day = $activeSelectionBtn.closest('.day').attr('data-day');
			var type = $activeSelectionBtn.attr('data-type');

			// console.log(week);
			// console.log(day);
			// console.log(type);

			// get dish and remove if have to update
			var actualDishId = selectedDishes[week]['days'][day][type].id;
			if (actualDishId && actualDishId.length == 1) {
				updateCheckerAjax(actualDishId[0], -1);
			}

			// save id to json
			selectedDishes[week]['days'][day][type].id = Array.isArray(values) ? values : [values];
			// console.log(selectedDishes);

			// show selected dish name
			var $selectedOptions = $select.find('option:selected');
			var text = '';
			for (var i = 0; i < $selectedOptions.length; i++) {
				if (i == $selectedOptions.length - 1) {
					text += $($selectedOptions[i]).text();
				} else {
					text += $($selectedOptions[i]).text() + ', ';
				}
			}

			// edited dish element
			var $dish = $('.table--generator').find('.day[data-day="' + day + '"] .week[data-week="' + week + '"] .dish[data-type="' + type + '"]');

			// update mobile and desktop version
			$dish.text(text);

			// colored after edit
			$dish.closest('.dishRow').addClass('edited');

			// save date of the day
			var date = selectedDishes[week]['days'][day]['date'];

			// check dish
			// ajax
			if ($selectedOptions.length == 1) {
				updateCheckerAjax(values[0], 1, $dish, date);
			}

			// close modal
			$modal.find('.modal-close').click();
		});
	}
})(jQuery);

},{}],4:[function(require,module,exports){
'use strict';

// https://codepen.io/gapcode/pen/vEJNZN
;(function ($) {

      "use strict";

      // Get IE or Edge browser version

      var version = detectIE();

      if (version !== false) {
            $('html').addClass('ie');
      }

      // add details to debug result
      // document.getElementById('details').innerHTML = window.navigator.userAgent;

      /**
       * detect IE
       * returns version of IE or false, if browser is not Internet Explorer
       */
      function detectIE() {
            var ua = window.navigator.userAgent;

            // Test values; Uncomment to check result …

            // IE 10
            // ua = 'Mozilla/5.0 (compatible; MSIE 10.0; Windows NT 6.2; Trident/6.0)';

            // IE 11
            // ua = 'Mozilla/5.0 (Windows NT 6.3; Trident/7.0; rv:11.0) like Gecko';

            // Edge 12 (Spartan)
            // ua = 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/39.0.2171.71 Safari/537.36 Edge/12.0';

            // Edge 13
            // ua = 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/46.0.2486.0 Safari/537.36 Edge/13.10586';

            var msie = ua.indexOf('MSIE ');
            if (msie > 0) {
                  // IE 10 or older => return version number
                  return parseInt(ua.substring(msie + 5, ua.indexOf('.', msie)), 10);
            }

            var trident = ua.indexOf('Trident/');
            if (trident > 0) {
                  // IE 11 => return version number
                  var rv = ua.indexOf('rv:');
                  return parseInt(ua.substring(rv + 3, ua.indexOf('.', rv)), 10);
            }

            // var edge = ua.indexOf('Edge/');
            // if (edge > 0) {
            //   // Edge (IE 12+) => return version number
            //   return parseInt(ua.substring(edge + 5, ua.indexOf('.', edge)), 10);
            // }

            // other browser
            return false;
      }
})(jQuery);

},{}],5:[function(require,module,exports){
'use strict';

;(function ($) {

	'use strict';

	var likes = {};

	// check the local storage
	if (localStorage.getItem('jidelnicekLikes')) {
		likes = JSON.parse(localStorage.getItem('jidelnicekLikes'));

		var $likes = $('.menu__like');
		for (var i = 0; i < $likes.length; i++) {
			var $el = $($likes[i]);
			if (likes[$el.attr('data-dish-id')]) {
				$el.attr('data-url', $el.attr('data-url-dislike'));
				$el.addClass('active');
			}
		}
	}

	// click to like button
	$('.like').on('click', function (e) {
		var $el = $(this);
		var url = $el.attr('data-url');
		var id = parseInt($el.attr('data-dish-id'));
		var isActive = $el.hasClass('active');

		var params = {
			"id": id
		};

		$('body').addClass('waiting-for-load'); // ajax pending

		// ajax
		var jqXHR = $.post(url, params).done(function (data) {
			if (data.status == 'ok') {
				if (isActive) {
					delete likes[id];
					localStorage.setItem('jidelnicekLikes', JSON.stringify(likes));

					$el.attr('data-url', $el.attr('data-url-like'));
					$el.removeClass('active');
				} else {
					likes[id] = 1;
					localStorage.setItem('jidelnicekLikes', JSON.stringify(likes));

					$el.attr('data-url', $el.attr('data-url-dislike'));
					$el.addClass('active');
				}
			}

			$('body').removeClass('waiting-for-load'); // ajax pending
		}).fail(function (xhr, textStatus, errorThrown) {
			console.log(xhr);
			$('body').removeClass('waiting-for-load'); // ajax pending
		});
	});
})(jQuery);

},{}],6:[function(require,module,exports){
'use strict';

;(function ($) {

	'use strict';

	var $selectBg = $('#frm-backgroundForm-background');
	if ($selectBg.length) {
		var $images = $('#preview').find('.preview__bg');
		$images.attr('src', '/upload/' + $selectBg.val());

		$selectBg.on('change', function (e) {
			$images.attr('src', '/upload/' + $(this).val());
		});
	}
})(jQuery);

},{}]},{},[1])

//# sourceMappingURL=main.js.map
