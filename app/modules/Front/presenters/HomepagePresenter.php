<?php

namespace App\FrontModule\Presenters;

use Nette;
use App\Model;
use Tracy\Debugger;
use Nette\Application\IResponse;

use App\Model\MenuManager;
use App\Model\DishManager;

class HomepagePresenter extends BasePresenter
{

	/** @var MenuManager */
	private $menuManager;

	/** @var DishManager */
	private $dishManager;

	// database
	protected $database;

	public function __construct(Nette\Database\Context $database, MenuManager $menuManager, DishManager $dishManager)
	{
		$this->database = $database;
		$this->menuManager = $menuManager;
		$this->dishManager = $dishManager;
	}

	public function renderDefault()
	{
		$this->template->settings = $this->getSettings();

		$ageGroupKeys = json_decode($this->getSettings()['age_group']);
		$ageGroup = $this->getAgeGroup($ageGroupKeys[0]);
		$actualMenu = $this->menuManager->getActualMenu($ageGroup);
		// Debugger::barDump(empty($isEmpty['monday']['main1']));
		// Debugger::barDump(empty($isEmpty));

		// test if some ageGroup menu is empty
		// $this->template->isMenuGroup1Empty = $this->menuManager->testIfEmpty($this->menuManager->getActualMenu('3-6 let')['dishes']);
		// $this->template->isMenuGroup2Empty = $this->menuManager->testIfEmpty($this->menuManager->getActualMenu('7-10 let')['dishes']);
		// $this->template->isMenuGroup3Empty = $this->menuManager->testIfEmpty($this->menuManager->getActualMenu('11-14 let')['dishes']);
		// $this->template->isMenuGroup4Empty = $this->menuManager->testIfEmpty($this->menuManager->getActualMenu('15-18 let')['dishes']);
		// $this->template->isMenuGroup5Empty = $this->menuManager->testIfEmpty($this->menuManager->getActualMenu('others')['dishes']);
		$this->template->isMenuGroup1Empty = false;
		$this->template->isMenuGroup2Empty = false;
		$this->template->isMenuGroup3Empty = false;
		$this->template->isMenuGroup4Empty = false;
		$this->template->isMenuGroup5Empty = false;

		$this->template->ageGroups = $ageGroupKeys;
		$id = null;
		if ($actualMenu['week']) {
			$id = $actualMenu['week']->id;
		}
		$this->template->id = $id;
	}

	public function actionDetail1() {
		$this->setTemplateForDetail(1);
	}

	public function actionDetail23() {

		$ageGroupKeys = json_decode($this->getSettings()['age_group']);
		$ageGroupNumber = 2;
		if (in_array(3, $ageGroupKeys)) {
			$ageGroupNumber = 3;
		}

		$this->setTemplateForDetail($ageGroupNumber);
	}

	public function actionDetail3() {
		$this->setTemplateForDetail(3);
	}

	public function actionDetail4() {
		$this->setTemplateForDetail(4);
	}

	public function actionDetail5() {
		$this->setTemplateForDetail(5);
	}

	private function setTemplateForDetail(int $ageGroupKey) {
		$this->template->settings = $this->getSettings();
		
		$this->template->showLikesComponent = true;
		$this->template->closedAtDates = json_decode($this->getSettings()['closed']);

		$ageGroup = $this->getAgeGroup($ageGroupKey);
		$this->template->ageGroup = $ageGroupKey;

		$actualMenu = $this->menuManager->getActualMenu($ageGroup);
		if ($this->menuManager->testIfEmpty($actualMenu['dishes'])) {
			
			$actualMenuId = $actualMenu['week']->id;

			$notFound = true;
			while($notFound) {
				$foundedMenu = $this->menuManager->getMenuArray($actualMenuId++, $ageGroup);

				if (!$this->menuManager->testIfEmpty($foundedMenu['dishes'])) {
					$notFound = false;
					$actualMenu = $foundedMenu;
				}
			}
		}

		$this->template->menu = $actualMenu;
	}

	public function actionPrevious1() {
		$this->setTemplateForPrevious(1);
	}

	public function actionPrevious23() {
		
		$ageGroupKeys = json_decode($this->getSettings()['age_group']);
		$ageGroupNumber = 2;
		if (in_array(3, $ageGroupKeys)) {
			$ageGroupNumber = 3;
		}

		$this->setTemplateForPrevious($ageGroupNumber);

	}

	public function actionPrevious4() {
		$this->setTemplateForPrevious(4);
	}

	public function actionPrevious5() {
		$this->setTemplateForPrevious(5);
	}


	private function setTemplateForPrevious(int $ageGroupKey)
	{
		$this->template->settings = $this->getSettings();
		$this->template->showLikesComponent = true;
		$this->template->closedAtDates = json_decode($this->getSettings()['closed']);
		
		$this->template->ageGroup = $ageGroupKey;
		$this->template->menu = $this->menuManager->getPreviousMenu($ageGroupKey);
	}

	public function actionLike() {

		$httpRequest = $this->getHttpRequest();
		$httpResponse = $this->getHttpResponse();

		$id = $httpRequest->getPost('id');
		if ($id) {
			$result = $this->dishManager->likeDish($id);
			if($result) {
				$httpResponse->setCode(Nette\Http\Response::S200_OK);
				
				$responseDone = new \Nette\Application\Responses\JsonResponse([
					'status' => 'ok',
					'code' => Nette\Http\Response::S200_OK,
					'message' => 'Jídlo ID ' . $id . ' bylo lajknuto'
				]);
				$this->sendResponse($responseDone);

			}
			else {
				$httpResponse->setCode(Nette\Http\Response::S200_OK);
				
				$responseFail = new \Nette\Application\Responses\JsonResponse([
					'status' => 'error',
					'code' => Nette\Http\Response::S404_NOT_FOUND,
					'message' => 'Jídlo ID ' . $id . ' nebylo nalezeno'
				]);
				$this->sendResponse($responseFail);

			}
		}

	}

	public function actionDislike() {

		$httpRequest = $this->getHttpRequest();
		$httpResponse = $this->getHttpResponse();

		$id = $httpRequest->getPost('id');
		if ($id) {
			$result = $this->dishManager->dislikeDish($id);
			if($result == 1) {
				$httpResponse->setCode(Nette\Http\Response::S200_OK);
				
				$response = new \Nette\Application\Responses\JsonResponse([
					'status' => 'ok',
					'code' => Nette\Http\Response::S200_OK,
					'message' => 'Jídlo ID ' . $id . ' bylo dislajknuto'
				]);
				$this->sendResponse($response);

			}
			else {
				if ($result == 0) {
					$httpResponse->setCode(Nette\Http\Response::S200_OK);
					
					$response = new \Nette\Application\Responses\JsonResponse([
						'status' => 'ok',
						'code' => Nette\Http\Response::S204_NO_CONTENT,
						'message' => 'Jídlo ID ' . $id . ' nebylo dislajknuto, protože počet lajků se rovná 0'
					]);
					$this->sendResponse($response);
				}
				else {
					$httpResponse->setCode(Nette\Http\Response::S200_OK);
					
					$response = new \Nette\Application\Responses\JsonResponse([
						'status' => 'error',
						'code' => Nette\Http\Response::S404_NOT_FOUND,
						'message' => 'Jídlo ID ' . $id . ' nebylo nalezeno'
					]);
					$this->sendResponse($response);
				}

			}
		}
	}

	private function getAgeGroup(int $key) {
		switch($key) {
			case 1:
				return '3-6 let';
			case 2:
				return '7-10 let';
			case 3:
				return '11-14 let';
			case 4:
				return '15-18 let';
			default:
				return 'others';
		}
	}

}