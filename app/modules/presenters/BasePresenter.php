<?php

namespace App\Presenters;

use Nette;
use App\Model;
use Nette\Utils\DateTime;
use Tracy\Debugger;

/**
 * Base presenter for all application presenters.
 */
abstract class BasePresenter extends Nette\Application\UI\Presenter
{

	//database
	protected  $database;

	public function __construct(Nette\Database\Context $database) {
		$this->database = $database;
	}

	// get remote IP address - local IP address
	protected function getIpAddress() {
		//set voter IP address
		$httpRequest = $this->getHttpRequest();
		return $httpRequest->getRemoteAddress();
	}

	// get actual server time
	protected function getActualServerTime() {
		return new DateTime();
	}

	// get settings
	protected function getSettings() {
		
		$settings = $this->database->table('settings')->get(1)->toArray();
		if (!$settings) {
			$settings = 'Není nastaveno';
		}

		// Debugger::barDump($settings);

		return $settings;

	}

	/**
	 * Lazy hacky rendering Bootstrap form (v4)
	 */
	protected function renderAsBootstrapForm($form)
	{
		$renderer = $form->getRenderer();


		$renderer->wrappers['controls']['container'] = NULL;
		$renderer->wrappers['pair']['container'] = 'div class="form-group"';
		// $renderer->wrappers['pair']['container'] = 'div class="form-group row"';
		$renderer->wrappers['pair']['.error'] = 'has-error';
		// $renderer->wrappers['control']['container'] = 'div class=col-10';
		// $renderer->wrappers['label']['container'] = 'div class="col-3 control-label"';
		$renderer->wrappers['control']['description'] = 'small class="form-text text-muted"';
		$renderer->wrappers['control']['errorcontainer'] = 'small class="form-text text-muted"';

		// Debugger::barDump($renderer->wrappers);

		// make form and controls compatible with Twitter Bootstrap
		// $form->getElementPrototype()->class('form-horizontal');

		foreach ($form->getControls() as $control)
		{

			// Debugger::barDump($control);

			// Labels
			// $control->getLabelPrototype()->addClass('col-2 col-form-label');

			if ($control instanceof  \Nette\Forms\Controls\Button)
			{
				$control->getControlPrototype()->addClass(empty($usedPrimary) ? 'btn btn-secondary mt-4' : 'btn');
				$usedPrimary = TRUE;
			}
			elseif($control instanceof \Nette\Forms\Controls\UploadControl) {				
				$control->getControlPrototype()->addClass('form-control-file');
			}
			elseif ($control instanceof \Nette\Forms\Controls\TextBase || $control instanceof \Nette\Forms\Controls\SelectBox || $control instanceof \Nette\Forms\Controls\MultiSelectBox)
			{
				$control->getControlPrototype()->addClass('form-control');
			}
			elseif ($control instanceof \Nette\Forms\Controls\Checkbox || $control instanceof \Nette\Forms\Controls\Radio)
			{
				// Labels
				$control->getLabelPrototype()->addClass('form-check-label');

				// Control
				$control->getControlPrototype()->addClass('form-check-input');

				// Control wrapper
				$control->getSeparatorPrototype()->setName('div')->addClass('form-check ' . $control->getControlPrototype()->type);

			}
			elseif($control instanceof \Nette\Forms\Controls\CheckboxList || $control instanceof \Nette\Forms\Controls\RadioList) {

				// Labels
				$control->getLabelPrototype()->setName('label')->addClass('d-block');
				$control->getItemLabelPrototype()->addClass('form-check-label');

				// Control
				$control->getControlPrototype()->class[] = 'form-check-input';

				// Control wrapper
				$control->getSeparatorPrototype()->setName('div')->addClass('form-check form-check-inline mr-3 ' . $control->getControlPrototype()->type);
			}
		}
	}
}
