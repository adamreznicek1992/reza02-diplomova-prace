<?php

namespace App\AdminModule\Presenters;

use Nette;
use App\Forms\SignFormFactory;


class SignPresenter extends BasePresenter
{
	/** @var SignFormFactory @inject */
	public $factory;

	public function actionIn() {
		if ($this->getUser()->isLoggedIn()) {
			$this->redirect('Homepage:');
		}
	}

	/**
	 * Sign-in form factory.
	 * @return Nette\Application\UI\Form
	 */
	protected function createComponentSignInForm()
	{

		if ($this->getUser()->isLoggedIn()) {
			$form->getPresenter()->redirect('Homepage:');
		}

		$form = $this->factory->create();
		$form->onSuccess[] = function ($form) {
			$form->getPresenter()->redirect('Homepage:');
		};
		$this->renderAsBootstrapForm($form); //boostrap
		return $form;
	}


	public function actionOut()
	{
		$this->getUser()->logout();
		$this->redirect(':Front:Homepage:');
	}

}
