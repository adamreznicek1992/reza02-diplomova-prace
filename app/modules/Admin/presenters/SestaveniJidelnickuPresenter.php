<?php

namespace App\AdminModule\Presenters;

use Nette;
use App\Model;
use Tracy\Debugger;
use Nette\Utils\DateTime;

use App\Model\DishManager;
use App\Model\RecommendationsManager;
use App\Model\TypeManager;
use App\Model\MenuManager;
use App\Model\BasketManager;
use App\Model\CategoryManager;


class SestaveniJidelnickuPresenter extends BasePresenter
{

	/** @var DishManager */
	private $dishManager;

	/** @var RecommendationsManager */
	private $recommendationsManager;

	/** @var TypeManager */
	private $typeManager;

	/** @var MenuManager */
	private $menuManager;

	/** @var BasketManager */
	private $basketManager;

	/** @var CategoryManager */
	private $categoryManager;

	// database
	protected $database;

	public function __construct(Nette\Database\Context $database, DishManager $dishManager, RecommendationsManager $recommendationsManager, TypeManager $typeManager, MenuManager $menuManager, BasketManager $basketManager, CategoryManager $categoryManager)
	{
		$this->database = $database;
		$this->dishManager = $dishManager;
		$this->recommendationsManager = $recommendationsManager;
		$this->typeManager = $typeManager;
		$this->menuManager = $menuManager;
		$this->basketManager = $basketManager;
		$this->categoryManager = $categoryManager;
	}

	public function renderDefault() {
    $this->testIsUserLoggedIn();
		$this->template->actualServerTime = $this->getActualServerTime();
		$this->template->settings = $this->getSettings();

		$this->template->ageGroups = json_decode($this->getSettings()['age_group']);
	}

	public function actionEdit(int $id, int $ageGroupKey) {	

		$this->testIsUserLoggedIn();
		$this->template->actualServerTime = $this->getActualServerTime();
		$this->template->settings = $this->getSettings();

		$this->template->id = $id;
		$this->template->ageGroup = $ageGroupKey;

		$this->createLayoutForGenerator($ageGroupKey, false, $id);
	}

	public function renderMenuBuilder(int $ageGroupKey, bool $empty = false) {

		$this->testIsUserLoggedIn();
		$this->template->actualServerTime = $this->getActualServerTime();
		$this->template->settings = $this->getSettings();
		
		$this->template->ageGroup = $ageGroupKey;
		$this->createLayoutForGenerator($ageGroupKey, $empty);
	}

	private function createLayoutForGenerator(int $ageGroupKey, bool $empty = false, int $id = 0) {
		
		$ageGroup = $this->getAgeGroup($ageGroupKey);

		$soupTypeId = $this->typeManager->getId('Polévka');
		$mainDishTypeId = $this->typeManager->getId('Hlavní chod');

		$this->template->listOfSoups = $this->dishManager->getAllDishesByType('Polévka');
		$this->template->listOfMains = $this->dishManager->getAllDishesByType('Hlavní chod');
		$this->template->listOfAdditions = $this->dishManager->getAllDishesByType('Přídavek');
		$this->template->listOfDrinks = $this->dishManager->getAllDishesByType('Nápoj');

		// recommendations
		// basket
		if ($ageGroupKey < 5) {
			$this->template->recommedBasketForSoup = $this->recommendationsManager->getAllBasketRecommendationsByTypeAndAgeGroup($soupTypeId, $ageGroup);
			$this->template->recommedBasketForMainDish = $this->recommendationsManager->getAllBasketRecommendationsByTypeAndAgeGroup($mainDishTypeId, $ageGroup);
		}

		// nutri
		$this->template->recommedNutriForSoup = $this->recommendationsManager->getAllNutriRecommendationsByType($soupTypeId);
		$this->template->recommedNutriForMainDish = $this->recommendationsManager->getAllNutriRecommendationsByType($mainDishTypeId);

		// create
		if (!$id) {

			$this->template->editedMenuIndex = null;

			// actual week number
			$actualWeekNumber = intval(date('W'));
			$this->template->week = $actualWeekNumber;
			
			// last menu in db
			$lastMenu = $this->menuManager->getLastMenu();
	
			// create dates array
			if ($lastMenu) {
				$dates = $this->getDatesForGenerator($lastMenu->year, $lastMenu->number);
			}
			else {
				$dates = $this->getDatesForGenerator(intval(date('Y')), $actualWeekNumber);
			}

			// generate menu via automatic method
			$menusArray = $this->generateMenu($ageGroupKey, $empty);

		}
		// edit
		else {

			// get all 4 weeks by id
			$set = $this->menuManager->getMenuSet($id);
			if (!$set) {
				return false;
			}

			// get the index of the edited menu
			$index = 1;
			foreach($set as $m) {
				if ($m->id == $id) {
					break;
				}
				$index++;
			}
			// Debugger::barDump($index);
			$this->template->editedMenuIndex = $index;
			$this->template->id = $id;
	
			// create dates array
			$firstMenu = reset($set);
			$weekNumber = $firstMenu->number;
			$yearNumber = $firstMenu->year;
	
			if ($weekNumber == 1) {
				$weekNumber = 53;
				$yearNumber -= 1;
			}
			else {
				$weekNumber -= 1;
			}

			$dates = $this->getDatesForGenerator($yearNumber, $weekNumber);

			// get menu array
			$menusArray = $this->generateMenu($ageGroupKey, $empty, $set);
		}

		$menusJson = $this->transferMenuToJson($menusArray, $dates);

		// Debugger::barDump($menusJson);

		$this->template->dates = $dates; 
		$this->template->menuArray = $menusArray;
		$this->template->menuJson = $menusJson;
	}

	public function transferMenuToJson(array $menu, array $dates) {

		// Debugger::barDump($menu);

		$json = [];
		$days = ['monday', 'tuesday', 'wednesday', 'thursday', 'friday'];

		// Debugger::barDump($menu['mains2']);

		for ($i = 1; $i < 5; $i++) {
			$week = 'week' . $i;
			$json[$week]['year'] = $dates[$week]['year'];
			$json[$week]['number'] = $dates[$week]['number'];
			$json[$week]['days'] = [];

			foreach($days as $index => $day) {
				$json[$week]['days'][$day] = [];

				// snacks 1 - nursery school
				$snacks1 = $menu['snacks1'];
				$snack1 = [];
				if ($snacks1) {
					foreach($snacks1[(($index + 1) + (($i - 1) * count($days))) - 1] as $sn1) {
						$snack1[] = $sn1->id;
					}
				}
				$json[$week]['days'][$day]['snacks1']['id'] = $snack1;

				// soups
				$soups = $menu['soups'];
				$soup = null;
				if ($soups) {
					$soup = $soups[(($index + 1) + (($i - 1) * count($days))) - 1];
				} 
				$json[$week]['days'][$day]['soup']['id'] = ($soup) ? [$soup->id] : null;

				// mains 1
				$mains = $menu['mains'];
				$main = null;
				if ($soups) {
					$main = $mains[(($index + 1) + (($i - 1) * count($days))) - 1];
				}
				$json[$week]['days'][$day]['main']['id'] = ($main) ? [$main->id] : null;

				// mains 2
				if ($this->getSettings()['has_two_main_dishes']) {
					if ($menu['mains2']) {
						$m2 = $menu['mains2'][(($index + 1) + (($i - 1) * count($days))) - 1];
						if ($m2) {
							$json[$week]['days'][$day]['main2']['id'] = [$m2->id];
						}
						else {
							$json[$week]['days'][$day]['main2']['id'] = null;
						}
					}
					else {
						$json[$week]['days'][$day]['main2']['id'] = null;
					}
				}

				// additions
				$additions = $menu['additions'];
				$adds = [];
				if ($additions) {
					foreach($additions[(($index + 1) + (($i - 1) * count($days))) - 1] as $a) {
						$adds[] = $a->id;
					}
				}
				$json[$week]['days'][$day]['additions']['id'] = $adds;

				// drinks
				$drinks = $menu['drinks'];
				$drs = [];
				if ($drinks) {
					foreach($drinks[(($index + 1) + (($i - 1) * count($days))) - 1] as $d) {
						$drs[] = $d->id;
					}
				}
				$json[$week]['days'][$day]['drinks']['id'] = $drs;

				// snacks 2 - nursery school
				$snacks2 = $menu['snacks2'];
				$snack2 = [];
				if ($snacks2) {
					foreach($snacks2[(($index + 1) + (($i - 1) * count($days))) - 1] as $sn2) {
						$snack2[] = $sn2->id;
					}
				}
				$json[$week]['days'][$day]['snacks2']['id'] = $snack2;

				$json[$week]['days'][$day]['date'] = date('Y-m-d', strtotime($day, strtotime($dates[$week]['year'] . 'W' . $dates[$week]['number2d'])));
			}
		}		

		return json_decode(json_encode($json));
	}

	private function generateMenu(int $ageGroupKey, bool $empty, array $set = null) : array {

		$menu = [];

		$soupTypeId = $this->typeManager->getId('Polévka');
		$mainDishTypeId = $this->typeManager->getId('Hlavní chod');

		$snacks1Array = []; //nursery school
		$soupsArray = [];
		$mainDishesArray = [];
		$mainDishesArray2 = [];
		$additionsArray = [];
		$drinksArray = [];
		$snacks2Array = []; //nursery school

		// if not manual method = empty menu
		if (!$empty) {

			// automatic method
			if (!$set) {

				// get snack 1
				$snacks1Array = $this->getApproSnacksArray();

				// get soups
				$soupsArray = $this->getAprroDishesArrayByRecommendations($soupTypeId);
				
				// get main dishes
				$mainDishesArray = $this->getAprroDishesArrayByRecommendations($mainDishTypeId);
	
				// get alternative main dishes if configured
				if ($this->getSettings()['has_two_main_dishes']) {
					$mainDishesArray2 = $this->getAlternativeMainDishes($mainDishesArray);
				}
	
				// get additions (fruits, salads and desserts)
				$additionsArray = $this->getApproAdditionsArray();
	
				// get drinks
				$drinksArray = $this->getApproDrinksArray();

				// get snack 2
				$snacks2Array = $this->getApproSnacksArray();
			}
			// edit
			else {

				$ageGroup = $this->getAgeGroup($ageGroupKey);

				$days = ['monday', 'tuesday', 'wednesday', 'thursday', 'friday'];
				foreach ($set as $w) {
					$index = 0;
					foreach ($w->related('relation_menu_day_menu_week') as $relToDay) {
		
						$day = $relToDay->ref('menu_day');
						$dayName = $days[$index++];
						
						$sn1Array = [];
						$sn2Array = [];
						$addsArray = [];
						$drsArray = [];
	
						$relations = $day->related('relation_dish_menu_day');
	
						$hasSoup = false;
						$hasMain = false;
						$hasMain2 = false;
						foreach($relations as $relToDish) {

							if ($relToDish->age_group == $ageGroup) {
								
								$dish = $relToDish->ref('dish');
		
								// second main dish
								if ($relToDish->is_main2 || $relToDish->is_snack1 || $relToDish->is_snack2) {
									
									if ($this->getSettings()['has_two_main_dishes']) {
										if ($relToDish->is_main2) {
											$mainDishesArray2[] = $dish;
											$hasMain2 = true;
										}
									}
	
									if ($relToDish->is_snack1) {
										$sn1Array[] = $dish;
									}
	
									if ($relToDish->is_snack2) {
										$sn2Array[] = $dish;
									}
	
								}
								// all dishes
								else {
									switch($dish->type->name) {
										case 'Hlavní chod':
											$mainDishesArray[] = $dish;
											$hasMain = true;
											break;
										case 'Polévka':
											$soupsArray[] = $dish;
											$hasSoup = true;
											break;
										case 'Nápoj':
											$drsArray[] = $dish;
											break;
										default:
											$addsArray[] = $dish;
											break;
									}
								}
							}
	
						}
	
						if (!$hasSoup) {
							$soupsArray[] = null;
						}
	
						if (!$hasMain) {
							$mainDishesArray[] = null;
						}
	
						if (!$hasMain2) {
							$mainDishesArray2[] = null;
						}
						
						$snacks1Array[] = $sn1Array;
						$snacks2Array[] = $sn2Array;
						$drinksArray[] = $drsArray;
						$additionsArray[] = $addsArray;
					}
				}
			}
		}


		// add dishes into menu array
		$menu['snacks1'] = $snacks1Array;
		$menu['soups'] = $soupsArray;
		$menu['mains'] = $mainDishesArray;
		$menu['mains2'] = $mainDishesArray2;
		$menu['additions'] = $additionsArray;
		$menu['drinks'] = $drinksArray;
		$menu['snacks2'] = $snacks2Array;

		// if configured for two main dishes
		if ($this->getSettings()['has_two_main_dishes']) {
			$menu['mains2'] = $mainDishesArray2;
		}

		// calculate menu basket and nutri recommendation
		$mainDishesArrayMerge = array_merge($mainDishesArray, $mainDishesArray2);
		
		// dont calculate for others age group
		if ($ageGroupKey < 5) {
			$menu['basket']['mains'] = $this->calculateBasket($mainDishTypeId, $mainDishesArrayMerge, $ageGroupKey);
			$menu['basket']['soups'] = $this->calculateBasket($soupTypeId, $soupsArray, $ageGroupKey);
		}

		$menu['nutri']['mains'] = $this->calculateNutri($mainDishTypeId, $mainDishesArrayMerge);
		$menu['nutri']['soups'] = $this->calculateNutri($soupTypeId, $soupsArray);

		// Debugger::barDump($menu);

		return $menu;
	}

	private function getAlternativeMainDishes(array $dishes) : array {

		// main dishes array copy
		$alternatives = array_replace([], $dishes);

		// shake it
		shuffle($alternatives);

		// forbidden category in the same day
		$sweetCategoryId = $this->categoryManager->getCategoryByName('Bezmasé jídlo sladké')->id;
		$loop = 0;
		$valid = false;
		while (!$valid && $loop < 10) {

			$loop++;

			for ($i = 0; $i < count($dishes); $i++) {

				$d = $dishes[$i];
				$a = $alternatives[$i];
				if ($d && $a) {
					$dCatId = $d->category->id;
					$aCatId = $a->category->id;
	
					// test if not equal dishes and not be two sweet dishes in the same day
					if ($d->id == $a->id || ($dCatId == $sweetCategoryId && $aCatId == $sweetCategoryId)) {
	
						$newIndex = $this->getIndexForSwap($dishes, $a, $i, $sweetCategoryId);
	
						// swap dishes
						$temp = $alternatives[$newIndex];
						$alternatives[$newIndex] = $a;
						$alternatives[$i] = $temp;
	
						break;
					}
				}
				
				// if array has no issue, then valid
				if ($i == count($dishes) - 1) {
					$valid = true;
				} 
			}
		}

		return $alternatives;
	}

	private function getIndexForSwap(array $dishes, object $item, int $index, int $forbiddenCat) : int {
		for ($i = 0; $i < count($dishes); $i++) {
			if ($i != $index) {
				
				$d = $dishes[$i];
				$dCatId = $d->category->id;
				$itemCatId = $item->category->id;

				if ($d->id != $item->id && !($dCatId == $forbiddenCat && $itemCatId == $forbiddenCat)) {
					return $i;
				}
			}
		}

		return 0;
	}

	private function getApproAdditionsArray() : array {
		
		$array = [];

		$fruitCategory = $this->categoryManager->getCategoryByName('Ovoce');
		$fruits = $this->dishManager->getDishesByCategory($fruitCategory->id);
		$fruitsKeys = array_keys($fruits);

		$salads = $this->dishManager->getAllDishesByType('Salát', true);
		$saladsKeys = array_keys($salads);
		$desserts = $this->dishManager->getAllDishesByType('Moučník/dezert', true);
		$dessertsKeys = array_keys($desserts);

		// 20 days == 20items in array
		for ($i = 0; $i < 20; $i++) {

			$adds = [];
			
			// random id from fruits
			$randomNumber = rand(0, count($fruits) - 1);
			$adds[] = $fruits[$fruitsKeys[$randomNumber]];

			// random salads
			if (rand(0, 2) == 1) {
				$randomNumber = rand(0, count($salads) - 1);
				$adds[] = $salads[$saladsKeys[$randomNumber]];
			}

			// random desserts
			if (rand(0, 4) == 1) {
				$randomNumber = rand(0, count($desserts) - 1);
				$adds[] = $desserts[$dessertsKeys[$randomNumber]];
			}

			$array[$i] = $adds; 

		}

		return $array;
	}

	private function getApproDrinksArray() : array {
		$array = [];

		// 20 days == 20items in array
		for ($i = 0; $i < 20; $i++) {
			$array[$i] = $this->dishManager->getAllDishesByType('Nápoj', true, rand(1, 2));
		}

		return $array;
	}

	private function getApproSnacksArray() : array {
		
		$array = [];

		// Fruits
		$fruitCategory = $this->categoryManager->getCategoryByName('Ovoce');
		$fruits = $this->dishManager->getDishesByCategory($fruitCategory->id);
		$fruitsKeys = array_keys($fruits);

		// Vegetables
		$vegetableCategory = $this->categoryManager->getCategoryByName('Zelenina');
		$vegetables = $this->dishManager->getDishesByCategory($vegetableCategory->id);
		$vegetablesKeys = array_keys($vegetables);

		// Rolls and breads
		$pastryCategory = $this->categoryManager->getCategoryByName('Pečivo');
		$pastries = $this->dishManager->getDishesByCategory($pastryCategory->id);
		$pastriesKeys = array_keys($pastries);

		// 20 days == 20items in array
		for ($i = 0; $i < 20; $i++) {

			$others = [];

			$randomNumber = rand(0, count($pastries) - 1);
			$others[] = $pastries[$pastriesKeys[$randomNumber]];
			
			// random id from fruits
			if (rand(0, 2) == 1) {
				$randomNumber = rand(0, count($fruits) - 1);
				$others[] = $fruits[$fruitsKeys[$randomNumber]];
			}

			// random id from vegetables
			if (rand(0, 3) == 1) {
				$randomNumber = rand(0, count($vegetables) - 1);
				$others[] = $vegetables[$vegetablesKeys[$randomNumber]];
			}

			$array[$i] = $others; 

		}

		return $array;
	}

	private function getAprroDishesArrayByRecommendations(int $typeId) : array {

		$recommendations = $this->recommendationsManager->getAllNutriRecommendationsByType($typeId);

		$dishes = [];
		foreach($recommendations as $r) {
			$catId = $r->category->id;
			$count = $r->value;
			$dishes = array_merge($dishes, $this->dishManager->getApproDishesByCategory($catId, $count));
		}

		// if array not has 20 dishes, add items of null
		$diffCount = 20 - count($dishes);
		if ($diffCount != 0) {
			$diffArray = array_fill(0, $diffCount, null);
			$dishes = array_merge($dishes, $diffArray);
		}

		// shake it
		shuffle($dishes);

		return $dishes;
	}

	private function calculateBasket(int $typeId, array $dishes, int $ageGroupKey) : array {

		$ageGroup = $this->getAgeGroup($ageGroupKey);

		$basket = [];
		$basketItems = $this->recommendationsManager->getBasketAssocArray($typeId, $ageGroup);
		foreach ($basketItems as $bi) {
			$basket[$bi] = 0;
		}

		// all selected dishes
		foreach($dishes as $d) {

			if ($d) {
				$dishBasket = $this->basketManager->getBasket($d->id, $ageGroupKey);
	
				// all dish basket items
				foreach ($basketItems as $i) {
					$basket[$i] += $dishBasket[$i];
				}
			}
		}

		return $basket;
	}

	public function calculateNutri(int $typeId, array $dishes) {

		$nutri = [];

		$nutriItems = $this->recommendationsManager->getAllNutriRecommendationsByType($typeId);
		// Debugger::barDump($nutriItems);

		foreach($nutriItems as $n) {
			$counter = 0;

			foreach($dishes as $key => $d) {
				if ($d) {
					if ($n->category->id == $d->category->id) {
						// increase the counter
						$counter++;
						// remove dish from array - optimalisation
						unset($dishes[$key]);
						
					}
				}
			}

			$nutri[$n->id] = $counter;
		}

		return $nutri;
	}

	public function getDatesForGenerator($yearNumber, $weekNumber) {

		$dates = null;

		for ($i = 1; $i < 5; $i++) {

			if ($weekNumber == 53) {
				$weekNumber = 1;
				$yearNumber += 1;
			}
			else {
				$weekNumber += 1;
			}

			$dates['week' . $i] = [
				'year' => $yearNumber,
				'number' => $weekNumber,
				'number2d' => sprintf('%02d', $weekNumber),
				'monday' => strtotime( 'monday', strtotime($yearNumber . "W" . sprintf('%02d', $weekNumber))),
				'friday'  => strtotime( 'friday', strtotime($yearNumber . "W" . sprintf('%02d', $weekNumber))),
			];

		}

		return $dates;
	}

	public function actionDish(int $id) {

		$this->testIsUserLoggedIn();

		$httpResponse = $this->getHttpResponse();

		if ($id) {
			$result = $this->dishManager->getDish($id);
			if($result) {
				$httpResponse->setCode(Nette\Http\Response::S200_OK);
				
				$responseDone = new \Nette\Application\Responses\JsonResponse([
					'status' => 'ok',
					'code' => Nette\Http\Response::S200_OK,
					'message' => 'Jídlo ID ' . $id . ' bylo nalezeno',
					'dish' => $result,
				]);
				$this->sendResponse($responseDone);

			}
			else {
				$httpResponse->setCode(Nette\Http\Response::S200_OK);
				
				$responseFail = new \Nette\Application\Responses\JsonResponse([
					'status' => 'error',
					'code' => Nette\Http\Response::S404_NOT_FOUND,
					'message' => 'Jídlo ID ' . $id . ' nebylo nalezeno',
					'dish' => null,
				]);
				$this->sendResponse($responseFail);

			}
		}

	}

	public function actionSaveMenu() {

		$this->testIsUserLoggedIn();

		$httpRequest = $this->getHttpRequest();
		$httpResponse = $this->getHttpResponse();

		$menu = json_decode($httpRequest->getPost('menu'));
		if ($menu) {

			// get last menu and update the build_set parameter
			$lastMenu = $this->menuManager->getLastMenu();
			$buildSetNumber = 1;
			if ($lastMenu) {
				$buildSetNumber = $lastMenu->build_set + 1;
			}

			// save the menu
			foreach ($menu as $key => $w) {
				
				// Debugger::barDump($w);

				$dayNumber = 1;

				$week = [];
				$week['number'] = $w->number;
				$week['year'] = $w->year;
				foreach (($w->days) as $dayName => $day) {
					$week['days'][] = ($this->menuManager->saveDay($day, $dayNumber++, $this->getSettings()['has_two_main_dishes']))->id;
				}
				$week['build_set'] = $buildSetNumber;

				// Debugger::barDump($week);
				$this->menuManager->saveWeek($week);

			}

			$responseDone = new \Nette\Application\Responses\JsonResponse([
				'status' => 'ok',
				'code' => Nette\Http\Response::S200_OK,
				'message' => 'Jídelníček byl uložen'
			]);
			$this->sendResponse($responseDone);

		}

	}

	public function actionEditMenu() {

		$this->testIsUserLoggedIn();

		$httpRequest = $this->getHttpRequest();
		$httpResponse = $this->getHttpResponse();

		$id = $httpRequest->getPost('id');
		$menu = json_decode($httpRequest->getPost('menu'));
		$ageGroupKey = $httpRequest->getPost('ageGroup');

		if ($menu) {

			// Debugger::barDump($menu);

			$week = $this->menuManager->getMenu($id);
			if ($week) {

				$index = 0;
				foreach (($menu->days) as $dayName => $day) {

					$relations = array_values($week->related('relation_menu_day_menu_week')->fetchAll());
					
					$dayId = $relations[$index++]->ref('menu_day')->id;
					$result = $this->menuManager->updateDay($dayId, $day, $ageGroupKey, $this->getSettings()['has_two_main_dishes']);
	
					if (!$result) {
						$responseFail = new \Nette\Application\Responses\JsonResponse([
							'status' => 'error',
							'code' => Nette\Http\Response::S404_NOT_FOUND,
							'message' => 'Jídelníček ' . $id . ' nemohl byt uložen, protože došlo k chybě u uložení dnu ' . $dayId,
						]);
						$this->sendResponse($responseFail);
						return false;
					}
				}
			}
			else {
				$responseFail = new \Nette\Application\Responses\JsonResponse([
					'status' => 'error',
					'code' => Nette\Http\Response::S404_NOT_FOUND,
					'message' => 'Jídelníček ' . $id . ' nemohl byt uložen, protože nebyl nalezen',
				]);
				$this->sendResponse($responseFail);
				return false;
			}

			$responseDone = new \Nette\Application\Responses\JsonResponse([
				'status' => 'ok',
				'code' => Nette\Http\Response::S200_OK,
				'message' => 'Jídelníček byl uložen'
			]);
			$this->sendResponse($responseDone);

		}

	}

	private function getAgeGroup(int $key) {
		switch($key) {
			case 1:
				return '3-6 let';
			case 2:
				return '7-10 let';
			case 3:
				return '11-14 let';
			case 4:
				return '15-18 let';
			default:
				return 'others';
		}
	}
}
