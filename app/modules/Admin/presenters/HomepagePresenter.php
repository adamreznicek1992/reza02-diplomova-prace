<?php

namespace App\AdminModule\Presenters;

use Nette;
use App\Model;
use Tracy\Debugger;
use Nette\Utils\DateTime;

use App\Model\MenuManager;


class HomepagePresenter extends BasePresenter
{

	/** @var MenuManager */
	private $menuManager;

	// database
	protected $database;

	public function __construct(Nette\Database\Context $database, MenuManager $menuManager)
	{
		$this->database = $database;
		$this->menuManager = $menuManager;
	}

	public function renderDefault()
	{
    $this->testIsUserLoggedIn();
		$this->template->actualServerTime = $this->getActualServerTime();
		$this->template->settings = $this->getSettings();
		$this->template->menus = $this->menuManager->getHomepageMenus();
	}
}
