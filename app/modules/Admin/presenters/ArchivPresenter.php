<?php

namespace App\AdminModule\Presenters;

use Nette;
use App\Model;
use Tracy\Debugger;
use Nette\Utils\DateTime;
use Nette\Application\UI\Form;

use App\Model\MenuManager;


class ArchivPresenter extends BasePresenter
{

	/** @var MenuManager */
	private $menuManager;

	// database
	protected $database;

	public function __construct(Nette\Database\Context $database, MenuManager $menuManager)
	{
		$this->database = $database;
		$this->menuManager = $menuManager;
	}

	public function renderDefault(): void
	{
    $this->testIsUserLoggedIn();
		$this->template->actualServerTime = $this->getActualServerTime();
		$this->template->settings = $this->getSettings();

		$this->template->menus = $this->menuManager->getAllMenus();

		$ageGroupKeys = json_decode($this->getSettings()['age_group']);
		$ageGroup = $this->getAgeGroup($ageGroupKeys[0]);

		$this->template->actualMenu = $this->menuManager->getActualMenu($ageGroup);

	}

	public function actionDetailMenu(int $id): void {
		$this->testIsUserLoggedIn();
		$this->template->actualServerTime = $this->getActualServerTime();
		$this->template->settings = $this->getSettings();

		$menu = $this->menuManager->getMenu($id);
		if (!$menu) {
			$this->flashMessage('Jídelníček nebyl nalezen.', 'alert-danger');
			$this->redirect('Archiv:');
		}
	
		$this->template->id = $id;
		$this->template->ageGroups = json_decode($this->getSettings()['age_group']);
	}

	public function actionPdfMenu(int $id, bool $isPrintable): void {
		$this->testIsUserLoggedIn();
		$this->template->actualServerTime = $this->getActualServerTime();
		$this->template->settings = $this->getSettings();

		$menu = $this->menuManager->getMenu($id);
		if (!$menu) {
			$this->flashMessage('Jídelníček nebyl nalezen.', 'alert-danger');
			$this->redirect('Archiv:');
		}
	
		$this->template->id = $id;
		$this->template->isPrintable = $isPrintable;
		$this->template->ageGroups = json_decode($this->getSettings()['age_group']);
	}

	public function actionEditMenu(int $id): void {
		$this->testIsUserLoggedIn();
		$this->template->actualServerTime = $this->getActualServerTime();
		$this->template->settings = $this->getSettings();

		$menu = $this->menuManager->getMenu($id);
		if (!$menu) {
			$this->flashMessage('Jídelníček nebyl nalezen.', 'alert-danger');
			$this->redirect('Archiv:');
		}
	
		$this->template->id = $id;
		$this->template->ageGroups = json_decode($this->getSettings()['age_group']);
	}

	public function actionDetail(int $id, int $ageGroupKey): void {
		
		$this->testIsUserLoggedIn();
		$this->template->actualServerTime = $this->getActualServerTime();
		$this->template->settings = $this->getSettings();

		$menu = $this->menuManager->getMenu($id);
		if (!$menu) {
			$this->flashMessage('Jídelníček nebyl nalezen.', 'alert-danger');
			$this->redirect('Archiv:');
		}

		$ageGroup = $this->getAgeGroup($ageGroupKey);
		$this->template->ageGroup = $ageGroupKey;

		$this->template->actualMenu = $this->menuManager->getActualMenu($ageGroup);
		$this->template->menu = $this->menuManager->getMenuArray($id, $ageGroup);

		$this->template->showLikesComponent = false;
		$this->template->closedAtDates = json_decode($this->getSettings()['closed']);
	}

	public function actionDelete(int $id): void
	{

		$this->testIsUserLoggedIn();
		$this->template->actualServerTime = $this->getActualServerTime();
		$this->template->settings = $this->getSettings();

		$isDelete = $this->menuManager->deleteMenu($id);
		if (!$isDelete) {
			$this->flashMessage('Jídelníček nebyl nalezen.', 'alert-danger');
			$this->redirect('Archiv:');
		}

		$this->flashMessage('Jídelníčky byly odstraněny.', 'alert-success');
		$this->redirect('Archiv:');
	}

	public function actionEditBackground(int $id) {
		
		$this->testIsUserLoggedIn();
		$this->template->actualServerTime = $this->getActualServerTime();
		$this->template->settings = $this->getSettings();

		// get week menu
		$item = $this->menuManager->getMenu($id);
		if (!$item) {
			$this->flashMessage('Jídelníček nebyl nalezen', 'alert-danger');
			$this->redirect('Archiv:');
		}

		$this->template->id = $id;

		// set bg if not null
		if ($item->background) {
			$this['backgroundForm']->setDefaults([
				'background' => $item->background
			]);
		}

	}

	protected function createComponentBackgroundForm(): Form {

		$form = new Form;

		$fileList = [];
		$fileList['none'] = 'Vyberte obrázek';

		$directory = glob(__DIR__ . '/../../../../www/upload/*.{jpg,JPG,jpeg,JPEG,png,PNG,webp,WEBP}', GLOB_BRACE);

		foreach($directory as $key => $filename) {
			if(is_file($filename)){
				$fileList[basename($filename)] = basename($filename);
    	}   
	 	}

		$form->addSelect('background', 'Vyberte obrázek z již existujících:', $fileList);

		$form->addUpload('upload', 'Náhrát nové pozadí:')
			->addRule($form::IMAGE, 'Obrázek musí být JPEG, PNG, GIF or WebP.')
			->addRule($form::MAX_FILE_SIZE, 'Maximální velikost je 1 MB.', 1024 * 1024);

		$form->addSubmit('submit', 'Uložit');

		$form->onSuccess[] = [$this, 'backgroundFormSucceeded'];

		$this->renderAsBootstrapForm($form); //boostrap
		return $form;

	}

	public function backgroundFormSucceeded(Form $form, array $values): void {

		$id = $this->getParameter('id');
		// Debugger::barDump($id);
		// Debugger::barDump($values);

     if ($values['upload']->hasFile()) {

				$soubor = $values['upload'];
				$soubor->move("upload/" . $values['upload']->name);

				// rename if necessary
        // if($soubor->isOk()) {
				// }
				
				// get week menu
				$item = $this->menuManager->getMenu($id);
				if (!$item) {
					$this->flashMessage('Obrázek byl uložen, ale jídelníček nebyl nalezen', 'alert-warning');
        	$this->redirect('Archiv:');
				}

				// update the DB 
				$item->update(
					[
						'background' => $values['upload']->name
					]
				);

        $this->flashMessage('Obrázek byl uložen a nastaven jako pozadí jídelníčku', 'alert-success');
        $this->redirect('Archiv:');

    } else {

			// get week menu
			$item = $this->menuManager->getMenu($id);
			if (!$item) {
				$this->flashMessage('Jídelníček nebyl nalezen', 'alert-danger');
				$this->redirect('Archiv:');
			}

			// update the DB

			$background = ($values['background'] != 'none') ? $values['background'] : null;
			$item->update(
				[
					'background' => $background
				]
			);

			$this->flashMessage('Pozadí jídelníčku bylo nastaveno', 'alert-success');
			$this->redirect('this');

    }
	}

	private function getAgeGroup(int $key) {
		switch($key) {
			case 1:
				return '3-6 let';
			case 2:
				return '7-10 let';
			case 3:
				return '11-14 let';
			case 4:
				return '15-18 let';
			default:
				return 'others';
		}
	}
}
