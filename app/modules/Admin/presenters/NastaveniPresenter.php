<?php

namespace App\AdminModule\Presenters;

use Nette;
use App\Model;
use Tracy\Debugger;
use Nette\Utils\DateTime;
use Nette\Application\UI\Form;


class NastaveniPresenter extends BasePresenter
{

	public function renderDefault()
	{
    $this->testIsUserLoggedIn();
		$this->template->actualServerTime = $this->getActualServerTime();
		$this->template->settings = $this->getSettings();

		$settingsId = 1; // only first settings

		$settings = $this->database->table('settings')->get($settingsId);
		if (!$settings) {
			$this->flashMessage('Nastavení nebylo nalezeno', 'alert-danger');
			$this->redirect('Nastaveni:create');
		}

		$values = $settings->toArray();
		$values['closed'] = json_decode($values['closed']);
		$values['age_group'] = json_decode($values['age_group']);
		$this['settingsForm']->setDefaults($values);

	}

	public function actionCreate()
	{
		$this->testIsUserLoggedIn();
		$this->template->actualServerTime = $this->getActualServerTime();
		$this->template->settings = $this->getSettings();
	}

	public function actionEdit(int $settingsId): void
	{

		$this->testIsUserLoggedIn();
		$this->template->actualServerTime = $this->getActualServerTime();
		$this->template->settings = $this->getSettings();

		$settings = $this->database->table('settings')->get($settingsId);
		if (!$settings) {
			$this->flashMessage('Nastavení nebylo nalezeno, je potřeba ho vytvořit.', 'alert-danger');
			$this->redirect('Nastaveni:');
		}

		$values = $settings->toArray();
		$values['closed'] = json_decode($values['closed']);
		$values['age_group'] = json_decode($values['age_group']);
		$this['settingsForm']->setDefaults($values);
	}
	
	private function createRange($start, $end, $format = 'j. n. Y') {
    $start  = new DateTime($start);
    $end    = new DateTime($end);
    $invert = $start > $end;

    $dates = array();
    $dates[$start->format('Y-m-d')] = $start->format($format);
    while ($start != $end) {
        $start->modify(($invert ? '-' : '+') . '1 day');
        $dates[$start->format('Y-m-d')] = $start->format($format);
    }
    return $dates;
	}

	protected function createComponentSettingsForm(): Form
	{

		$form = new Form; // means Nette\Application\UI\Form

		$form->addText('school_name', 'Název školy:')
				 ->setAttribute('placeholder', "Název se zobrazí v patičce a na jídelníčku")
				 ->setRequired();

		$form->addText('school_url', 'Webové stránky školy:')
		     ->setAttribute('placeholder', "Odkaz na stránky školy z patičky a jídelníčku")
				 ->setRequired();
		
		$form->addText('email', 'Emailová adresa jídelny:')
		     ->setAttribute('placeholder', "Emailová adresa se zobrazí u jídelníčku")
				 ->setRequired();

		$form->addText('phone', 'Telefonní číslo:')
		     ->setAttribute('placeholder', "Telefonní číslo se zobrazí u jídelníčku")
				 ->setRequired();

		$form->addText('cookers', 'Kuchařky:')
		     ->setAttribute('placeholder', "Příjmenní kuchařek oddělte čárkou");

		$form->addText('supervisor', 'Vedoucí jídelny:')
		     ->setAttribute('placeholder', "Jméno se zobrazí u jídelníčku")
				 ->setRequired();

		$form->addText('cookbook_url', 'Odkaz na oblíbenou online kuchařku:')
				 ->setAttribute('placeholder', "např. https://www.jidelny.cz/recepty.aspx")
				 ->setRequired();

		$now = date('Y-m-d');
		$dates = $this->createRange('2020-10-01', date('Y-m-d', strtotime('+ 3 months', strtotime($now))));
		$form->addMultiSelect('closed', 'Dny, kdy se nevaří (pro výběr držte CTRL):', $dates);

		$ageGroups = [
			1 => 'Mateřská škola',
			2 => 'Základní škola, první stupeň',
			3 => 'Základní škola, druhý stupeň',
			4 => 'Střední škola',
			5 => 'Cizí strávníci',
		];

		$form->addMultiSelect('age_group', 'Jídelna vaří pro:', $ageGroups)
			->setDefaultValue(2)
			->setRequired();

		$days = [
			'Monday' => 'Pondělí',
			'Tuesday' => 'Úterý',
			'Wednesday' => 'Středa',
			'Thursday' => 'Čtvrtek',
			'Friday' => 'Pátek',
			'Saturday' => 'Sobota',
			'Sunday' => 'Neděle',
		];
		$form->addSelect('menu_actived_at_day', 'Den v týdnu, kdy se má spustit nový jídelníček:', $days)
			->setDefaultValue('Friday');

		$form->addText('menu_actived_at_time', 'Přesný čas, kdy se má spustit nový jídelníček:')
			->setHtmlType('time');

		
		$twoMainDishes = [
			1 => 'Ano',
			0 => 'Ne'
		];

		$form->addRadioList('has_two_main_dishes', 'Nabízet ze dvou hlavních jídel?', $twoMainDishes);

		$form->addSubmit('settingsSubmit', 'Uložit');

		$form->onSuccess[] = [$this, 'settingsFormSucceeded'];

		$this->renderAsBootstrapForm($form); //boostrap
		return $form;
	}

	public function settingsFormSucceeded(Form $form, array $values): void
	{

		// $this->testIsUserLoggedIn();
		// $this->template->actualServerTime = $this->getActualServerTime();
		// $this->template->settings = $this->getSettings();

		// $settingsId = $this->getParameter('settingId');

		// closed dates into json 
		$values['closed'] = json_encode($values['closed']);

		// age groups
		$values['age_group'] = json_encode($values['age_group']);

		$settingsId = 1;
		if ($settingsId) {
			$settings = $this->database->table('settings')->get($settingsId);
			$settings->update($values);
		}
		else {
			$settings = $this->database->table('settings')->insert($values);
		}
 
		// $this->database->table('settings')->insert([
		// 	'school_name' => $values->settingsSchoolName,
		// 	'school_url' => $values->settingsSchoolUrl,
		// 	'' => $values->settingsCookbookUrl,
		// ]);

		$this->flashMessage('Nastavení bylo uloženo', 'alert-success');
		$this->redirect('Nastaveni:');
	}

}
