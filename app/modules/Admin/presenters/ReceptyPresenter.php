<?php

namespace App\AdminModule\Presenters;

use Nette;
use App\Model;
use Tracy\Debugger;
use Nette\Utils\DateTime;
use Nette\Application\UI\Form;
use Nette\Application\IResponse;

use App\Model\DishManager;
use App\Model\SideManager;
use App\Model\TypeManager;
use App\Model\CategoryManager;
use App\Model\AllergenManager;


class ReceptyPresenter extends BasePresenter
{

	/** @var DishManager */
	private $dishManager;

	/** @var SideManager */
	private $sideManager;

	/** @var TypeManager */
	private $typeManager;

	/** @var CategoryManager */
	private $categoryManager;

	/** @var AllergenManager */
	private $allergenManager;

	// database
	protected $database;

	// categories
	// public $categoriesMainDish;

	public function __construct(Nette\Database\Context $database, DishManager $dishManager, SideManager $sideManager, TypeManager $typeManager, CategoryManager $categoryManager, AllergenManager $allergenManager)
	{
		$this->database = $database;
		$this->dishManager = $dishManager;
		$this->sideManager = $sideManager;
		$this->typeManager = $typeManager;
		$this->categoryManager = $categoryManager;
		$this->allergenManager = $allergenManager;
		// $this->categoriesMainDish = 1;
	}

	public function renderDefault()
	{
    $this->testIsUserLoggedIn();
		$this->template->actualServerTime = $this->getActualServerTime();
		$this->template->settings = $this->getSettings();

		// $dishes = $this->dishManager->getAllDishes()->limit(5);
		$dishes = $this->dishManager->getAllDishes();
		$this->template->dishes = $dishes;
	}

	public function actionPridatHlavniChod()
	{
		$this->testIsUserLoggedIn();
		$this->template->actualServerTime = $this->getActualServerTime();
		$this->template->settings = $this->getSettings();
		$this->template->ageGroups = json_decode($this->getSettings()['age_group']);
	}

	public function actionPridatPolevku()
	{
		$this->testIsUserLoggedIn();
		$this->template->actualServerTime = $this->getActualServerTime();
		$this->template->settings = $this->getSettings();
		$this->template->ageGroups = json_decode($this->getSettings()['age_group']);
	}

	public function actionPridatNapoj()
	{
		$this->testIsUserLoggedIn();
		$this->template->actualServerTime = $this->getActualServerTime();
		$this->template->settings = $this->getSettings();
	}

	public function actionPridatOvoce()
	{
		$this->testIsUserLoggedIn();
		$this->template->actualServerTime = $this->getActualServerTime();
		$this->template->settings = $this->getSettings();
	}

	public function actionPridatSalat()
	{
		$this->testIsUserLoggedIn();
		$this->template->actualServerTime = $this->getActualServerTime();
		$this->template->settings = $this->getSettings();
	}

	public function actionPridatMoucnikDezert()
	{
		$this->testIsUserLoggedIn();
		$this->template->actualServerTime = $this->getActualServerTime();
		$this->template->settings = $this->getSettings();
	}

	public function actionPridatOstatni()
	{
		$this->testIsUserLoggedIn();
		$this->template->actualServerTime = $this->getActualServerTime();
		$this->template->settings = $this->getSettings();
	}

	// Forms
	// Hlavní jídlo
	protected function createComponentMainDishForm(): Form
	{

		$form = new Form; // means Nette\Application\UI\Form

		$typeId = $this->typeManager->getId('Hlavní chod');
		
		// save type
		$form->addHidden('type_id', $typeId);

		// categories
		$categories = $this->typeManager->getCategoriesArray($typeId);
		// Debugger::barDump($categories);

		// sides
		$sides = $this->sideManager->getAllSidesArray();
		// Debugger::barDump($sides);

		// allergens
		$allergens = $this->allergenManager->getAllAllergensArray();
		// Debugger::barDump($allergens);

		$form->addText('name', 'Název:')
				 ->setAttribute('placeholder', "Název, který se zobrazí na jídelníčku")
				 ->addRule(Form::MAX_LENGTH, 'Název je příliš dlouhý', 100)
				 ->setRequired();

		$form->addTextArea('recipe', 'Recept:')
		->setAttribute('placeholder', "Recept nebo postup při přípravě pokrmu")
		     ->addRule(Form::MAX_LENGTH, 'Recept je příliš dlouhý', 10000);

		$form->addSelect('category_id', 'Kategorie:')
				 ->setItems($categories, true)
				 ->setPrompt('Zvolte kategorii')
				 ->setRequired();

		$form->addSelect('side_id', 'Příloha:')
				 ->setItems($sides, true)
				 ->setPrompt('Zvolte přílohu');

		$form->addCheckboxList('allergens', 'Alergeny:')
				 ->setItems($allergens, true);

		//  basket
		$ageGroups = json_decode($this->getSettings()['age_group']);
		$basketKeys = ['meat' => 'Maso', 'fish' => 'Ryby', 'legumes' => 'Luštěniny', 'cereals' => 'Obiloviny', 'potatoes' => 'Brambory', 'vegetables' => 'Zelenina', 'fat' => 'Tuk', 'salt' => 'Sůl'];
		foreach ($basketKeys as $key => $value) {
			foreach ($ageGroups as $ag) {
				if ($ag < 5) {
					$form->addText($key . $ag, $value . ' (g)')
						 ->setDefaultValue('0')
						 ->addRule(Form::FLOAT, 'Hodnota musí být číslo')
						 ->addRule(Form::MIN, 'Hodnota musí být kladná', 0);
				}
			}
		}

		$form->addSubmit('submit', 'Uložit recept');

		$form->onSuccess[] = [$this, 'dishFormSucceeded'];

		$this->renderAsBootstrapForm($form); //boostrap
		return $form;
	}

	// Polévka
	protected function createComponentSoupForm(): Form
	{

		$form = new Form; // means Nette\Application\UI\Form

		$typeId = $this->typeManager->getId('Polévka');

		// save type
		$form->addHidden('type_id', $typeId);

		// categories
		$categories = $this->typeManager->getCategoriesArray($typeId);
		// Debugger::barDump($categories);

		// allergens
		$allergens = $this->allergenManager->getAllAllergensArray();
		// Debugger::barDump($allergens);

		$form->addText('name', 'Název:')
				 ->setAttribute('placeholder', "Název, který se zobrazí na jídelníčku")
				 ->addRule(Form::MAX_LENGTH, 'Název je příliš dlouhý', 100)
				 ->setRequired();

		$form->addTextArea('recipe', 'Recept:')
		->setAttribute('placeholder', "Recept nebo postup při přípravě pokrmu")
		     ->addRule(Form::MAX_LENGTH, 'Recept je příliš dlouhý', 10000);

		$form->addSelect('category_id', 'Kategorie:')
				 ->setItems($categories, true)
				 ->setPrompt('Zvolte kategorii')
				 ->setRequired();

		$form->addCheckboxList('allergens', 'Alergeny:')
				 ->setItems($allergens, true);

		//  basket
		$ageGroups = json_decode($this->getSettings()['age_group']);
		$basketKeys = ['meat' => 'Maso', 'fish' => 'Ryby', 'legumes' => 'Luštěniny', 'cereals' => 'Obiloviny', 'potatoes' => 'Brambory', 'vegetables' => 'Zelenina', 'fat' => 'Tuk', 'salt' => 'Sůl'];
		foreach ($basketKeys as $key => $value) {
			foreach ($ageGroups as $ag) {
				if ($ag < 5) {
					$form->addText($key . $ag, $value . ' (g)')
						 ->setDefaultValue('0')
						 ->addRule(Form::FLOAT, 'Hodnota musí být číslo')
						 ->addRule(Form::MIN, 'Hodnota musí být kladná', 0);
				}
			}
		}

		$form->addSubmit('submit', 'Uložit recept');

		$form->onSuccess[] = [$this, 'dishFormSucceeded'];

		$this->renderAsBootstrapForm($form); //boostrap
		return $form;
	}

	// Nápoj
	protected function createComponentDrinkForm(): Form
	{

		$form = new Form; // means Nette\Application\UI\Form

		$typeId = $this->typeManager->getId('Nápoj');

		// save type
		$form->addHidden('type_id', $typeId);

		// categories
		$categories = $this->typeManager->getCategoriesArray($typeId);
		// Debugger::barDump($categories);

		// allergens
		$allergens = $this->allergenManager->getAllAllergensArray();
		// Debugger::barDump($allergens);

		$form->addText('name', 'Název:')
				 ->setAttribute('placeholder', "Název, který se zobrazí na jídelníčku")
				 ->addRule(Form::MAX_LENGTH, 'Název je příliš dlouhý', 100)
				 ->setRequired();

		$form->addTextArea('recipe', 'Recept:')
		->setAttribute('placeholder', "Recept nebo postup při přípravě nápoje")
		     ->addRule(Form::MAX_LENGTH, 'Recept je příliš dlouhý', 10000);

		$form->addSelect('category_id', 'Kategorie:')
				 ->setItems($categories, true)
				 ->setPrompt('Zvolte kategorii')
				 ->setRequired();

		$form->addCheckboxList('allergens', 'Alergeny:')
				 ->setItems($allergens, true);

		$form->addSubmit('submit', 'Uložit recept');

		$form->onSuccess[] = [$this, 'dishFormSucceeded'];

		$this->renderAsBootstrapForm($form); //boostrap
		return $form;
	}

	// Salát
	protected function createComponentSaladForm(): Form
	{

		$form = new Form; // means Nette\Application\UI\Form

		// save type
		$form->addHidden('type_id', $this->typeManager->getId('Salát'));

		// allergens
		$allergens = $this->allergenManager->getAllAllergensArray();
		// Debugger::barDump($allergens);

		$form->addText('name', 'Název:')
				 ->setAttribute('placeholder', "Název, který se zobrazí na jídelníčku")
				 ->addRule(Form::MAX_LENGTH, 'Název je příliš dlouhý', 100)
				 ->setRequired();

		$form->addTextArea('recipe', 'Recept:')
				 ->setAttribute('placeholder', "Recept nebo postup při přípravě salátu")
				 ->addRule(Form::MAX_LENGTH, 'Recept je příliš dlouhý', 10000);


		$form->addCheckboxList('allergens', 'Alergeny:')
				 ->setItems($allergens, true);

		$form->addSubmit('submit', 'Uložit recept');

		$form->onSuccess[] = [$this, 'dishFormSucceeded'];

		$this->renderAsBootstrapForm($form); //boostrap
		return $form;
	}

	// Moučník/dezert
	protected function createComponentDessertForm(): Form
	{

		$form = new Form; // means Nette\Application\UI\Form

		// save type
		$form->addHidden('type_id', $this->typeManager->getId('Moučník/dezert'));

		// allergens
		$allergens = $this->allergenManager->getAllAllergensArray();
		// Debugger::barDump($allergens);

		$form->addText('name', 'Název:')
				 ->setAttribute('placeholder', "Název, který se zobrazí na jídelníčku")
				 ->addRule(Form::MAX_LENGTH, 'Název je příliš dlouhý', 100)
				 ->setRequired();

		$form->addTextArea('recipe', 'Recept:')
				 ->setAttribute('placeholder', "Recept nebo postup při přípravě moučníku/dezertu")
				 ->addRule(Form::MAX_LENGTH, 'Recept je příliš dlouhý', 10000);

		$form->addCheckboxList('allergens', 'Alergeny:')
				 ->setItems($allergens, true);

		$form->addSubmit('submit', 'Uložit recept');

		$form->onSuccess[] = [$this, 'dishFormSucceeded'];

		$this->renderAsBootstrapForm($form); //boostrap
		return $form;
	}

	// Svačina
	protected function createComponentOthersForm(): Form
	{

		$form = new Form; // means Nette\Application\UI\Form

		$typeId = $this->typeManager->getId('Ostatní');

		// save type
		$form->addHidden('type_id', $typeId);

		// categories
		$categories = $this->typeManager->getCategoriesArray($typeId);
		// Debugger::barDump($categories);

		// allergens
		$allergens = $this->allergenManager->getAllAllergensArray();
		// Debugger::barDump($allergens);

		$form->addText('name', 'Název:')
					->setAttribute('placeholder', "Název, který se zobrazí na jídelníčku")
					->addRule(Form::MAX_LENGTH, 'Název je příliš dlouhý', 100)
					->setRequired();

		$form->addTextArea('recipe', 'Recept:')
					->setAttribute('placeholder', "Recept nebo postup při přípravě položky")
					->addRule(Form::MAX_LENGTH, 'Recept je příliš dlouhý', 10000);

		$form->addSelect('category_id', 'Kategorie:')
					->setItems($categories, true)
					->setPrompt('Zvolte kategorii')
					->setRequired();

		$form->addCheckboxList('allergens', 'Alergeny:')
					->setItems($allergens, true);

		$form->addSubmit('submit', 'Uložit recept');

		$form->onSuccess[] = [$this, 'dishFormSucceeded'];

		$this->renderAsBootstrapForm($form); //boostrap
		return $form;
	}

	public function actionEdit(int $id): void
	{

		$this->testIsUserLoggedIn();
		$this->template->actualServerTime = $this->getActualServerTime();
		$this->template->settings = $this->getSettings();
		$this->template->ageGroups = json_decode($this->getSettings()['age_group']);

		$item = $this->dishManager->getDish($id);
		if (!$item) {
			$this->flashMessage('Recept nebyl nalezen.', 'alert-danger');
			$this->redirect('Recepty:');
		}

		// Debugger::barDump($item);

		$typeId = $item['type_id'];
		// Debugger::barDump($typeId);

		$this->template->type = $this->typeManager->getName($typeId);

		switch ($this->typeManager->getName($typeId)) {
			case 'Hlavní chod':
				$this['mainDishForm']->setDefaults($item);
				break;
			case 'Polévka':
				$this['soupForm']->setDefaults($item);
				break;
			case 'Nápoj':
				$this['drinkForm']->setDefaults($item);
				break;
			case 'Ovoce':
				$this['fruitForm']->setDefaults($item);
				break;
			case 'Salát':
				$this['saladForm']->setDefaults($item);
				break;
			case 'Moučník/dezert':
				$this['dessertForm']->setDefaults($item);
				break;
			case 'Ostatní':
				$this['othersForm']->setDefaults($item);
				break;
			default:
				$this->flashMessage('Chyba, takový typ pokrmu neexistuje.', 'alert-danger');
				$this->redirect('Recepty:');
		}
	}

	public function dishFormSucceeded(Form $form, array $values): void
	{
		$id = $this->getParameter('id');
		// if edit
		if ($id) {
			$this->dishManager->updateDish($id, $values);
			$this->flashMessage('Recept byl uložen', 'alert-success');
			$this->redirect('Recepty:');
		}
		// if create
		else {
			$this->dishManager->saveDish($values);
			$this->flashMessage('Recept byl uložen', 'alert-success');
			$this->redirect('this');
		}
	}

	public function actionDelete(int $id): void
	{

		$this->testIsUserLoggedIn();
		$this->template->actualServerTime = $this->getActualServerTime();
		$this->template->settings = $this->getSettings();

		$isDelete = $this->dishManager->deleteDish($id);
		if (!$isDelete) {
			$this->flashMessage('Recept nebyl nalezen.', 'alert-danger');
			$this->redirect('Recepty:');
		}

		$this->flashMessage('Recept byl odstraněn.', 'alert-success');
		$this->redirect('Recepty:');
	}

	// set dish status via toggle button (ajax)
	public function actionSetStatus() {

		$this->testIsUserLoggedIn();

		$httpRequest = $this->getHttpRequest();
		$httpResponse = $this->getHttpResponse();

		$id = $httpRequest->getPost('id');
		$status = ($httpRequest->getPost('enabled') === "true") ? 1 : 0;

		if ($id) {
			$result = $this->dishManager->setEnableStatus($id, $status);
			if($result == 1) {
				$httpResponse->setCode(Nette\Http\Response::S200_OK);
				
				$response = new \Nette\Application\Responses\JsonResponse([
					'status' => 'ok',
					'code' => Nette\Http\Response::S200_OK,
					'message' => 'Status jídla ID ' . $id . ' byl nastaven'
				]);
				$this->sendResponse($response);
			}
			else {
				$httpResponse->setCode(Nette\Http\Response::S200_OK);
					
				$response = new \Nette\Application\Responses\JsonResponse([
					'status' => 'error',
					'code' => Nette\Http\Response::S404_NOT_FOUND,
					'message' => 'Jídlo ID ' . $id . ' nebylo nalezeno'
				]);
				$this->sendResponse($response);
			}
		}
	}
}
