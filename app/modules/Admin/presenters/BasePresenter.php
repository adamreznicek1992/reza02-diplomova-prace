<?php

namespace App\AdminModule\Presenters;

use Nette;
use App\Model;


/**
 * Base presenter for all application presenters.
 */
abstract class BasePresenter extends \App\Presenters\BasePresenter
{
    
    protected function testIsUserLoggedIn() {
        if (!$this->getUser()->isLoggedIn()) {
        	$this->flashMessage('Musíte se nejdříve přihlásit!', 'alert-info');
            $this->redirect('Sign:in');
        }
    }

    protected function getTitlePageConfig() {
    	//get config
        $config = \Nette\Neon\Neon::decode(
            file_get_contents( $this->context->parameters['appDir'] . '/config/app.neon')
        );

        return $config['titlePage'];
    }

}
