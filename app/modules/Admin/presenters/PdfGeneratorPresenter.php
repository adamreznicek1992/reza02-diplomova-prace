<?php

namespace App\AdminModule\Presenters;

use Nette;
use App\Model;
use Tracy\Debugger;
use Nette\Utils\DateTime;

use App\Model\MenuManager;

// mPDF
use Mpdf\Mpdf as mPDF;
use Nette\Application\UI\ITemplateFactory;


class PdfGeneratorPresenter extends BasePresenter
{

	/** @var ITemplateFactory @inject */
	public $templateFactory;

	/** @var MenuManager */
	private $menuManager;

	// database
	protected $database;

	public function __construct(Nette\Database\Context $database, MenuManager $menuManager)
	{
		$this->database = $database;
		$this->menuManager = $menuManager;
	}

	public function renderDefault()
	{
    $this->testIsUserLoggedIn();
		$this->template->actualServerTime = $this->getActualServerTime();
		$this->template->settings = $this->getSettings();
	}

	public function actionExportPdf(int $id, bool $isPrintable, $ageGroupKey)
	{

		// test if user logged
		$this->testIsUserLoggedIn();

		$ageGroup = $this->getAgeGroup($ageGroupKey);

		// set PDF footer
		$footer = array (
			'odd' => array (
				'L' => array (
						'content' => $this->getSettings()['phone'], // print phone number
						'font-size' => 9
				),
				'C' => array (
						'content' => 'Změna jídelníčku vyhrazena.<br>Strava je určena k&nbsp;okamžité spotřebě.',
						'font-size' => 9
				),
				'R' => array (
						'content' => $this->getSettings()['email'], // print email
						'font-size' => 9
				),
				'line' => 1,
			),
		);

		// get the menu
		$item = $this->menuManager->getMenuArray($id, $ageGroup);
		if (!$item) {
			$this->flashMessage('Jídelníček nenalezen', 'alert-danger');
			$this->redirect('Archiv:');
		}

		// set template
		$t = $this->templateFactory->createTemplate();
		$t->setFile(__DIR__ . '/../templates/PdfGenerator/exportPdf.latte');
		$t->settings = $this->getSettings();
		$t->menu = $item;
		$t->closedAtDates = json_decode($this->getSettings()['closed']);
		$t->ageGroup = $ageGroupKey;

		// set mPDF
		$pdf = new mPDF();
		$pdf->ignore_invalid_utf8 = true;
		$pdf->setFooter($footer);
		$pdf->WriteHTML($t);

		// check if print or save PDF
		if ($isPrintable) {
			$pdf->Output(); // open PDF for print
		}
		else {

			$shortcut = 'jine';
			if ($ageGroupKey == 1) {
				$shortcut = 'ms';
			}

			if ($ageGroupKey == 2 || $ageGroupKey == 3) {
				$shortcut = 'zs';
			}

			if ($ageGroupKey == 4) {
				$shortcut = 'ss';
			}

			if ($ageGroupKey == 5) {
				$shortcut = 'cs';
			}

			$pdf->Output('jidelnicek-' . $shortcut . '-' . $id . '.pdf', 'D'); // save PDF
		}

		$this->redirect('this');
	}

	private function getAgeGroup(int $key) {
		switch($key) {
			case 1:
				return '3-6 let';
			case 2:
				return '7-10 let';
			case 3:
				return '11-14 let';
			case 4:
				return '15-18 let';
			default:
				return 'others';
		}
	}
}
