<?php

namespace App\Forms;

use Nette;
use Nette\Application\UI\Form;
use Nette\Security\User;


class SignFormFactory
{
	/** @var FormFactory */
	private $factory;

	/** @var User */
	private $user;


	public function __construct(Nette\Database\Context $database, FormFactory $factory, User $user)
	{
		$this->factory = $factory;
		$this->user = $user;
		$this->database = $database;
	}


	/**
	 * @return Form
	 */
	public function create()
	{

		$form = $this->factory->create();
		$form->addText('username', 'Jméno:')
			->setRequired('Vyplňte vaše uživatelské jméno.');

		$form->addPassword('password', 'Heslo:')
			->setRequired('Zadejte heslo.');

		$form->addCheckbox('remember', 'Zůstat přihlášen');

		$form->addSubmit('send', 'Přihlásit');

		$form->onSuccess[] = array($this, 'formSucceeded');
		return $form;
	}


	public function formSucceeded(Form $form, $values)
	{
		if ($values->remember) {
			$this->user->setExpiration('7 days');
		} else {
			$this->user->setExpiration('30 minutes');
		}

		try {
			$this->user->login($values->username, $values->password);
		} catch (Nette\Security\AuthenticationException $e) {
			$form->addError('Zadané jméno a heslo není správné.');
		}
	}

}
