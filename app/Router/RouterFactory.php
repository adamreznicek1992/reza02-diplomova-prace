<?php

declare(strict_types=1);

namespace App\Router;

use Nette;
use Nette\Application\Routers\RouteList;
use Nette\Application\Routers\Route;


final class RouterFactory
{
	use Nette\StaticClass;

	public static function createRouter(): RouteList
	{
		$router = new RouteList;

		// admin module routes
		$admin = new RouteList('Admin');
		$admin[] = new Route('admin/prihlasit', 'Sign:in');
		$admin[] = new Route('admin/odhlasit', 'Sign:out');

		$admin[] = new Route('admin/archiv/detail-jidelnicku/<id>', 'Archiv:detailMenu');
		$admin[] = new Route('admin/archiv/detail-jidelnicku/detail/<id>/<ageGroupKey>', 'Archiv:detail');
		
		$admin[] = new Route('admin/archiv/pdf/<id>/<isPrintable>', 'Archiv:pdfMenu');
		$admin[] = new Route('admin/archiv/pdf/generator/<id>/<isPrintable>/<ageGroupKey>', 'PdfGenerator:exportPdf');		

		$admin[] = new Route('admin/archiv/zmena-pozadi-jidelnicku/<id>', 'Archiv:editBackground');

		$admin[] = new Route('admin/archiv/uprava-jidelnicku/<id>', 'Archiv:editMenu');
		$admin[] = new Route('admin/archiv/uprava-jidelnicku/uprava/<id>/<ageGroupKey>', 'SestaveniJidelnicku:edit');

		$admin[] = new Route('admin/recepty/editace-jidla/<id>', 'Recepty:edit');
		
		$admin[] = new Route('admin/rucni-sestaveni-jidelnicku/<ageGroupKey>/<empty>', 'SestaveniJidelnicku:menuBuilder');
		$admin[] = new Route('admin/automaticke-sestaveni-jidelnicku/<ageGroupKey>', 'SestaveniJidelnicku:menuBuilder');

		$admin[] = new Route('admin/recepty/status-aktivity', 'Recepty:setStatus');
		$admin[] = new Route('admin/<presenter>/<action>[/<id>]', 'Homepage:default');
		$router[] = $admin;

		// front module routes
		$front = new RouteList('Front');
		$front[] = new Route('/minuly-tyden', 'Homepage:previous');
		$front[] = new Route('/like', 'Homepage:like');
		$front[] = new Route('/dislike', 'Homepage:dislike');

		// menu routes
		$front[] = new Route('/aktualni-jidelnicek/materska-skola', 'Homepage:detail1');
		$front[] = new Route('/aktualni-jidelnicek/zakladni-skola', 'Homepage:detail23');
		$front[] = new Route('/aktualni-jidelnicek/stredni-skola', 'Homepage:detail4');
		$front[] = new Route('/aktualni-jidelnicek/cizi-stravnici', 'Homepage:detail5');

		$front[] = new Route('/minuly-jidelnicek/materska-skola', 'Homepage:previous1');
		$front[] = new Route('/minuly-jidelnicek/zakladni-skola', 'Homepage:previous23');
		$front[] = new Route('/minuly-jidelnicek/stredni-skola', 'Homepage:previous4');
		$front[] = new Route('/minuly-jidelnicek/cizi-stravnici', 'Homepage:previous5');

		$front[] = new Route('/aktualni-jidelnicek/materska-skola/pdf', 'PdfGeneratorFront:exportPdf1');
		$front[] = new Route('/aktualni-jidelnicek/zakladni-skola/pdf', 'PdfGeneratorFront:exportPdf23');
		$front[] = new Route('/aktualni-jidelnicek/stredni-skola/pdf', 'PdfGeneratorFront:exportPdf4');
		$front[] = new Route('/aktualni-jidelnicek/cizi-stravnici/pdf', 'PdfGeneratorFront:exportPdf5');
		

		$front[] = new Route('<presenter>/<action>[/<id>]', 'Homepage:default');
		$router[] = $front;

		// default
		$router[] = new Route('<presenter>/<action>[/<id>]', 'Homepage:default');
		return $router;
	}
}
