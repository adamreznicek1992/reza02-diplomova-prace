<?php

namespace App\Model;

use Nette;
use Tracy\Debugger;

class BasketManager
{
	use Nette\SmartObject;

	/** @var Nette\Database\Context */
	private $database;

	public function __construct(Nette\Database\Context $database)
	{
		$this->database = $database;
	}

	public function getBasket(int $dishId, int $ageGroupKey) {

		$ageGroup = $this->getAgeGroup($ageGroupKey);

		$item = null;
		$relDishBaskets = $this->database->table('relation_dish_basket')->where('dish_id', $dishId); 
		foreach ($relDishBaskets as $r) {
			if ($r->basket->age_group == $ageGroup) {
				$item = $r->basket;
				break;
			}
		}

		if (!$item) {
			return null;
		}
		
		return $item;
	}

	public function updateBasket(object $basket, array $values, string $ageGroupKey)
	{
		if ($basket && $values && $ageGroupKey) {

			$ageGroup = $this->getAgeGroup($ageGroupKey);

			$basketValues = [
				'meat' => $values['meat' . $ageGroupKey],
				'fish' => $values['fish' . $ageGroupKey],
				'legumes' => $values['legumes' . $ageGroupKey],
				'cereals' => $values['cereals' . $ageGroupKey],
				'potatoes' => $values['potatoes' . $ageGroupKey],
				'vegetables' => $values['vegetables' . $ageGroupKey],
				'fat' => $values['fat' . $ageGroupKey],
				'salt' => $values['salt' . $ageGroupKey],
				'age_group' => $ageGroup,
			];
			
			$basket->update($basketValues);

			return true;
		}

		return false;
	}

	public function saveBaskets(array $values, int $dishId)
	{
		if ($values && $dishId) {

			foreach ($values as $ageGroupKey => $b) {

				$ageGroup = $this->getAgeGroup($ageGroupKey);

				$basketValues = [
					'meat' => $values[$ageGroupKey]['meat'],
					'fish' => $values[$ageGroupKey]['fish'],
					'legumes' => $values[$ageGroupKey]['legumes'],
					'cereals' => $values[$ageGroupKey]['cereals'],
					'potatoes' => $values[$ageGroupKey]['potatoes'],
					'vegetables' => $values[$ageGroupKey]['vegetables'],
					'fat' => $values[$ageGroupKey]['fat'],
					'salt' => $values[$ageGroupKey]['salt'],
					'age_group' => $ageGroup,
				];

				$basket = $this->database->table('basket')->insert($basketValues);
				$this->database->table('relation_dish_basket')->insert([
					'dish_id' => $dishId,
					'basket_id' => $basket->id
				]);
			}

			return true;
		}

		return false;
	}

	public function saveBasket(array $values, int $dishId, int $ageGroupKey)
	{
		if ($values && $dishId && $ageGroupKey) {

			$ageGroup = $this->getAgeGroup($ageGroupKey);

			$basketValues = [
				'meat' => $values['meat' . $ageGroupKey],
				'fish' => $values['fish' . $ageGroupKey],
				'legumes' => $values['legumes' . $ageGroupKey],
				'cereals' => $values['cereals' . $ageGroupKey],
				'potatoes' => $values['potatoes' . $ageGroupKey],
				'vegetables' => $values['vegetables' . $ageGroupKey],
				'fat' => $values['fat' . $ageGroupKey],
				'salt' => $values['salt' . $ageGroupKey],
				'age_group' => $ageGroup,
			];

			$basket = $this->database->table('basket')->insert($basketValues);
			$this->database->table('relation_dish_basket')->insert([
				'dish_id' => $dishId,
				'basket_id' => $basket->id
			]);

			return true;
		}

		return false;
	}

	public function addBasketArray($basket, $array, $ageGroupKey) {
		$array['meat' . $ageGroupKey] = $basket['meat'];
		$array['fish' . $ageGroupKey] = $basket['fish'];
		$array['legumes' . $ageGroupKey] = $basket['legumes'];
		$array['cereals' . $ageGroupKey] = $basket['cereals'];
		$array['potatoes' . $ageGroupKey] = $basket['potatoes'];
		$array['vegetables' . $ageGroupKey] = $basket['vegetables'];
		$array['fat' . $ageGroupKey] = $basket['fat'];
		$array['salt' . $ageGroupKey] = $basket['salt'];

		return $array;
	}

	public function addBasketObject($basket, $array, $ageGroupKey) {

		$a = [];
		$a['meat'] = $basket['meat'];
		$a['fish'] = $basket['fish'];
		$a['legumes'] = $basket['legumes'];
		$a['cereals'] = $basket['cereals'];
		$a['potatoes'] = $basket['potatoes'];
		$a['vegetables'] = $basket['vegetables'];
		$a['fat'] = $basket['fat'];
		$a['salt'] = $basket['salt'];

		$array['basket' . $ageGroupKey] = $a;

		return $array;
	}

	public function removeBaskets(int $dishId) {
		$relDishBaskets = $this->database->table('relation_dish_basket')->where('dish_id', $dishId); 
		foreach($relDishBaskets as $r) {
			$b = $r->basket;
			$r->delete();
			$b->delete();
		}
	}

	private function getAgeGroup(int $key) {
		switch($key) {
			case 1:
				return '3-6 let';
			case 2:
				return '7-10 let';
			case 3:
				return '11-14 let';
			case 4:
				return '15-18 let';
			default:
				return null;
		}
	}
}