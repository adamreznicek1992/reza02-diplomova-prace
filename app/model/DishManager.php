<?php

namespace App\Model;

use Nette;
use Tracy\Debugger;

use App\Model\AllergenManager;
use App\Model\BasketManager;

class DishManager
{
	use Nette\SmartObject;

	/** @var AllergenManager */
	private $allergenManager;

	/** @var BasketManager */
	private $basketManager;

	/** @var Nette\Database\Context */
	private $database;

	public function __construct(Nette\Database\Context $database, AllergenManager $allergenManager, BasketManager $basketManager)
	{
		$this->database = $database;
		$this->allergenManager = $allergenManager;
		$this->basketManager = $basketManager;
	}

	public function getAllDishes()
	{
		return $this->database->table('dish')
			->where('created_at < ', new \DateTime)
			->order('created_at DESC');
	}

	public function saveDish(array $values)
	{

		Debugger::barDump($values);

		// save the basket
		$basket = null;
		$ageGroups = $this->getAgeGroups();
		if ($values['type_id'] == 1 || $values['type_id'] == 2) {

			$basket = [];

			$basketKeys = ['meat' => 'Maso', 'fish' => 'Ryby', 'legumes' => 'Luštěniny', 'cereals' => 'Obiloviny', 'potatoes' => 'Brambory', 'vegetables' => 'Zelenina', 'fat' => 'Tuk', 'salt' => 'Sůl'];
			foreach ($ageGroups as $ag) {
				if ($ag < 5) {
					$basket[$ag] = [];
					foreach ($basketKeys as $key => $value) {
						$basket[$ag][$key] = $values[$key . $ag];
						unset($values[$key . $ag]);
					}
				}
			}
		}

		$allergens = null;
		if (array_key_exists('allergens', $values)) {
			$allergens = $values['allergens'];
			unset($values['allergens']);
		}

		// save dish
		$item = $this->database->table('dish')->insert($values);
		// Debugger::barDump($item);

		// save the basket
		if ($basket) {
			$this->basketManager->saveBaskets($basket, $item->id);
		}

		// add new allergens into relation table
		if ($allergens) {
			$this->allergenManager->saveAllergens($allergens, $item->id);
		}

		return $item;
	}

	public function updateDish(int $id, array $values)
	{

		$item = $this->database->table('dish')->get($id);
		if ($item) {

			// update basket
			$ageGroups = $this->getAgeGroups();
			if ($values['type_id'] == 1 || $values['type_id'] == 2) {

				foreach ($ageGroups as $ag) {
					if ($ag < 5) {
						$basket = $this->basketManager->getBasket($item->id, $ag);
		
						// if basket exists - update it
						if ($basket) {
							$this->basketManager->updateBasket($basket, $values, $ag);
						}
						// basket not exist, you have to create
						else {
							$this->basketManager->saveBasket($values, $item->id, $ag);
						}
		
						unset($values['meat' . $ag]);
						unset($values['fish' . $ag]);
						unset($values['legumes' . $ag]);
						unset($values['cereals' . $ag]);
						unset($values['potatoes' . $ag]);
						unset($values['vegetables' . $ag]);
						unset($values['fat' . $ag]);
						unset($values['salt' . $ag]);
					}
				}
			}

			$allergens = null;
			if (array_key_exists('allergens', $values)) {
				$allergens = $values['allergens'];
				unset($values['allergens']);
			}

			// update values without allergens
			$item->update($values);

			// add new allergens into relation table
			$this->allergenManager->saveAllergens($allergens, $id);
			
			return $item;
		}

		return null;
	}

	public function increaseFrequencyAtDate(array $dishes, string $date) {
		foreach($dishes as $d) {
			$item = $this->database->table('dish')->get($d);
			if ($item) {
				
				$newFrequency = $item->frequency + 1;
				// Debugger::barDump($newFrequency);
				
				$dateTimestamp = $item->last_used_at;
				if ($dateTimestamp < date($date)) {
					$dateTimestamp = date($date);
				} 
				// Debugger::barDump($dateTimestamp);
				
				$item->update([
					'frequency' => $newFrequency,
					'last_used_at' => $dateTimestamp
					]);
			}
		}
	}

	public function decreaseFrequencyAtDate($id) {
		$item = $this->database->table('dish')->get($id);

		if ($item) {
			$newFrequency = ($item->frequency > 1) ? ($item->frequency - 1) : 0;

			$item->update([
				'frequency' => $newFrequency,
				'last_used_at' => NULL // its not correct but better then keep old date
				]);
		}
	}

	public function getDishFetch(int $id) {
		return $this->database->table('dish')->get($id);
	}

	public function getDish(int $id)
	{

		$item = $this->database->table('dish')->get($id);
		if (!$item) {
			return null;
		}
		
		$itemArray = $item->toArray();

		// get all allergens
		$allergens = array();
		$allergensNames = array();
		foreach ($item->related('relation_dish_allergen') as $dishAllergen) {
			$allergens[] = $dishAllergen->allergen->id;
			$allergensNames[] = $dishAllergen->allergen->name;
		}
		$itemArray['allergens'] = $allergens;
		$itemArray['allergensNames'] = $allergensNames;

		// get basket
		$ageGroups = $this->getAgeGroups();
		foreach ($ageGroups as $ag) {
			$basket = $this->basketManager->getBasket($item->id, $ag);
			
			// Debugger::barDump($basket);
			if ($basket) {
				$itemArray = $this->basketManager->addBasketArray($basket, $itemArray, $ag);
				$itemArray = $this->basketManager->addBasketObject($basket, $itemArray, $ag);
			}
		}

		// type name
		$type = $item->type;
		if($type) {
			$itemArray['type'] = $type->name;
		}

		// category name
		$category = $item->category;
		if($category) {
			$itemArray['category'] = $category->name;
		}

		// side name
		$side = $item->side;
		if($side) {
			$itemArray['side'] = $side->name;
		}

		// Debugger::barDump($itemArray);

		return $itemArray;
	}

	public function deleteDish(int $id)
	{
		$item = $this->database->table('dish')->get($id);
		if (!$item) {
			return false;
		}
		
		// remove all relations to allergens
		$this->allergenManager->removeAllergens($id);

		// remove baskets
		$this->basketManager->removeBaskets($id);

		// remove dish
		$item->delete();
		
		return true;
	}

	public function likeDish(int $id) {
		$item = $this->database->table('dish')->get($id);
		if ($item) {
			$likes = $item->likes;
			$item->update(['likes' => $likes + 1]);
			return true;
		}
		return false;
	}

	public function dislikeDish(int $id) {
		$item = $this->database->table('dish')->get($id);
		if ($item) {
			$likes = $item->likes;
			if ($likes > 0) {
				$item->update(['likes' => $likes - 1]);
				return 1;
			}
			else {
				$item->update(['likes' => 0]);
				return 0;
			}
		}
		return -1;
	}

	public function getAllDishesByType(string $type, bool $rand = false, int $count = 5000) {
		$dishes = $this->database->table('dish');

		// Get additions
		if ($type == 'Přídavek') {
			if (!$rand) {
				return $dishes->where('enabled = 1 AND (type.name = ? OR type.name = ? OR type.name = ? OR type.name = ?)', 'Ostatní', 'Salát', 'Moučník/dezert', 'Nápoj')->order('type.id ASC, category.id ASC')->limit($count)->fetchAll();
			}
			else {
				return $dishes->where('enabled = 1 AND (type.name = ? OR type.name = ? OR type.name = ?)', 'Ostatní', 'Salát', 'Moučník/dezert')->order('RAND()')->limit($count)->fetchAll();
			}
		}

		// Get drinks (1 non milk and if possible then 1 milk)
		if ($type == 'Nápoj' && $rand) {
			
			$milkDrink = [];
			if ($count > 1) {
				$milkDrink = $this->database->table('dish')->where('enabled = 1 AND type.name = ? AND category_id = 36', $type)->order('RAND()')->limit(1)->fetchAll();
			}

			$nonMilkDrinks = $this->database->table('dish')->where("enabled = 1 AND type.name = ? AND category_id != 36", $type)->order('RAND()')->limit(($count > 1) ? $count - 1 : $count)->fetchAll();

			return array_merge($nonMilkDrinks, $milkDrink);
		}

		// Get others
		if (!$rand) {
			return $dishes->where('enabled = 1 AND type.name = ?', $type)->order('category.id ASC')->limit($count)->fetchAll();
		}
		else {
			return $dishes->where('enabled = 1 AND type.name = ?', $type)->order('RAND()')->limit($count)->fetchAll();
		}
	}

	public function getDishesByCategory(int $categoryId, int $count = 5000) {
		return $this->database->table('dish')->where('category_id = ?', $categoryId)->order('RAND()')->limit($count)->fetchAll();
	}

	public function getApproDishesByCategory(int $categoryId, int $count) {
		
		// version 1
		// $dishes = $this->database->table('dish')->where('category_id = ?', $categoryId)->order('RAND()')->limit($count)->fetchAll();

		// version 2
		// get two times more dishes by last_used_at and likes
		// $dishes = $this->database->table('dish')->where('category_id = ?', $categoryId)->order('last_used_at ASC, likes DESC')->limit($count * 2)->fetchAll();

		// version 3
		// get 1.5 times more dishes by last_used_at likes and frequencies
		$coefficient = 1.5;
		$dishes = $this->database->table('dish')->where('enabled = 1 AND category_id = ?', $categoryId)->order('last_used_at ASC, likes DESC, frequency ASC')->limit(floor($count * 1.5))->fetchAll();

		// shake it
		shuffle($dishes);

		// slide array and return length equals $count
		$approDishes = array_slice($dishes, 0, $count);

		return $approDishes;
	}

	public function setEnableStatus(int $id, bool $status) : int {
		$item = $this->database->table('dish')->get($id);
		if (!$item) {
			return 0;
		}
		else {
			$item->update([
				'enabled' => $status
			]);
			return 1;
		}
	}

	private function getAgeGroups() : array {
		return json_decode($this->database->table('settings')->get(1)->toArray()['age_group']);
	}
}
