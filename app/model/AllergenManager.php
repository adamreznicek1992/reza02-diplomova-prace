<?php

namespace App\Model;

use Nette;
use Tracy\Debugger;

class AllergenManager
{
	use Nette\SmartObject;

	/** @var Nette\Database\Context */
	private $database;

	public function __construct(Nette\Database\Context $database)
	{
		$this->database = $database;
	}

	public function getAllAllergens()
	{
		return $this->database->table('allergen')
			->order('id ASC');
	}

	public function getAllAllergensArray()
	{
		return $this->database->table('allergen')
			->order('id ASC')->fetchPairs('id', 'number');
	}

	public function saveAllergens(array $allergens, int $dishId) {
		
		// remove all relations to allergens
		$this->database->table('relation_dish_allergen')
		->where('dish_id', $dishId)
		->delete();
		
		if ($allergens) {
			foreach($allergens as $key => $allergenId) {
				// test if allergen id exists
				if ($this->database->table('allergen')->get($allergenId)) {
					$this->database->table('relation_dish_allergen')->insert([
						'dish_id' => $dishId,
						'allergen_id' => $allergenId 
					]);
				}
			}
		}
	}

	public function removeAllergens(int $dishId) {
		$this->database->table('relation_dish_allergen')
		->where('dish_id', $dishId)
		->delete();
	}
}