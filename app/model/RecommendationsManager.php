<?php

namespace App\Model;

use Nette;
use Tracy\Debugger;

class RecommendationsManager
{
	use Nette\SmartObject;

	/** @var Nette\Database\Context */
	private $database;

	public function __construct(Nette\Database\Context $database)
	{
		$this->database = $database;
	}

	public function getAllBasketRecommendations()
	{
		return $this->database->table('recommendations_basket')
			->order('id ASC');
	}

	public function getAllBasketRecommendationsByTypeAndAgeGroup(int $typeId, string $ageGroup)
	{
		return $this->database->table('recommendations_basket')
			->where('type_id = ? AND age_group = ? AND value > 0', $typeId, $ageGroup)
			->order('id ASC')
			->fetchAll();
	}

	public function getBasketAssocArray(int $typeId, string $ageGroup) : array {
		return $this->database->table('recommendations_basket')->where('type_id = ? AND age_group = ?', $typeId, $ageGroup)->fetchPairs('id', 'translation');
	}

	public function getAllNutriRecommendations()
	{
		return $this->database->table('recommendations_nutri')
			->order('id ASC');
	}

	public function getAllNutriRecommendationsByType(int $typeId)
	{
		return $this->database->table('recommendations_nutri')
			->where('type_id = ? AND value > 0', $typeId)
			->order('id ASC')
			->fetchAll();
	}
}