<?php

namespace App\Model;

use Nette;
use Tracy\Debugger;

use App\Model\DishManager;

class MenuManager
{
	use Nette\SmartObject;

	/** @var DishManager */
	private $dishManager;

	/** @var Nette\Database\Context */
	private $database;

	public function __construct(Nette\Database\Context $database, DishManager $dishManager)
	{
		$this->database = $database;
		$this->dishManager = $dishManager;
	}

	public function getAllMenus() {
		return $this->database->table('menu_week')
			->order('year DESC, number DESC')
			->fetchAll();
	}

	public function getMenu(int $id) {
		return $this->database->table('menu_week')->get($id);
	}

	public function getMenuSet(int $id) {

		$menu = $this->getMenu($id);
		if (!$menu) {
			return false;
		}

		$set = $this->database->table('menu_week')->where('build_set = ?', $menu->build_set)->order('id ASC')->limit(4)->fetchAll();

		return $set;
	}

	public function getMenuArray(int $id, string $ageGroup) {
		return $this->prepareMenuArray($id, null, null, $ageGroup);
	}

	public function getLastMenu()
	{
		return $this->database->table('menu_week')
			->order('id DESC')
			->fetch();
	}

	public function getHomepageMenus() {

		$menus = [];

		$ageGroupKeys = $this->getAgeGroups();
		$ageGroup = $this->getAgeGroup($ageGroupKeys[0]);

		$actualMenu = $this->getActualMenu($ageGroup);
		if ($actualMenu['week']) {
			$id = $actualMenu['week']->id;
			$menus[] = $this->getMenu($id);
			$menus[] = $this->getMenu($id - 1);
			$menus[] = $this->getMenu($id - 2);
		}

		return $menus;
	}

	public function getMenuByDate($year, $week) 
	{
		return $this->database->table('menu_week')
			->where('year = ? AND number = ?', $year, $week)
			->fetch();
	}

	public function saveDay(object $values, int $dayNumber, int $hasTwoMainDishes = 0) {

		// Debugger::barDump($values);
		$day = $this->database->table('menu_day')->insert([
			'number' => $dayNumber,
			'date' => $values->date
		]);

		// add new relations
		$this->createRelationDishMenuDay($values, $day->id, $hasTwoMainDishes);

		return $day;

	}

	public function updateDay(int $id, object $values, int $ageGroupKey, int $hasTwoMainDishes = 0) {
		
		$item = $this->database->table('menu_day')->get($id);
		if(!$item) {
			return false;
		}

		$ageGroup = $this->getAgeGroup($ageGroupKey);

		// remove all dish relations
		$this->database->table('relation_dish_menu_day')
			->where('menu_day_id = ? AND age_group = ?', $id, $ageGroup)
			->delete();

		// add new relations
		$this->createRelationDishMenuDay($values, $id, $hasTwoMainDishes, $ageGroupKey);

		return true;
	}

	private function createRelationDishMenuDay(object $values, int $dayId, int $hasTwoMainDishes, $ageGroupKey = null) {

		// Debugger::barDump($values);

		// edit
		if ($ageGroupKey) {
			$ageGroupKeys[] = $ageGroupKey;
		}
		// create
		else {
			$ageGroupKeys = $this->getAgeGroups();
		}

		foreach ($ageGroupKeys as $ag) {

			$ageGroup = $this->getAgeGroup($ag);

			if ($ageGroup == '3-6 let' && $values->snacks1->id) {
				$this->dishManager->increaseFrequencyAtDate($values->snacks1->id, $values->date);

				// save to relation table dish day
				foreach($values->snacks1->id as $a) {
					$this->database->table('relation_dish_menu_day')->insert([
						'dish_id' => $a,
						'menu_day_id' => $dayId,
						'is_snack1' => 1,
						'age_group' => $ageGroup
					]);
				}
			}
			
			if ($values->soup->id) {
				$this->dishManager->increaseFrequencyAtDate($values->soup->id, $values->date);

				// save to relation table dish day
				$this->database->table('relation_dish_menu_day')->insert([
					'dish_id' => $values->soup->id[0],
					'menu_day_id' => $dayId,
					'age_group' => $ageGroup
				]);
			}
			
			if ($values->main->id) {
				$this->dishManager->increaseFrequencyAtDate($values->main->id, $values->date);

				// save to relation table dish day
				$this->database->table('relation_dish_menu_day')->insert([
					'dish_id' => $values->main->id[0],
					'menu_day_id' => $dayId,
					'age_group' => $ageGroup
				]);
			}

			if ($hasTwoMainDishes && $values->main2->id) {
				$this->dishManager->increaseFrequencyAtDate($values->main2->id, $values->date);

				// save to relation table dish day
				$this->database->table('relation_dish_menu_day')->insert([
					'dish_id' => $values->main2->id[0],
					'menu_day_id' => $dayId,
					'is_main2' => 1,
					'age_group' => $ageGroup
				]);
			}

			if (($ageGroup == '7-10 let' || $ageGroup == '11-14 let' || $ageGroup == '15-18 let') && $values->additions->id) {
				$this->dishManager->increaseFrequencyAtDate($values->additions->id, $values->date);

				// save to relation table dish day
				foreach($values->additions->id as $a) {
					$this->database->table('relation_dish_menu_day')->insert([
						'dish_id' => $a,
						'menu_day_id' => $dayId,
						'age_group' => $ageGroup
					]);
				}
			}

			if ($ageGroup != 'others' && $values->drinks->id) {
				$this->dishManager->increaseFrequencyAtDate($values->drinks->id, $values->date);

				// save to relation table dish day
				foreach($values->drinks->id as $a) {
					$this->database->table('relation_dish_menu_day')->insert([
						'dish_id' => $a,
						'menu_day_id' => $dayId,
						'age_group' => $ageGroup
					]);
				}
			}

			if ($ageGroup == '3-6 let' && $values->snacks2->id) {
				$this->dishManager->increaseFrequencyAtDate($values->snacks2->id, $values->date);

				// save to relation table dish day
				foreach($values->snacks2->id as $a) {
					$this->database->table('relation_dish_menu_day')->insert([
						'dish_id' => $a,
						'menu_day_id' => $dayId,
						'is_snack2' => 1,
						'age_group' => $ageGroup
					]);
				}
			}

		}

		return true;
	}

	public function saveWeek(array $values) {

		$week = $this->database->table('menu_week')->insert([
			'number' => $values['number'],
			'year' => $values['year'],
			'build_set' => $values['build_set']
		]);

		// Debugger::barDump($values['days']);

		// save to relation table day week
		foreach($values['days'] as $dayId) {
			$this->database->table('relation_menu_day_menu_week')->insert([
				'menu_day_id' => $dayId,
				'menu_week_id' => $week->id
			]);
		}

		return $week;
	}

	public function getActualMenu(string $ageGroup) {

		$settings = $this->database->table('settings')->get(1)->toArray();
		if (!$settings) {
			$settings['menu_actived_at_day'] = 'Friday';
			$settings['menu_actived_at_time'] = '16:00';
		}

		$weekNumber = date('W');
		$yearNumber = date('Y');

		// cause 1.1.2021 has W = 53 and Y = 2021, but you need W = 53 and Y = 2020.
		if ($weekNumber == 53) {
			$yearNumber -= 1;
		}

		$now = time();
		$limit = strtotime($settings['menu_actived_at_day'] . $settings['menu_actived_at_time'], strtotime($yearNumber . 'W' . $weekNumber));

		// Debugger::barDump($weekNumber);
		// Debugger::barDump($yearNumber);

		// Debugger::barDump($now);
		// Debugger::barDump($limit);
		// Debugger::barDump(date('Y.m.d', $limit));

		if ($now >= $limit) {
			// show next week menu
			if ($weekNumber == 53) {
				$weekNumber = 1;
				$yearNumber += 1;
			}
			else {
				$weekNumber += 1;
			}
		}

		// Debugger::barDump($weekNumber);
		// Debugger::barDump($yearNumber);

		return $this->prepareMenuArray(null, $yearNumber, $weekNumber, $ageGroup);

	}

	public function getPreviousMenu(int $ageGroupKey) {
		$ageGroup = $this->getAgeGroup($ageGroupKey);
		$actualMenu = $this->getActualMenu($ageGroup);
		return ($actualMenu['week']) ? $this->getMenuArray($actualMenu['week']->id - 1, $ageGroup) : null;
	}

	private function prepareMenuArray($id, $yearNumber, $weekNumber, $ageGroup) {

		$menu = [];
		if ($id) {
			$menu['week'] = $this->getMenu($id);
		}
		else {
			if ($yearNumber && $weekNumber) {
				$menu['week'] = $this->getMenuByDate($yearNumber, $weekNumber);
			}
			else {
				$menu['week'] = null;
			}
		}
		
		if ($menu['week']) {

			$menu['dishes'] = [];
			
			// get day all dishes
			$days = ['monday','tuesday','wednesday','thursday','friday'];

			$index = 0;
			foreach ($menu['week']->related('relation_menu_day_menu_week') as $relToDay) {

				$day = $relToDay->ref('menu_day');
				$dayName = $days[$index++];

				$menu['dishes'][$dayName] = [];
				$menu['dishes'][$dayName]['snacks1'] = [];
				$menu['dishes'][$dayName]['soup'] = null;
				$menu['dishes'][$dayName]['main'] = null;
				$menu['dishes'][$dayName]['main2'] = null;
				$menu['dishes'][$dayName]['additions'] = [];
				$menu['dishes'][$dayName]['drinks'] = [];
				$menu['dishes'][$dayName]['snacks2'] = [];

				$relations = $day->related('relation_dish_menu_day');

				foreach($relations as $relToDish) {

					if ($relToDish->age_group == $ageGroup) {
					
						$dish = $relToDish->ref('dish');

						// second main dish
						if ($relToDish->is_main2 || $relToDish->is_snack1 || $relToDish->is_snack2) {

							if ($relToDish->is_main2) {
								$menu['dishes'][$dayName]['main2'] = $dish;
							}

							if ($relToDish->is_snack1) {
								$menu['dishes'][$dayName]['snacks1'][] = $dish;
							}

							if ($relToDish->is_snack2) {
								$menu['dishes'][$dayName]['snacks2'][] = $dish;
							}
						}
						// all dishes
						else {
							switch($dish->type->name) {
								case 'Hlavní chod':
									$menu['dishes'][$dayName]['main'] = $dish;
									break;
								case 'Polévka':
									$menu['dishes'][$dayName]['soup'] = $dish;
									break;
								case 'Nápoj':
									$menu['dishes'][$dayName]['drinks'][] = $dish;
									break;
								default:
									$menu['dishes'][$dayName]['additions'][] = $dish;
									break;
							}
						}

					}
				}
			}
		}

		return $menu;

	}

	public function deleteMenu(int $id) {
		
		$item = $this->database->table('menu_week')->get($id);
		if (!$item) {
			return false;
		}

		$buildSetNumber = $item->build_set;
		$set = $this->database->table('menu_week')->where('build_set = ?', $buildSetNumber)->fetchAll();

		foreach ($set as $w) {

			$relationsDay = $w->related('relation_menu_day_menu_week');
			
			foreach($relationsDay as $relDay) {
				$day = $relDay->ref('menu_day');
				
				$relationsDishDay = $this->database->table('relation_dish_menu_day')->where('menu_day_id', $day->id); 
				
				// update dish frequency and last used date
				foreach ($relationsDishDay as $relDish) {
					$this->dishManager->decreaseFrequencyAtDate($relDish->dish_id);
				} 
				
				// remove relations to dish
				$relationsDishDay->delete();

				// remove relation to day
				$this->database->table('relation_menu_day_menu_week')
				->where('menu_day_id', $day->id)
				->delete();

				$day->delete();
			}

			$w->delete();
		}

		return true;
	}

	private function getAgeGroups() : array {
		return json_decode($this->database->table('settings')->get(1)->toArray()['age_group']);
	}

	private function getAgeGroup(int $key) {
		switch($key) {
			case 1:
				return '3-6 let';
			case 2:
				return '7-10 let';
			case 3:
				return '11-14 let';
			case 4:
				return '15-18 let';
			default:
				return 'others';
		}
	}

	public function testIfEmpty(array $menuOriginal) : bool {

		$menu = array_merge([], $menuOriginal);

		foreach ($menu as $dayName => $day) {
			foreach($day as $dishName => $dish) {
				if (empty($dish)) {
					unset($menu[$dayName][$dishName]);
				}
			}

			if (empty($menu[$dayName])) {
				unset($menu[$dayName]);
			}
		}
	
		if (empty($menu)) {
			return true;
		}
	
		return false;
	}
}