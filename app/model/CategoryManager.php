<?php

namespace App\Model;

use Nette;
use Tracy\Debugger;

class CategoryManager
{
	use Nette\SmartObject;

	/** @var Nette\Database\Context */
	private $database;

	public function __construct(Nette\Database\Context $database)
	{
		$this->database = $database;
	}

	public function getAllCategories()
	{
		return $this->database->table('category')
			->order('id ASC');
	}

	public function getAllCategoriesByType(int $typeId)
	{
		return $this->database->table('category')
			->where('type_id', $typeId)
			->order('id ASC')
			->fetchAll();
	}

	public function getAllCategoriesByTypeArray(int $typeId)
	{
		return $this->database->table('category')
			->where('type_id', $typeId)
			->order('id ASC')
			->fetchPairs('id', 'name');
	}

	public function getCategoryByName(string $name)
	{
		return $this->database->table('category')
			->where('name = ?', $name)
			->fetch();
	}
}