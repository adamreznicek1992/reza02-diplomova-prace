<?php

namespace App\Model;

use Nette;
use Tracy\Debugger;

use App\Model\CategoryManager;

class TypeManager
{
	use Nette\SmartObject;

	/** @var CategoryManager */
	private $categoryManager;

	/** @var Nette\Database\Context */
	private $database;

	public function __construct(Nette\Database\Context $database, CategoryManager $categoryManager)
	{
		$this->database = $database;
		$this->categoryManager = $categoryManager;
	}

	public function getAllTypes()
	{
		return $this->database->table('type')
			->order('id ASC');
	}

	public function getAllTypesArray()
	{
		$types = $this->database->table('type')
			->order('id ASC');
		
		$typesArray = [];
		foreach ($types as $type) {
			$typesArray[$type->id] = $type->name;
		}

		return $typesArray;
	}

	public function getId(string $value)
	{
		$item = $this->database->table('type')->where('name', $value)->fetch();
		if ($item) {
			return $item->id;
		}

		return null;
	}

	public function getName(int $id)
	{

		$item = $this->database->table('type')->get($id);
		if ($item) {
			return $item->name;
		}

		return null;
	}

	public function hasSide(int $id)
	{

		$item = $this->database->table('type')->get($id);
		if ($item) {
			return $item->has_side;
		}

		return false;
	}

	public function hasBasket(int $id)
	{

		$item = $this->database->table('type')->get($id);
		if ($item) {
			return $item->has_basket;
		}

		return false;
	}

	public function hasCategory(int $id)
	{

		$item = $this->database->table('type')->get($id);
		if ($item) {
			return $item->has_category;
		}

		return false;
	}

	public function getCategories(int $id)
	{
		return $this->categoryManager->getAllCategoriesByType($id);
	}

	public function getCategoriesArray(int $id)
	{
		return $this->categoryManager->getAllCategoriesByTypeArray($id);
	}
}