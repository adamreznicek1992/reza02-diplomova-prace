<?php

namespace App\Model;

use Nette;
use Tracy\Debugger;

class SideManager
{
	use Nette\SmartObject;

	/** @var Nette\Database\Context */
	private $database;

	public function __construct(Nette\Database\Context $database)
	{
		$this->database = $database;
	}

	public function getAllSides()
	{
		return $this->database->table('side')
			->order('id ASC');
	}

	public function getAllSidesArray()
	{
		return $this->database->table('side')
			->order('id ASC')->fetchPairs('id', 'name');
	}
}