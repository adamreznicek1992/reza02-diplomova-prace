// Paths
const templateDevDir = 'www';
const distDir = 'dist';
const cssDevDir = templateDevDir + '/styles';
const cssDistDir = distDir + '/styles';

// Load GULP plugins
var gulp = require('gulp');
var $ = require('gulp-load-plugins')();

// build CSS file and optimize
gulp.task('styles', function () {
  return gulp.src(cssDevDir + '/main.scss')
    .pipe($.plumber())
    .pipe($.sourcemaps.init())
    .pipe($.sass().on('error', $.sass.logError))
    .pipe($.postcss([
      require('autoprefixer-core')({browsers: [ '> 1%', 'last 3 version']}),
    ]))
    .pipe($.sourcemaps.write())
    .pipe(gulp.dest(cssDevDir))
    .pipe($.csso({restructure: false}))
    .pipe(gulp.dest(cssDistDir));
});
