
const settingsFile = require('./settings');

var gulp = require('gulp');
var open = require('gulp-open');
var $ = require('gulp-load-plugins')();

gulp.task('connect', function(){
  $.connect.server({
    root: ['./'],
    port: 9000,
    livereload: true
  });
});