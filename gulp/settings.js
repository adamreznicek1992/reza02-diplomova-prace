
// Paths
const appDevDir = 'app';
const templateDevDir = 'www';
const distDir = 'dist';

const cssDevDir = templateDevDir + '/styles';
const cssDistDir = distDir + '/styles';
const jsDevDir = templateDevDir + '/scripts';
const jsDistDir = distDir + '/scripts';
const imgDevDir = templateDevDir + '/images';
const fontsDevDir = templateDevDir + '/fonts';
const fontsDistDir = distDir + '/fonts';
const dataDevDir = templateDevDir + '/datas';