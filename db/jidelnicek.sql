-- phpMyAdmin SQL Dump
-- version 5.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Dec 06, 2020 at 07:24 PM
-- Server version: 10.3.16-MariaDB
-- PHP Version: 7.3.6

SET FOREIGN_KEY_CHECKS=0;
SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `jidelnicek`
--

-- --------------------------------------------------------

--
-- Table structure for table `allergen`
--

CREATE TABLE `allergen` (
  `id` int(11) UNSIGNED NOT NULL,
  `number` varchar(255) NOT NULL,
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_czech_ci NOT NULL,
  `label` varchar(255) CHARACTER SET utf8 COLLATE utf8_czech_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `allergen`
--

INSERT INTO `allergen` (`id`, `number`, `name`, `label`) VALUES
(1, 'a1', 'Obiloviny obsahující lepek', 'pšenice, žito, ječmen, oves, špalda, kamut nebo jejich hybridní odrůdy a výrobky z nich'),
(2, 'a2', 'Korýši', 'a výrobky z nich'),
(3, 'a3', 'Vejce', 'a výrobky z nich'),
(4, 'a4', 'Ryby', 'a výrobky z nich'),
(5, 'a5', 'Podzemnice Olejná (arašídy)', 'a výrobky z nich'),
(6, 'a6', 'Sójové boby (sója)\r\n', 'a výrobky z nich'),
(7, 'a7', 'Mléko', 'a výrobky z něj'),
(8, 'a8', 'Skořápkové plody', 'mandle, lískové ořechy, vlašské ořechy, kešu ořechy, pekanové ořechy, para ořechy, pistácie, makadamie a výrobky z nich'),
(9, 'a9', 'Celer', 'a výrobky z něj'),
(10, 'a10', 'Hořčice', 'a výrobky z ní'),
(11, 'a11', 'Sezamová semena (sezam)', 'a výrobky z nich'),
(12, 'a12', 'Oxid siřičitý a siřičitany', 'v koncentracích vyšších 10 mg, ml/kg, l, vyjádřeno SO<sub>2</sub>'),
(13, 'a13', 'Vlčí bob (lupina)', 'a výrobky z něj'),
(14, 'a14', 'Měkkýši', 'a výrobky z nich\r\n');

-- --------------------------------------------------------

--
-- Table structure for table `basket`
--

CREATE TABLE `basket` (
  `id` int(11) UNSIGNED NOT NULL,
  `meat` int(10) UNSIGNED DEFAULT 0,
  `fish` int(10) UNSIGNED DEFAULT 0,
  `legumes` int(10) UNSIGNED DEFAULT 0,
  `cereals` int(10) UNSIGNED DEFAULT 0,
  `potatoes` int(10) UNSIGNED DEFAULT 0,
  `vegetables` int(10) UNSIGNED DEFAULT 0,
  `fat` int(10) UNSIGNED DEFAULT 0,
  `salt` float UNSIGNED DEFAULT 0,
  `age_group` enum('3-6 let','7-10 let','11-14 let','15-18 let') NOT NULL DEFAULT '7-10 let'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `basket`
--

INSERT INTO `basket` (`id`, `meat`, `fish`, `legumes`, `cereals`, `potatoes`, `vegetables`, `fat`, `salt`, `age_group`) VALUES
(1, 65, 0, 0, 60, 0, 50, 6, 1.6, '7-10 let'),
(3, 75, 0, 0, 0, 200, 70, 8, 1.6, '7-10 let'),
(4, 55, 0, 10, 0, 200, 75, 8, 1.6, '7-10 let'),
(5, 70, 0, 0, 0, 150, 150, 6, 1.2, '7-10 let'),
(12, 70, 0, 0, 100, 0, 2, 0, 2, '7-10 let'),
(13, 65, 0, 0, 60, 0, 70, 8, 1.6, '7-10 let'),
(14, 0, 0, 0, 0, 0, 50, 4, 0.5, '7-10 let'),
(15, 65, 0, 0, 100, 0, 60, 8, 1.6, '7-10 let'),
(16, 0, 0, 0, 8, 0, 45, 4, 1, '7-10 let'),
(17, 70, 0, 0, 0, 200, 70, 8, 1.4, '7-10 let'),
(18, 75, 0, 0, 0, 250, 85, 8, 1.2, '7-10 let'),
(19, 35, 0, 5, 0, 0, 250, 8, 1.6, '7-10 let'),
(20, 10, 0, 0, 8, 0, 40, 4, 1, '7-10 let'),
(21, 0, 0, 0, 0, 350, 0, 8, 0.8, '7-10 let'),
(22, 0, 0, 0, 0, 150, 0, 8, 0.4, '7-10 let'),
(23, 0, 0, 35, 0, 0, 0, 4, 1, '7-10 let'),
(24, 65, 0, 0, 60, 0, 75, 8, 1.2, '7-10 let'),
(25, 75, 0, 0, 0, 200, 75, 6, 1.3, '7-10 let'),
(26, 65, 0, 3, 80, 0, 65, 4, 1.2, '7-10 let'),
(27, 80, 0, 0, 65, 0, 70, 6, 1.6, '7-10 let'),
(28, 65, 0, 3, 60, 0, 60, 4, 1.1, '7-10 let'),
(29, 90, 0, 0, 0, 200, 65, 8, 1.6, '7-10 let'),
(30, 65, 0, 0, 65, 0, 67, 4, 0.8, '7-10 let'),
(31, 60, 0, 0, 0, 150, 70, 5, 1.2, '7-10 let'),
(32, 65, 0, 0, 65, 0, 50, 4, 0.8, '7-10 let'),
(33, 100, 0, 0, 10, 200, 65, 8, 1.6, '7-10 let'),
(34, 60, 0, 0, 75, 0, 60, 4, 0.8, '7-10 let'),
(35, 70, 0, 0, 0, 250, 75, 6, 1.6, '7-10 let'),
(36, 80, 0, 10, 0, 250, 60, 8, 1.6, '7-10 let'),
(37, 70, 0, 3, 70, 0, 60, 3, 0.4, '7-10 let'),
(38, 75, 0, 0, 0, 200, 85, 4, 1.2, '7-10 let'),
(39, 75, 0, 0, 60, 0, 67, 2, 0.8, '7-10 let'),
(40, 70, 0, 0, 0, 200, 67, 6, 1.3, '7-10 let'),
(41, 65, 0, 0, 80, 0, 67, 4, 0.8, '7-10 let'),
(42, 65, 0, 0, 80, 0, 67, 6, 1.3, '7-10 let'),
(43, 65, 0, 0, 0, 250, 67, 6, 1.3, '7-10 let'),
(44, 70, 0, 0, 0, 200, 70, 8, 1.6, '7-10 let'),
(45, 65, 0, 0, 60, 0, 67, 6, 1.2, '7-10 let'),
(46, 80, 0, 0, 0, 250, 70, 8, 1.6, '7-10 let'),
(47, 65, 0, 0, 75, 0, 85, 6, 1.6, '7-10 let'),
(48, 65, 0, 16, 0, 280, 67, 6, 1.7, '7-10 let'),
(49, 85, 0, 0, 75, 0, 55, 4, 0.8, '7-10 let'),
(50, 70, 0, 0, 65, 0, 60, 6, 1.2, '7-10 let'),
(51, 65, 0, 0, 70, 0, 65, 6, 1, '7-10 let'),
(52, 75, 0, 0, 0, 85, 65, 4, 1.6, '7-10 let'),
(53, 0, 105, 0, 20, 200, 75, 6, 1.4, '7-10 let'),
(54, 85, 0, 0, 85, 0, 70, 6, 1.6, '7-10 let'),
(55, 85, 0, 5, 85, 0, 75, 6, 1.6, '7-10 let'),
(56, 80, 0, 0, 90, 0, 70, 6, 1.6, '7-10 let'),
(57, 80, 0, 0, 0, 250, 75, 6, 1.1, '7-10 let'),
(58, 80, 0, 0, 75, 0, 70, 6, 1.2, '7-10 let'),
(59, 80, 0, 0, 80, 0, 65, 5, 1.6, '7-10 let'),
(60, 75, 0, 0, 85, 0, 65, 4, 0.8, '7-10 let'),
(61, 0, 105, 0, 0, 250, 70, 6, 0.8, '7-10 let'),
(62, 0, 105, 0, 0, 250, 70, 6, 1.6, '7-10 let'),
(63, 0, 105, 10, 20, 250, 70, 8, 1.6, '7-10 let'),
(64, 0, 105, 0, 0, 250, 70, 6, 1.2, '7-10 let'),
(65, 0, 105, 0, 5, 250, 65, 7, 1.3, '7-10 let'),
(66, 0, 105, 0, 5, 200, 70, 6, 0.8, '7-10 let'),
(67, 0, 105, 0, 0, 250, 65, 8, 1.6, '7-10 let'),
(69, 0, 0, 60, 15, 0, 67, 4, 1.6, '7-10 let'),
(71, 0, 0, 60, 10, 0, 67, 6, 0.8, '7-10 let'),
(72, 40, 0, 30, 0, 0, 67, 8, 1.6, '7-10 let'),
(73, 40, 0, 30, 5, 0, 67, 4, 0.8, '7-10 let'),
(74, 0, 0, 60, 5, 0, 70, 3, 0.8, '7-10 let'),
(75, 0, 0, 60, 5, 0, 70, 4, 0.8, '7-10 let'),
(76, 40, 0, 30, 10, 0, 67, 3, 0.8, '7-10 let'),
(77, 0, 0, 60, 10, 0, 67, 5, 0.8, '7-10 let'),
(78, 0, 0, 0, 150, 0, 70, 8, 1.6, '7-10 let'),
(79, 0, 0, 0, 150, 0, 70, 6, 0.8, '7-10 let'),
(80, 0, 0, 20, 10, 0, 150, 3, 0.8, '7-10 let'),
(81, 0, 0, 0, 80, 0, 150, 4, 0.8, '7-10 let'),
(82, 0, 0, 0, 50, 0, 0, 10, 0.4, '7-10 let'),
(83, 0, 0, 0, 4, 150, 0, 10, 1.3, '7-10 let'),
(84, 0, 0, 0, 100, 0, 0, 10, 0.1, '7-10 let'),
(85, 0, 0, 0, 100, 0, 0, 10, 0.8, '7-10 let'),
(86, 0, 0, 0, 120, 0, 0, 10, 0.4, '7-10 let'),
(87, 0, 0, 0, 120, 0, 0, 12, 0.8, '7-10 let'),
(88, 0, 0, 0, 0, 0, 50, 3, 0.5, '7-10 let'),
(89, 0, 0, 35, 0, 0, 0, 3, 0.6, '7-10 let'),
(90, 0, 0, 35, 5, 0, 25, 4, 1, '7-10 let'),
(91, 0, 0, 0, 0, 0, 50, 4, 0.5, '7-10 let'),
(92, 0, 0, 0, 0, 0, 50, 2, 1, '7-10 let'),
(93, 0, 0, 35, 0, 0, 0, 2, 0.5, '7-10 let'),
(94, 0, 35, 5, 10, 0, 10, 4, 1, '7-10 let'),
(95, 0, 0, 35, 0, 0, 5, 2, 0.5, '7-10 let'),
(96, 15, 0, 20, 0, 0, 5, 23, 0.7, '7-10 let'),
(97, 0, 35, 0, 8, 0, 15, 4, 0.5, '7-10 let'),
(98, 0, 0, 10, 0, 0, 30, 1, 0.7, '7-10 let'),
(99, 10, 0, 0, 10, 0, 40, 3, 0.8, '7-10 let'),
(100, 10, 0, 0, 15, 0, 40, 3, 0.8, '7-10 let'),
(101, 0, 0, 0, 0, 25, 40, 2, 0.6, '7-10 let'),
(102, 30, 0, 0, 0, 0, 40, 4, 1, '7-10 let'),
(103, 10, 0, 0, 8, 0, 40, 3, 0.7, '7-10 let'),
(104, 0, 0, 0, 0, 25, 0, 3, 0.5, '7-10 let'),
(105, 0, 0, 0, 3, 0, 50, 2, 0.7, '7-10 let'),
(106, 0, 0, 0, 10, 0, 35, 3, 1, '7-10 let'),
(107, 0, 0, 0, 10, 0, 45, 2, 0.5, '7-10 let'),
(108, 0, 0, 0, 0, 25, 40, 2, 0.5, '7-10 let'),
(109, 0, 0, 0, 10, 0, 0, 2, 1, '7-10 let'),
(110, 0, 0, 0, 0, 25, 50, 1, 0.3, '7-10 let'),
(111, 0, 0, 0, 0, 25, 40, 3, 0.5, '7-10 let'),
(112, 15, 0, 0, 0, 20, 30, 2, 0, '7-10 let'),
(113, 15, 0, 5, 0, 0, 25, 3, 0.3, '7-10 let'),
(114, 0, 0, 0, 0, 0, 50, 1, 0.2, '7-10 let'),
(115, 0, 0, 0, 0, 0, 50, 2, 0.3, '7-10 let'),
(116, 0, 0, 0, 0, 0, 50, 2, 0.5, '7-10 let'),
(117, 30, 0, 0, 0, 0, 50, 2, 1, '7-10 let'),
(118, 30, 0, 0, 0, 0, 40, 4, 1, '7-10 let'),
(119, 0, 0, 0, 0, 30, 40, 4, 1, '7-10 let'),
(120, 0, 0, 0, 0, 25, 45, 3, 1, '7-10 let'),
(121, 0, 0, 0, 0, 25, 45, 2, 0.5, '7-10 let'),
(122, 0, 0, 0, 0, 25, 45, 2, 1, '7-10 let'),
(123, 0, 0, 0, 0, 30, 50, 2, 0.5, '7-10 let'),
(124, 0, 0, 0, 0, 25, 40, 2, 1, '7-10 let'),
(125, 20, 0, 0, 0, 25, 20, 4, 0.5, '7-10 let'),
(126, 20, 0, 0, 0, 25, 30, 1, 0.7, '7-10 let'),
(127, 0, 0, 10, 0, 0, 30, 2, 0.8, '7-10 let'),
(128, 15, 0, 5, 0, 0, 25, 4, 1, '7-10 let'),
(129, 15, 0, 5, 0, 0, 25, 4, 1, '7-10 let'),
(130, 15, 0, 20, 2, 0, 5, 2, 0.6, '7-10 let'),
(131, 15, 0, 20, 2, 0, 5, 4, 1, '7-10 let'),
(132, 0, 35, 0, 10, 0, 10, 5, 1, '7-10 let'),
(133, 0, 0, 0, 10, 0, 0, 1, 1, '7-10 let'),
(134, 0, 0, 0, 0, 25, 0, 4, 1, '7-10 let'),
(135, 75, 0, 0, 100, 0, 70, 4, 1.2, '7-10 let'),
(142, 68, 0, 0, 90, 0, 63, 4, 0.7, '3-6 let'),
(143, 86, 0, 0, 130, 0, 77, 5, 1.2, '11-14 let'),
(144, 101, 0, 0, 135, 0, 95, 5, 1.4, '15-18 let'),
(149, 50, 0, 9, 0, 180, 68, 7, 1, '3-6 let'),
(150, 63, 0, 12, 0, 250, 83, 9, 1.6, '11-14 let'),
(151, 74, 0, 12, 0, 300, 101, 10, 1.8, '15-18 let'),
(152, 59, 0, 0, 54, 0, 68, 7, 0.7, '3-6 let'),
(153, 75, 0, 0, 78, 0, 83, 9, 1.2, '11-14 let'),
(154, 88, 0, 0, 81, 0, 101, 10, 1.4, '15-18 let'),
(155, 0, 0, 0, 0, 135, 0, 7, 0.2, '3-6 let'),
(156, 0, 0, 0, 0, 188, 0, 9, 0.4, '11-14 let'),
(157, 0, 0, 0, 0, 225, 0, 10, 0.5, '15-18 let'),
(158, 0, 0, 0, 0, 315, 0, 7, 0.5, '3-6 let'),
(159, 0, 0, 0, 0, 438, 0, 9, 0.8, '11-14 let'),
(160, 0, 0, 0, 0, 525, 0, 10, 0.9, '15-18 let'),
(161, 32, 0, 5, 0, 0, 225, 7, 1, '3-6 let'),
(162, 40, 0, 6, 0, 0, 275, 9, 1.6, '11-14 let'),
(163, 47, 0, 6, 0, 0, 338, 10, 1.8, '15-18 let'),
(164, 68, 0, 0, 0, 180, 63, 7, 1, '3-6 let'),
(165, 86, 0, 0, 0, 250, 77, 9, 1.6, '11-14 let'),
(166, 101, 0, 0, 0, 300, 95, 10, 1.8, '15-18 let'),
(167, 59, 0, 0, 54, 0, 45, 5, 1, '3-6 let'),
(168, 75, 0, 0, 78, 0, 55, 7, 1.6, '11-14 let'),
(169, 88, 0, 0, 81, 0, 68, 7, 1.8, '15-18 let'),
(170, 63, 0, 0, 0, 135, 135, 5, 0.7, '3-6 let'),
(171, 81, 0, 0, 0, 188, 165, 7, 1.2, '11-14 let'),
(172, 95, 0, 0, 0, 225, 203, 7, 1.4, '15-18 let'),
(173, 59, 0, 0, 54, 0, 63, 7, 1, '3-6 let'),
(174, 75, 0, 0, 78, 0, 77, 9, 1.6, '11-14 let'),
(175, 88, 0, 0, 81, 0, 95, 10, 1.8, '15-18 let'),
(176, 59, 0, 0, 90, 0, 54, 7, 1, '3-6 let'),
(177, 75, 0, 0, 130, 0, 66, 9, 1.6, '11-14 let'),
(178, 88, 0, 0, 135, 0, 81, 10, 1.8, '15-18 let'),
(179, 63, 0, 0, 0, 180, 63, 7, 0.8, '3-6 let'),
(180, 81, 0, 0, 0, 250, 77, 9, 1.4, '11-14 let'),
(181, 95, 0, 0, 0, 300, 95, 10, 1.6, '15-18 let'),
(182, 68, 0, 0, 0, 225, 77, 7, 0.7, '3-6 let'),
(183, 86, 0, 0, 0, 313, 94, 9, 1.2, '11-14 let'),
(184, 101, 0, 0, 0, 375, 115, 10, 1.4, '15-18 let'),
(185, 68, 0, 0, 0, 180, 68, 5, 0.8, '3-6 let'),
(186, 86, 0, 0, 0, 250, 83, 7, 1.3, '11-14 let'),
(187, 101, 0, 0, 0, 300, 101, 7, 1.5, '15-18 let'),
(188, 59, 0, 3, 72, 0, 59, 4, 0.7, '3-6 let'),
(189, 75, 0, 4, 104, 0, 72, 5, 1.2, '11-14 let'),
(190, 88, 0, 4, 108, 0, 88, 5, 1.4, '15-18 let'),
(191, 72, 0, 0, 59, 0, 63, 5, 1, '3-6 let'),
(192, 92, 0, 0, 85, 0, 77, 7, 1.6, '11-14 let'),
(193, 108, 0, 0, 88, 0, 95, 7, 1.8, '15-18 let'),
(194, 59, 0, 3, 54, 0, 54, 4, 0.7, '3-6 let'),
(195, 75, 0, 4, 78, 0, 66, 5, 1.1, '11-14 let'),
(196, 88, 0, 4, 81, 0, 81, 5, 1.3, '15-18 let'),
(197, 81, 0, 0, 0, 180, 59, 7, 1, '3-6 let'),
(198, 104, 0, 0, 0, 250, 72, 9, 1.6, '11-14 let'),
(199, 122, 0, 0, 0, 300, 88, 10, 1.8, '15-18 let'),
(200, 59, 0, 0, 59, 0, 60, 4, 0.5, '3-6 let'),
(201, 75, 0, 0, 85, 0, 74, 5, 0.8, '11-14 let'),
(202, 88, 0, 0, 88, 0, 91, 5, 0.9, '15-18 let'),
(203, 54, 0, 0, 0, 135, 63, 5, 0.7, '3-6 let'),
(204, 69, 0, 0, 0, 188, 77, 6, 1.2, '11-14 let'),
(205, 81, 0, 0, 0, 225, 95, 6, 1.4, '15-18 let'),
(206, 59, 0, 0, 59, 0, 45, 4, 0.5, '3-6 let'),
(207, 75, 0, 0, 85, 0, 55, 5, 0.8, '11-14 let'),
(208, 88, 0, 0, 88, 0, 68, 5, 0.9, '15-18 let'),
(209, 90, 0, 0, 9, 180, 59, 7, 1, '3-6 let'),
(210, 115, 0, 0, 13, 250, 72, 9, 1.6, '11-14 let'),
(211, 135, 0, 0, 14, 300, 88, 10, 1.8, '15-18 let'),
(212, 54, 0, 0, 68, 0, 54, 4, 0.5, '3-6 let'),
(213, 69, 0, 0, 98, 0, 66, 5, 0.8, '11-14 let'),
(214, 81, 0, 0, 101, 0, 81, 5, 0.9, '15-18 let'),
(215, 63, 0, 0, 0, 225, 68, 5, 1, '3-6 let'),
(216, 81, 0, 0, 0, 313, 83, 7, 1.6, '11-14 let'),
(217, 95, 0, 0, 0, 375, 101, 7, 1.8, '15-18 let'),
(218, 72, 0, 9, 0, 225, 54, 7, 1, '3-6 let'),
(219, 92, 0, 12, 0, 313, 66, 9, 1.6, '11-14 let'),
(220, 108, 0, 12, 0, 375, 81, 10, 1.8, '15-18 let'),
(221, 63, 0, 3, 63, 0, 54, 3, 0.2, '3-6 let'),
(222, 81, 0, 4, 91, 0, 66, 4, 0.4, '11-14 let'),
(223, 95, 0, 4, 95, 0, 81, 4, 0.5, '15-18 let'),
(224, 68, 0, 0, 0, 180, 77, 4, 0.7, '3-6 let'),
(225, 86, 0, 0, 0, 250, 94, 5, 1.2, '11-14 let'),
(226, 101, 0, 0, 0, 300, 115, 5, 1.4, '15-18 let'),
(227, 68, 0, 0, 54, 0, 60, 2, 0.5, '3-6 let'),
(228, 86, 0, 0, 78, 0, 74, 2, 0.8, '11-14 let'),
(229, 101, 0, 0, 81, 0, 91, 2, 0.9, '15-18 let'),
(230, 63, 0, 0, 0, 180, 60, 5, 0.8, '3-6 let'),
(231, 81, 0, 0, 0, 250, 74, 7, 1.3, '11-14 let'),
(232, 95, 0, 0, 0, 300, 91, 7, 1.5, '15-18 let'),
(233, 59, 0, 0, 72, 0, 60, 4, 0.5, '3-6 let'),
(234, 75, 0, 0, 104, 0, 74, 5, 0.8, '11-14 let'),
(235, 88, 0, 0, 108, 0, 91, 5, 0.9, '15-18 let'),
(236, 59, 0, 0, 72, 0, 60, 5, 0.8, '3-6 let'),
(237, 75, 0, 0, 104, 0, 74, 7, 1.3, '11-14 let'),
(238, 88, 0, 0, 108, 0, 91, 7, 1.5, '15-18 let'),
(239, 59, 0, 0, 0, 225, 60, 5, 0.8, '3-6 let'),
(240, 75, 0, 0, 0, 313, 74, 7, 1.3, '11-14 let'),
(241, 88, 0, 0, 0, 375, 91, 7, 1.5, '15-18 let'),
(242, 63, 0, 0, 0, 180, 63, 7, 1, '3-6 let'),
(243, 81, 0, 0, 0, 250, 77, 9, 1.6, '11-14 let'),
(244, 95, 0, 0, 0, 300, 95, 10, 1.8, '15-18 let'),
(245, 59, 0, 0, 54, 0, 60, 5, 0.7, '3-6 let'),
(246, 75, 0, 0, 78, 0, 74, 7, 1.2, '11-14 let'),
(247, 88, 0, 0, 81, 0, 91, 7, 1.4, '15-18 let'),
(248, 72, 0, 0, 0, 225, 63, 7, 1, '3-6 let'),
(249, 92, 0, 0, 0, 313, 77, 9, 1.6, '11-14 let'),
(250, 108, 0, 0, 0, 375, 95, 10, 1.8, '15-18 let'),
(251, 59, 0, 0, 68, 0, 77, 5, 1, '3-6 let'),
(252, 75, 0, 0, 98, 0, 94, 7, 1.6, '11-14 let'),
(253, 88, 0, 0, 101, 0, 115, 7, 1.8, '15-18 let'),
(254, 59, 0, 14, 0, 252, 60, 5, 1, '3-6 let'),
(255, 75, 0, 18, 0, 350, 74, 7, 1.7, '11-14 let'),
(256, 88, 0, 18, 0, 420, 91, 7, 2, '15-18 let'),
(257, 77, 0, 0, 68, 0, 50, 4, 0.5, '3-6 let'),
(258, 98, 0, 0, 98, 0, 61, 5, 0.8, '11-14 let'),
(259, 115, 0, 0, 101, 0, 74, 5, 0.9, '15-18 let'),
(260, 63, 0, 0, 59, 0, 54, 5, 0.7, '3-6 let'),
(261, 81, 0, 0, 85, 0, 66, 7, 1.2, '11-14 let'),
(262, 95, 0, 0, 88, 0, 81, 7, 1.4, '15-18 let'),
(263, 59, 0, 0, 63, 0, 59, 5, 0.6, '3-6 let'),
(264, 75, 0, 0, 91, 0, 72, 7, 1, '11-14 let'),
(265, 88, 0, 0, 95, 0, 88, 7, 1.2, '15-18 let'),
(266, 68, 0, 0, 0, 77, 59, 4, 1, '3-6 let'),
(267, 86, 0, 0, 0, 106, 72, 5, 1.6, '11-14 let'),
(268, 101, 0, 0, 0, 128, 88, 5, 1.8, '15-18 let'),
(269, 0, 95, 0, 18, 180, 68, 5, 0.8, '3-6 let'),
(270, 0, 137, 0, 26, 250, 83, 7, 1.4, '11-14 let'),
(271, 0, 158, 0, 27, 300, 101, 7, 1.6, '15-18 let'),
(272, 77, 0, 0, 77, 0, 63, 5, 1, '3-6 let'),
(273, 98, 0, 0, 111, 0, 77, 7, 1.6, '11-14 let'),
(274, 115, 0, 0, 115, 0, 95, 7, 1.8, '15-18 let'),
(275, 77, 0, 5, 77, 0, 68, 5, 1, '3-6 let'),
(276, 98, 0, 6, 111, 0, 83, 7, 1.6, '11-14 let'),
(277, 115, 0, 6, 115, 0, 101, 7, 1.8, '15-18 let'),
(278, 72, 0, 0, 81, 0, 63, 5, 1, '3-6 let'),
(279, 92, 0, 0, 117, 0, 77, 7, 1.6, '11-14 let'),
(280, 108, 0, 0, 122, 0, 95, 7, 1.8, '15-18 let'),
(281, 72, 0, 0, 0, 225, 68, 5, 0.7, '3-6 let'),
(282, 92, 0, 0, 0, 313, 83, 7, 1.1, '11-14 let'),
(283, 108, 0, 0, 0, 375, 101, 7, 1.3, '15-18 let'),
(284, 72, 0, 0, 68, 0, 63, 5, 0.7, '3-6 let'),
(285, 92, 0, 0, 98, 0, 77, 7, 1.2, '11-14 let'),
(286, 108, 0, 0, 101, 0, 95, 7, 1.4, '15-18 let'),
(287, 72, 0, 0, 72, 0, 59, 5, 1, '3-6 let'),
(288, 92, 0, 0, 104, 0, 72, 6, 1.6, '11-14 let'),
(289, 108, 0, 0, 108, 0, 88, 6, 1.8, '15-18 let'),
(290, 68, 0, 0, 77, 0, 59, 4, 0.5, '3-6 let'),
(291, 86, 0, 0, 111, 0, 72, 5, 0.8, '11-14 let'),
(292, 101, 0, 0, 115, 0, 88, 5, 0.9, '15-18 let'),
(293, 0, 95, 0, 0, 225, 63, 5, 0.5, '3-6 let'),
(294, 0, 137, 0, 0, 313, 77, 7, 0.8, '11-14 let'),
(295, 0, 158, 0, 0, 375, 95, 7, 0.9, '15-18 let'),
(296, 0, 95, 0, 0, 225, 63, 5, 1, '3-6 let'),
(297, 0, 137, 0, 0, 313, 77, 7, 1.6, '11-14 let'),
(298, 0, 158, 0, 0, 375, 95, 7, 1.8, '15-18 let'),
(299, 0, 95, 9, 18, 225, 63, 7, 1, '3-6 let'),
(300, 0, 137, 12, 26, 313, 77, 9, 1.6, '11-14 let'),
(301, 0, 158, 12, 27, 375, 95, 10, 1.8, '15-18 let'),
(302, 0, 95, 0, 0, 225, 63, 5, 0.7, '3-6 let'),
(303, 0, 137, 0, 0, 313, 77, 7, 1.2, '11-14 let'),
(304, 0, 158, 0, 0, 375, 95, 7, 1.4, '15-18 let'),
(305, 0, 95, 0, 5, 225, 59, 6, 0.8, '3-6 let'),
(306, 0, 137, 0, 7, 313, 72, 8, 1.3, '11-14 let'),
(307, 0, 158, 0, 7, 375, 88, 8, 1.5, '15-18 let'),
(308, 0, 95, 0, 5, 180, 63, 5, 0.5, '3-6 let'),
(309, 0, 137, 0, 7, 250, 77, 7, 0.8, '11-14 let'),
(310, 0, 158, 0, 7, 300, 95, 7, 0.9, '15-18 let'),
(311, 0, 95, 0, 0, 225, 59, 7, 1, '3-6 let'),
(312, 0, 137, 0, 0, 313, 72, 9, 1.6, '11-14 let'),
(313, 0, 158, 0, 0, 375, 88, 10, 1.8, '15-18 let'),
(314, 0, 0, 54, 14, 0, 60, 4, 1, '3-6 let'),
(315, 0, 0, 69, 20, 0, 74, 5, 1.6, '11-14 let'),
(316, 0, 0, 69, 20, 0, 91, 5, 1.8, '15-18 let'),
(317, 0, 0, 54, 9, 0, 60, 5, 0.5, '3-6 let'),
(318, 0, 0, 69, 13, 0, 74, 7, 0.8, '11-14 let'),
(319, 0, 0, 69, 14, 0, 91, 7, 0.9, '15-18 let'),
(320, 36, 0, 27, 0, 0, 60, 7, 1, '3-6 let'),
(321, 46, 0, 35, 0, 0, 74, 9, 1.6, '11-14 let'),
(322, 54, 0, 35, 0, 0, 91, 10, 1.8, '15-18 let'),
(323, 36, 0, 27, 5, 0, 60, 4, 0.5, '3-6 let'),
(324, 46, 0, 35, 7, 0, 74, 5, 0.8, '11-14 let'),
(325, 54, 0, 35, 7, 0, 91, 5, 0.9, '15-18 let'),
(326, 0, 0, 54, 5, 0, 63, 3, 0.5, '3-6 let'),
(327, 0, 0, 69, 7, 0, 77, 4, 0.8, '11-14 let'),
(328, 0, 0, 69, 7, 0, 95, 4, 0.9, '15-18 let'),
(329, 0, 0, 54, 5, 0, 63, 4, 0.5, '3-6 let'),
(330, 0, 0, 69, 7, 0, 77, 5, 0.8, '11-14 let'),
(331, 0, 0, 69, 7, 0, 95, 5, 0.9, '15-18 let'),
(332, 36, 0, 27, 9, 0, 60, 3, 0.5, '3-6 let'),
(333, 46, 0, 35, 13, 0, 74, 4, 0.8, '11-14 let'),
(334, 54, 0, 35, 14, 0, 91, 4, 0.9, '15-18 let'),
(335, 0, 0, 54, 9, 0, 60, 5, 0.5, '3-6 let'),
(336, 0, 0, 69, 13, 0, 74, 6, 0.8, '11-14 let'),
(337, 0, 0, 69, 14, 0, 91, 6, 0.9, '15-18 let'),
(338, 0, 0, 0, 135, 0, 63, 7, 1, '3-6 let'),
(339, 0, 0, 0, 195, 0, 77, 9, 1.6, '11-14 let'),
(340, 0, 0, 0, 203, 0, 95, 10, 1.8, '15-18 let'),
(341, 0, 0, 0, 135, 0, 63, 5, 0.5, '3-6 let'),
(342, 0, 0, 0, 195, 0, 77, 7, 0.8, '11-14 let'),
(343, 0, 0, 0, 203, 0, 95, 7, 0.9, '15-18 let'),
(344, 0, 0, 18, 9, 0, 135, 3, 0.5, '3-6 let'),
(345, 0, 0, 23, 13, 0, 165, 4, 0.8, '11-14 let'),
(346, 0, 0, 23, 14, 0, 203, 4, 0.9, '15-18 let'),
(347, 0, 0, 0, 72, 0, 135, 4, 0.5, '3-6 let'),
(348, 0, 0, 0, 104, 0, 165, 5, 0.8, '11-14 let'),
(349, 0, 0, 0, 108, 0, 203, 5, 0.9, '15-18 let'),
(350, 0, 0, 0, 45, 0, 0, 9, 0.2, '3-6 let'),
(351, 0, 0, 0, 65, 0, 0, 12, 0.4, '11-14 let'),
(352, 0, 0, 0, 68, 0, 0, 12, 0.5, '15-18 let'),
(353, 0, 0, 0, 4, 135, 0, 9, 0.8, '3-6 let'),
(354, 0, 0, 0, 5, 188, 0, 12, 1.3, '11-14 let'),
(355, 0, 0, 0, 5, 225, 0, 12, 1.5, '15-18 let'),
(356, 0, 0, 0, 90, 0, 0, 9, 0.1, '3-6 let'),
(357, 0, 0, 0, 130, 0, 0, 12, 0.1, '11-14 let'),
(358, 0, 0, 0, 135, 0, 0, 12, 0.1, '15-18 let'),
(359, 0, 0, 0, 90, 0, 0, 9, 0.5, '3-6 let'),
(360, 0, 0, 0, 130, 0, 0, 12, 0.8, '11-14 let'),
(361, 0, 0, 0, 135, 0, 0, 12, 0.9, '15-18 let'),
(362, 0, 0, 0, 108, 0, 0, 9, 0.2, '3-6 let'),
(363, 0, 0, 0, 156, 0, 0, 12, 0.4, '11-14 let'),
(364, 0, 0, 0, 162, 0, 0, 12, 0.5, '15-18 let'),
(365, 0, 0, 0, 108, 0, 0, 11, 0.5, '3-6 let'),
(366, 0, 0, 0, 156, 0, 0, 14, 0.8, '11-14 let'),
(367, 0, 0, 0, 162, 0, 0, 14, 0.9, '15-18 let'),
(368, 68, 0, 0, 90, 0, 63, 4, 1.4, '3-6 let'),
(369, 83, 0, 0, 110, 0, 77, 4, 1.7, '11-14 let'),
(370, 94, 0, 0, 125, 0, 88, 5, 1.9, '15-18 let'),
(371, 0, 0, 32, 0, 0, 0, 4, 1, '3-6 let'),
(372, 0, 0, 46, 0, 0, 0, 5, 1.3, '11-14 let'),
(373, 0, 0, 47, 0, 0, 0, 4, 1.4, '15-18 let'),
(374, 10, 0, 0, 8, 0, 36, 4, 1, '3-6 let'),
(375, 13, 0, 0, 8, 0, 52, 5, 1.3, '11-14 let'),
(376, 14, 0, 0, 12, 0, 60, 4, 1.4, '15-18 let'),
(377, 0, 0, 0, 0, 0, 45, 4, 0.5, '3-6 let'),
(378, 0, 0, 0, 0, 0, 65, 5, 0.7, '11-14 let'),
(379, 0, 0, 0, 0, 0, 75, 4, 0.7, '15-18 let'),
(380, 0, 0, 0, 8, 0, 41, 4, 1, '3-6 let'),
(381, 0, 0, 0, 8, 0, 59, 5, 1.3, '11-14 let'),
(382, 0, 0, 0, 12, 0, 68, 4, 1.4, '15-18 let'),
(383, 0, 0, 0, 0, 0, 45, 3, 0.5, '3-6 let'),
(384, 0, 0, 0, 0, 0, 65, 4, 0.7, '11-14 let'),
(385, 0, 0, 0, 0, 0, 75, 3, 0.7, '15-18 let'),
(386, 0, 0, 32, 0, 0, 0, 3, 0.6, '3-6 let'),
(387, 0, 0, 46, 0, 0, 0, 4, 0.8, '11-14 let'),
(388, 0, 0, 47, 0, 0, 0, 3, 0.8, '15-18 let'),
(389, 0, 0, 32, 5, 0, 23, 4, 1, '3-6 let'),
(390, 0, 0, 46, 5, 0, 33, 5, 1.3, '11-14 let'),
(391, 0, 0, 47, 8, 0, 38, 4, 1.4, '15-18 let'),
(392, 0, 0, 0, 0, 0, 45, 4, 0.5, '3-6 let'),
(393, 0, 0, 0, 0, 0, 65, 5, 0.7, '11-14 let'),
(394, 0, 0, 0, 0, 0, 75, 4, 0.7, '15-18 let'),
(395, 0, 0, 0, 0, 0, 45, 2, 1, '3-6 let'),
(396, 0, 0, 0, 0, 0, 65, 2, 1.3, '11-14 let'),
(397, 0, 0, 0, 0, 0, 75, 2, 1.4, '15-18 let'),
(398, 0, 0, 32, 0, 0, 0, 2, 0.5, '3-6 let'),
(399, 0, 0, 46, 0, 0, 0, 2, 0.7, '11-14 let'),
(400, 0, 0, 47, 0, 0, 0, 2, 0.7, '15-18 let'),
(401, 0, 32, 5, 10, 0, 9, 4, 1, '3-6 let'),
(402, 0, 46, 7, 10, 0, 13, 5, 1.3, '11-14 let'),
(403, 0, 53, 7, 15, 0, 15, 4, 1.4, '15-18 let'),
(404, 0, 0, 32, 0, 0, 5, 2, 0.5, '3-6 let'),
(405, 0, 0, 46, 0, 0, 7, 2, 0.7, '11-14 let'),
(406, 0, 0, 47, 0, 0, 8, 2, 0.7, '15-18 let'),
(407, 15, 0, 18, 0, 0, 5, 21, 0.7, '3-6 let'),
(408, 20, 0, 26, 0, 0, 7, 27, 0.9, '11-14 let'),
(409, 20, 0, 27, 0, 0, 8, 25, 0.9, '15-18 let'),
(410, 0, 32, 0, 8, 0, 14, 4, 0.5, '3-6 let'),
(411, 0, 46, 0, 8, 0, 20, 5, 0.7, '11-14 let'),
(412, 0, 53, 0, 12, 0, 23, 4, 0.7, '15-18 let'),
(413, 0, 0, 9, 0, 0, 27, 1, 0.7, '3-6 let'),
(414, 0, 0, 13, 0, 0, 39, 1, 0.9, '11-14 let'),
(415, 0, 0, 14, 0, 0, 45, 1, 0.9, '15-18 let'),
(416, 10, 0, 0, 10, 0, 36, 3, 0.8, '3-6 let'),
(417, 13, 0, 0, 10, 0, 52, 4, 1, '11-14 let'),
(418, 14, 0, 0, 15, 0, 60, 3, 1.1, '15-18 let'),
(419, 10, 0, 0, 15, 0, 36, 3, 0.8, '3-6 let'),
(420, 13, 0, 0, 15, 0, 52, 4, 1, '11-14 let'),
(421, 14, 0, 0, 23, 0, 60, 3, 1.1, '15-18 let'),
(422, 0, 0, 0, 0, 23, 36, 2, 0.6, '3-6 let'),
(423, 0, 0, 0, 0, 38, 52, 2, 0.8, '11-14 let'),
(424, 0, 0, 0, 0, 40, 60, 2, 0.8, '15-18 let'),
(425, 30, 0, 0, 0, 0, 36, 4, 1, '3-6 let'),
(426, 39, 0, 0, 0, 0, 52, 5, 1.3, '11-14 let'),
(427, 41, 0, 0, 0, 0, 60, 4, 1.4, '15-18 let'),
(428, 10, 0, 0, 8, 0, 36, 3, 0.7, '3-6 let'),
(429, 13, 0, 0, 8, 0, 52, 4, 0.9, '11-14 let'),
(430, 14, 0, 0, 12, 0, 60, 3, 0.9, '15-18 let'),
(431, 0, 0, 0, 0, 23, 0, 3, 0.5, '3-6 let'),
(432, 0, 0, 0, 0, 38, 0, 4, 0.7, '11-14 let'),
(433, 0, 0, 0, 0, 40, 0, 3, 0.7, '15-18 let'),
(434, 0, 0, 0, 3, 0, 45, 2, 0.7, '3-6 let'),
(435, 0, 0, 0, 3, 0, 65, 2, 0.9, '11-14 let'),
(436, 0, 0, 0, 5, 0, 75, 2, 0.9, '15-18 let'),
(437, 0, 0, 0, 10, 0, 32, 3, 1, '3-6 let'),
(438, 0, 0, 0, 10, 0, 46, 4, 1.3, '11-14 let'),
(439, 0, 0, 0, 15, 0, 53, 3, 1.4, '15-18 let'),
(440, 0, 0, 0, 10, 0, 41, 2, 0.5, '3-6 let'),
(441, 0, 0, 0, 10, 0, 59, 2, 0.7, '11-14 let'),
(442, 0, 0, 0, 15, 0, 68, 2, 0.7, '15-18 let'),
(443, 0, 0, 0, 0, 23, 36, 2, 0.5, '3-6 let'),
(444, 0, 0, 0, 0, 38, 52, 2, 0.7, '11-14 let'),
(445, 0, 0, 0, 0, 40, 60, 2, 0.7, '15-18 let'),
(446, 0, 0, 0, 10, 0, 0, 2, 1, '3-6 let'),
(447, 0, 0, 0, 10, 0, 0, 2, 1.3, '11-14 let'),
(448, 0, 0, 0, 15, 0, 0, 2, 1.4, '15-18 let'),
(449, 0, 0, 0, 0, 23, 45, 1, 0.3, '3-6 let'),
(450, 0, 0, 0, 0, 38, 65, 1, 0.4, '11-14 let'),
(451, 0, 0, 0, 0, 40, 75, 1, 0.4, '15-18 let'),
(452, 0, 0, 0, 0, 23, 36, 3, 0.5, '3-6 let'),
(453, 0, 0, 0, 0, 38, 52, 4, 0.7, '11-14 let'),
(454, 0, 0, 0, 0, 40, 60, 3, 0.7, '15-18 let'),
(455, 15, 0, 0, 0, 18, 27, 2, 0, '3-6 let'),
(456, 20, 0, 0, 0, 30, 39, 2, 0, '11-14 let'),
(457, 20, 0, 0, 0, 32, 45, 2, 0, '15-18 let'),
(458, 15, 0, 5, 0, 0, 23, 3, 0.3, '3-6 let'),
(459, 20, 0, 7, 0, 0, 33, 4, 0.4, '11-14 let'),
(460, 20, 0, 7, 0, 0, 38, 3, 0.4, '15-18 let'),
(461, 0, 0, 0, 0, 0, 45, 1, 0.2, '3-6 let'),
(462, 0, 0, 0, 0, 0, 65, 1, 0.3, '11-14 let'),
(463, 0, 0, 0, 0, 0, 75, 1, 0.3, '15-18 let'),
(464, 0, 0, 0, 0, 0, 45, 2, 0.3, '3-6 let'),
(465, 0, 0, 0, 0, 0, 65, 2, 0.4, '11-14 let'),
(466, 0, 0, 0, 0, 0, 75, 2, 0.4, '15-18 let'),
(467, 0, 0, 0, 0, 0, 45, 2, 0.5, '3-6 let'),
(468, 0, 0, 0, 0, 0, 65, 2, 0.7, '11-14 let'),
(469, 0, 0, 0, 0, 0, 75, 2, 0.7, '15-18 let'),
(470, 30, 0, 0, 0, 0, 45, 2, 1, '3-6 let'),
(471, 39, 0, 0, 0, 0, 65, 2, 1.3, '11-14 let'),
(472, 41, 0, 0, 0, 0, 75, 2, 1.4, '15-18 let'),
(473, 30, 0, 0, 0, 0, 36, 4, 1, '3-6 let'),
(474, 39, 0, 0, 0, 0, 52, 5, 1.3, '11-14 let'),
(475, 41, 0, 0, 0, 0, 60, 4, 1.4, '15-18 let'),
(476, 0, 0, 0, 0, 27, 36, 4, 1, '3-6 let'),
(477, 0, 0, 0, 0, 45, 52, 5, 1.3, '11-14 let'),
(478, 0, 0, 0, 0, 48, 60, 4, 1.4, '15-18 let'),
(479, 0, 0, 0, 0, 23, 41, 3, 1, '3-6 let'),
(480, 0, 0, 0, 0, 38, 59, 4, 1.3, '11-14 let'),
(481, 0, 0, 0, 0, 40, 68, 3, 1.4, '15-18 let'),
(482, 0, 0, 0, 0, 23, 41, 2, 0.5, '3-6 let'),
(483, 0, 0, 0, 0, 38, 59, 2, 0.7, '11-14 let'),
(484, 0, 0, 0, 0, 40, 68, 2, 0.7, '15-18 let'),
(485, 0, 0, 0, 0, 23, 41, 2, 1, '3-6 let'),
(486, 0, 0, 0, 0, 38, 59, 2, 1.3, '11-14 let'),
(487, 0, 0, 0, 0, 40, 68, 2, 1.4, '15-18 let'),
(488, 0, 0, 0, 0, 27, 45, 2, 0.5, '3-6 let'),
(489, 0, 0, 0, 0, 45, 65, 2, 0.7, '11-14 let'),
(490, 0, 0, 0, 0, 48, 75, 2, 0.7, '15-18 let'),
(491, 0, 0, 0, 0, 23, 36, 2, 1, '3-6 let'),
(492, 0, 0, 0, 0, 38, 52, 2, 1.3, '11-14 let'),
(493, 0, 0, 0, 0, 40, 60, 2, 1.4, '15-18 let'),
(494, 20, 0, 0, 0, 23, 18, 4, 0.5, '3-6 let'),
(495, 26, 0, 0, 0, 38, 26, 5, 0.7, '11-14 let'),
(496, 27, 0, 0, 0, 40, 30, 4, 0.7, '15-18 let'),
(497, 20, 0, 0, 0, 23, 27, 1, 0.7, '3-6 let'),
(498, 26, 0, 0, 0, 38, 39, 1, 0.9, '11-14 let'),
(499, 27, 0, 0, 0, 40, 45, 1, 0.9, '15-18 let'),
(500, 0, 0, 9, 0, 0, 27, 2, 0.8, '3-6 let'),
(501, 0, 0, 13, 0, 0, 39, 2, 1, '11-14 let'),
(502, 0, 0, 14, 0, 0, 45, 2, 1.1, '15-18 let'),
(503, 15, 0, 5, 0, 0, 23, 4, 1, '3-6 let'),
(504, 20, 0, 7, 0, 0, 33, 5, 1.3, '11-14 let'),
(505, 20, 0, 7, 0, 0, 38, 4, 1.4, '15-18 let'),
(506, 15, 0, 5, 0, 0, 23, 4, 1, '3-6 let'),
(507, 20, 0, 7, 0, 0, 33, 5, 1.3, '11-14 let'),
(508, 20, 0, 7, 0, 0, 38, 4, 1.4, '15-18 let'),
(509, 15, 0, 18, 2, 0, 5, 2, 0.6, '3-6 let'),
(510, 20, 0, 26, 2, 0, 7, 2, 0.8, '11-14 let'),
(511, 20, 0, 27, 3, 0, 8, 2, 0.8, '15-18 let'),
(512, 15, 0, 18, 2, 0, 5, 4, 1, '3-6 let'),
(513, 20, 0, 26, 2, 0, 7, 5, 1.3, '11-14 let'),
(514, 20, 0, 27, 3, 0, 8, 4, 1.4, '15-18 let'),
(515, 0, 32, 0, 10, 0, 9, 5, 1, '3-6 let'),
(516, 0, 46, 0, 10, 0, 13, 6, 1.3, '11-14 let'),
(517, 0, 53, 0, 15, 0, 15, 6, 1.4, '15-18 let'),
(518, 0, 0, 0, 10, 0, 0, 1, 1, '3-6 let'),
(519, 0, 0, 0, 10, 0, 0, 1, 1.3, '11-14 let'),
(520, 0, 0, 0, 15, 0, 0, 1, 1.4, '15-18 let'),
(521, 0, 0, 0, 0, 23, 0, 4, 1, '3-6 let'),
(522, 0, 0, 0, 0, 38, 0, 5, 1.3, '11-14 let'),
(523, 0, 0, 0, 0, 40, 0, 4, 1.4, '15-18 let');

-- --------------------------------------------------------

--
-- Table structure for table `category`
--

CREATE TABLE `category` (
  `id` smallint(5) UNSIGNED NOT NULL,
  `type_id` smallint(5) UNSIGNED NOT NULL,
  `name` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `category`
--

INSERT INTO `category` (`id`, `type_id`, `name`) VALUES
(1, 1, 'Drůbež s knedlíkem\r\n'),
(2, 1, 'Drůbež s bramborem\r\n'),
(3, 1, 'Drůbež s rýží\r\n'),
(4, 1, 'Drůbež s těstovinou\r\n'),
(5, 1, 'Vepřové s knedlíkem\r\n'),
(6, 1, 'Vepřové s bramborem\r\n'),
(7, 1, 'Vepřové s rýží\r\n'),
(8, 1, 'Vepřové s těstovinou\r\n'),
(9, 1, 'Ostatní masa s knedlíkem\r\n'),
(10, 1, 'Ostatní masa s bramborem\r\n'),
(11, 1, 'Ostatní masa s rýží\r\n'),
(12, 1, 'Ostatní masa s těstovinou\r\n'),
(13, 1, 'Ryba s bramborem\r\n'),
(14, 1, 'Luštěninové jídlo bezmasé\r\n'),
(15, 1, 'Luštěninové jídlo s drůbežím\r\n'),
(16, 1, 'Luštěninové jídlo s vepřovým\r\n'),
(17, 1, 'Luštěninové jídlo s ostatním masem\r\n'),
(18, 1, 'Bezmasé jídlo slané\r\n'),
(19, 1, 'Bezmasé jídlo sladké'),
(20, 2, 'Zeleninová\r\n'),
(21, 2, 'Zeleninová s masem\r\n'),
(22, 2, 'Zeleninová s bramborem\r\n'),
(23, 2, 'Zeleninová s bramborem a masem\r\n'),
(24, 2, 'Zeleninová s luštěninou\r\n'),
(25, 2, 'Zeleninová s luštěninou a masem\r\n'),
(26, 2, 'Zeleninová se zavářkou (těstovina, kapání atp.)\r\n'),
(27, 2, 'Zeleninová se zavářkou (těstovina, kapání atp.) a masem\r\n'),
(28, 2, 'Luštěninová\r\n'),
(29, 2, 'Luštěninová s masem\r\n'),
(30, 2, 'Rybí se zavářkou (těstovina, kapání atp.)\r\n'),
(31, 2, 'Mléčná se zavářkou (těstovina, kapání atp.)\r\n'),
(32, 2, 'Mléčná s bramborem\r\n'),
(33, 3, 'Čaj'),
(34, 3, 'Šťáva'),
(35, 3, 'Perlivý nápoj'),
(36, 3, 'Mléčný nápoj'),
(37, 1, 'Ryba s těstovinou'),
(38, 7, 'Pečivo'),
(39, 7, 'Ovoce'),
(40, 7, 'Zelenina');

-- --------------------------------------------------------

--
-- Table structure for table `dish`
--

CREATE TABLE `dish` (
  `id` int(11) UNSIGNED NOT NULL,
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_czech_ci NOT NULL,
  `recipe` text CHARACTER SET utf8 COLLATE utf8_czech_ci DEFAULT NULL,
  `type_id` smallint(5) UNSIGNED NOT NULL DEFAULT 1,
  `category_id` smallint(5) UNSIGNED DEFAULT NULL,
  `side_id` smallint(5) UNSIGNED DEFAULT NULL,
  `likes` int(11) NOT NULL DEFAULT 0,
  `frequency` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `enabled` tinyint(1) NOT NULL DEFAULT 1,
  `last_used_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `dish`
--

INSERT INTO `dish` (`id`, `name`, `recipe`, `type_id`, `category_id`, `side_id`, `likes`, `frequency`, `enabled`, `last_used_at`, `created_at`, `updated_at`) VALUES
(1, 'Krůtí maso s hráškem a mrkví', 'Technologický postup:\nMrkev podusíme na tuku, přidáme hrášek, na kostičky pokrájené vařené nebo pečené maso, přidáme koření kari a osolíme. Lehce promícháme a 10 minut podusíme.', 1, 2, 2, 1, 0, 1, NULL, '2020-06-15 13:33:41', '2020-12-05 22:51:18'),
(2, 'Rajčatové špagety', 'Na rozehřátém oleji orestujeme na drobno nakrájenou cibuli, přidáme pomleté filety ze sumečka a společně opékáme doměkka. Směs osolíme a opepříme, přidáme pasírovaná rajčata a krátce prohřejeme. Ve vroucí osolené vodě uvaříme špagety podle návodu na obalu. Hotovou rajčatovou směs smícháme s uvařenými špagetami.\nPodáváme s nasekanou petrželkou.', 1, 8, 3, 0, 0, 1, NULL, '2020-06-15 13:38:01', '2020-12-05 22:51:38'),
(3, 'Polévka fazolová krémová', 'Fazole zalijeme vodou, namočíme na 3 hodiny, pak slijeme a nalijeme čerstvou vodu a uvaříme doměkka. Cibuli nakrájíme na kostičky, na tuku osmahneme do sklovita, zaprášíme moukou a usmažíme světlou cibulovou jíšku. Zalijeme vodou, rozšleháme, osolíme a provaříme 20 minut. Přidáme fazole prolisované na masovém strojku, prolisovaný česnek, majoránku a polévku ještě 20 minut vaříme. Podáváme s nakrájenou, na sucho opraženou žemlí.', 2, 28, NULL, 0, 0, 1, NULL, '2020-06-15 13:47:29', '2020-12-05 22:51:18'),
(5, 'Banán', NULL, 7, 39, NULL, 0, 0, 1, NULL, '2020-06-15 13:58:54', '2020-12-05 22:51:01'),
(6, 'Salát zelný s křenovou omáčkou', 'Zelí jemně nastrouháme, osolíme a necháme odležet. Do smetany přidáme křen, ocet, cukr, pepř, zamícháme, přidáme do zelí, promícháme a dochutíme.', 5, NULL, NULL, 0, 0, 1, NULL, '2020-06-15 14:02:42', '2020-12-05 22:51:18'),
(7, 'Zelný salát s jablky', 'Hlávkové zelí zbavíme košťálu, nakrájíme na tenké nudličky, spaříme vodou, scedíme. Přidáme nastrouhaná jablka (bez slupek a jádřince), mrkev, nakrájenou cibulku, ochutíme solí, octem a cukrem. Pořádně promícháme. (Můžeme zakapat olejem.) Podáváme jako přílohu k masu.', 5, NULL, NULL, 0, 0, 1, NULL, '2020-06-15 14:05:07', '2020-12-05 22:51:18'),
(8, 'Ovocná vitamínová bomba', 'Ovoce nakrájíme na kousky. Vše smícháme se sirupem a s vodou. K přípravě lze využít ovoce dle momentální nabídky.', 5, NULL, NULL, 0, 0, 1, NULL, '2020-06-15 14:15:33', '2020-12-05 22:51:18'),
(9, 'Krém tvarohový s borůvkami', 'Kompot slijeme, ponecháme 1/5 šťávy a ovoce s ní rozmixujeme. Máslo utřeme s vanilkovým cukrem, přidáme prolisovaný tvaroh, podle potřeby mléko a ušleháme. Zašleháme ovocné pyré.', 6, NULL, NULL, 0, 0, 1, NULL, '2020-06-15 15:51:19', '2020-12-05 22:51:18'),
(10, 'Domácí mrkvový džus', 'Mrkev uvaříme ve vodě s cukrem doměkka. , rozmixujeme, přidáme převařenou vychladlou vodu, šťávu a dochutíme citronem. Džus ihned podáváme, neskladujeme.', 3, 34, NULL, 0, 0, 1, NULL, '2020-06-15 15:56:30', '2020-12-05 22:51:18'),
(11, 'Bramborové lívanečky', 'Syrové brambory nastrouháme najemno a mícháme s teplým mlékem, postupně přidáváme po jednom vejci a domícháme se smetanou. Pečeme nasucho v nádobě vyložené pečícím papírem. Sypeme práškovým cukrem a podáváme s bílou kávou. Nesolit!', 1, 19, NULL, 0, 0, 1, NULL, '2020-06-15 15:58:32', '2020-12-05 22:51:18'),
(12, 'Škubánky s mákem', 'Škubánky připravíme podle návodu. Mletý mák promícháme s cukrem, máslo rozpustíme.\nPomocí lžíce, kterou máčíme ve vodě (rozpuštěném másle), vykrajujeme z hotové hmoty úhledné noky - škubánky, sypeme je směsí máku s cukrem a mastíme rozpuštěným máslem.', 1, 19, 2, 0, 0, 1, NULL, '2020-06-15 16:06:03', '2020-12-05 22:51:38'),
(15, 'Polévka zeleninová s rýží', 'Očištěnou, jemně nastrouhanou mrkev a brukev, na malé kostičky nasekaný květák a propláchnutou rýži dáme vařit do osolené vody (těstoviny přidáme až po změknutí zeleniny). Vše vaříme do měkka. Zjemníme tukem a přidáme zelenou nať.', 2, 27, NULL, 0, 0, 1, NULL, '2020-06-15 16:08:48', '2020-12-05 22:51:18'),
(16, 'Mrkev dušená s hráškem', 'Oškrábanou omytou mrkev nakrájíme na kostky, zalijeme vařící vodou, osolíme a dusíme. Připravíme světlou jíšku, přidáme do téměř měkké mrkve a dodusíme. Přidáme sterilovaný hrášek.', 1, 16, NULL, 0, 0, 1, NULL, '2020-06-16 06:48:38', '2020-11-02 17:09:31'),
(17, 'Holandská omáčka se zeleninou', 'Jednu polovinu cibulky osmahneme a pak rozpustíme tavený sýr. Z druhé poloviny cibulky uděláme cibulovou jíšku, kterou zahustíme rozpuštěný tavený sýr povolený mlékem. Dále povaříme a dochutíme. Zeleninu mírně povaříme na páře, můžeme dochutit solí a bylinkami.', 1, 10, 2, 0, 0, 1, NULL, '2020-06-16 07:09:40', '2020-12-05 22:51:38'),
(18, 'Ovocný čaj', '', 3, 33, NULL, 0, 0, 1, NULL, '2020-06-16 07:10:09', '2020-12-05 22:51:18'),
(19, 'Jablko', NULL, 7, 39, NULL, 0, 0, 1, NULL, '2020-06-16 07:10:22', '2020-12-05 22:51:18'),
(27, 'Vepřová játra na cibulce', 'Omytá játra zbavíme blan a žilek a nakrájíme na stejnoměrné podlouhlé kousky. Cibuli oloupeme a jemně nakrájíme. Nakrájenou cibuli osmažíme na oleji do růžova, přidáme nakrájená játra, drcený kmín a dusíme ve vlastní šťávě do poloměkka. Vydušenou šťávu na tuk zaprášíme moukou, osmahneme, zalijeme vodou, rozšleháme a provaříme. Nakonec hotový pokrm osolíme.', 1, 7, 3, 0, 0, 1, NULL, '2020-06-16 16:50:29', '2020-12-05 22:51:38'),
(29, 'Okurková limonáda', '', 3, 35, NULL, 0, 0, 1, NULL, '2020-06-16 16:54:32', '2020-12-05 22:51:18'),
(30, 'Kuře s dýní', 'Porci kuřete vykostíme, nakrájíme na menší kousky, osolíme a posypeme kořením. Opečeme na sucho, podlijeme a dusíme. K poloměkkému masu přidáme oloupanou a na nudle nakrájenou dýni a společně dusíme. Pokrm zjemníme tukem.', 1, 2, 2, 0, 0, 1, NULL, '2020-06-16 17:48:55', '2020-12-05 22:51:38'),
(31, 'Broskev', NULL, 7, 39, NULL, 0, 0, 1, NULL, '2020-06-16 17:49:49', '2020-12-05 22:51:18'),
(39, 'Hovězí maso po stroganovsku', 'Z masa nakrájíme nudličky asi 1 cm široké a 4 cm dlouhé. Na tuku osmahneme cibuli, vložíme maso, osolíme, přidáme koření a dusíme ve vlastní šťávě. Přidáme rajský protlak, chvíli povaříme, pak přidáme mouku rozmíchanou v mléce. Nakonec vmícháme smetanu, vložíme máslo na zjemnění, dochutíme a provaříme. Podáváme s houskovým knedlíkem nebo dušenou rýží.', 1, 11, 3, 0, 0, 1, NULL, '2020-10-11 16:34:26', '2020-12-05 22:51:18'),
(40, 'Domácí citronáda', 'Do čerstvé vody přidáme vymačkanou šťávu s citronem a přidáme lžičku cukru.', 3, 34, NULL, 0, 0, 1, NULL, '2020-10-12 07:36:47', '2020-12-05 22:51:18'),
(41, 'Polévka kmínová', 'Očištěnou kořenovou zeleninu nastrouháme na hrubo, přidáme vývar z kostí a kmínu a vaříme. Když je zelenina měkká, zahustíme polévku nasucho opraženou moukou. Do povařené polévky přidáme rozšlehané vejce a tuk.', 2, 20, NULL, 0, 0, 1, NULL, '2020-10-12 09:32:12', '2020-12-05 22:51:18'),
(42, 'Hruška', NULL, 7, 39, NULL, 0, 0, 1, NULL, '2020-10-12 09:34:51', '2020-12-05 22:51:18'),
(43, 'Zelný salát s ananasem', 'Zelí nadrobno nakrouháme a promačkáme s cukrem a solí, až změkne. Lze dát i do robotu a pomocí hnětače promísit. Přidáme kompot i s nálevem a citrónovou šťávu.', 5, NULL, NULL, 0, 0, 1, NULL, '2020-10-12 09:39:48', '2020-12-05 22:51:18'),
(44, 'Křehké řezy s tvarohem', 'Mouku prosejeme s kypřícím práškem, přidáme cukr, tuk, mléko a vejce. Zpracujeme těsto, vyválíme na plát, dáme na plech, přidáme náplň, navrch dáme další plát, nebo můžeme dát mřížky a upečeme. Po vychladnutí nakrájíme porce a pocukrujeme prosátým moučkovým cukrem.', 6, NULL, NULL, 0, 0, 1, NULL, '2020-10-12 09:40:51', '2020-12-05 22:51:18'),
(45, 'Neapolské nudle', 'Na části tuku osmažíme drobně pokrájenou cibuli, přidáme na drobné nudličky pokrájené vepřové maso, po chvíli přidáme na nudličky pokrájený celer a uzené maso, utřený česnek se solí, kečup, mleté koření a vše orestujeme. Na pekáč navrstvíme polovinu uvařených těstovin, přidáme vrstvu masa se zeleninou a zakryjeme druhou polovinou těstovin. Navrch posypeme strouhaným sýrem a dáme zapéci do trouby do zlatova.', 1, 8, 3, 0, 0, 1, NULL, '2020-10-12 14:14:20', '2020-12-05 22:51:18'),
(46, 'Polévka česneková s ovesnými vločkami', 'Vločky vaříme do měkka. Nakonec přidáme utřený česnek, koření, sádlo, krátce povaříme a přidáme pažitku nebo petrželku.', 2, 26, NULL, 1, 0, 1, NULL, '2020-10-12 17:54:51', '2020-12-05 22:51:18'),
(47, 'Francouzské kuře na pomerančích (SD)', 'Mrkev oškrábeme, omyjeme a nakrájíme na kostičky. Pomeranče omyjeme a oloupeme. Nakrájíme na kostky. Očištěné a kůže zbavené porce kuřete osolíme a okořeníme pomerančovou kůrou. Mrkev lehce na sucho orestujeme. Přesuneme do pekáče, přidáme pokrájené pomeranče, bobkový list a dle tolerance tymián. Naklademe porce masa, podlijeme vodou a přikryté pečeme v troubě. Během pečení podle potřeby podléváme vařící vodou a přeléváme šťávou. Upečené porce vyjmeme, výpek propasírujeme. Zahustíme škrobem rozmíchaným v troše vody, 5 minut provaříme. Nakonec zjemníme kouskem čerstvého másla. Při podávání kuřecí porce přeléváme pomerančovou omáčkou.', 1, 2, 2, 0, 0, 1, NULL, '2020-10-20 14:38:41', '2020-12-05 22:51:18'),
(48, 'Kuřecí terina s rajčatovou krustou, bramborová kaše s řapíkatým celerem', 'Sekané kuřecí maso s bulgurem, mrkví, ochucené italským kořením a lněným semínkem. Fáš překrytá rajčatovou směsí, sypaná strouhaným sýrem a upečená. Přílohu tvoří bramborová kaše s rostlinnou smetanou a řapíkatým celerem. Kuřecí terina: Kuřecí stehenní řízky překrájíme, vložíme do kutru, zalijeme smetanou, přidáme bulgur uvařený v kuřecím bujónu, mrkev a rozkutrujeme - rozsekáme v blixeru. Pokud nemáme kutr, tak maso a další suroviny jemně semeleme. Do fáše zamícháme dětské koření Itálie, předem namočené lněné semínko (pustí šlem a váže) a rozetřeme do vymazané a strouhanou houskou vysypané GN.\nRajčatová krusta: Krájená rajčata v tomatové šťávě promícháme s koncentrovaným rajčatovým protlakem, bramborovým škrobem a osolíme. Směs rozetřeme přes masovou fáš. Vložíme do vyhřátého konvektomatu a pečeme při 150 °C asi 20 minut (podle zvolené výšky masa). Před dopečením posypeme strouhaným sýrem. Upečené maso před výdejem krájíme na požadované porce.\nBramborová kaše s řapíkatým celerem: Bramborovou kaši připravíme podle návodu. Použijeme přitom 940 ml vroucí vody a 150 ml studené vody. Řapíkatý celer překrájíme, vložíme do blixeru, přidáme smetanu a společně rozmixujeme najemno. Směs zlehka přimícháme do kaše. Nešleháme.\nSalát: Rukolu promícháme s kukuřicí a olivovým olejem.\nServis: Při podávání bramborovou kaši doplníme rukolovým salátem a přikládáme porce kuřecí teriny.', 1, 2, 2, 0, 0, 1, NULL, '2020-10-20 15:10:37', '2020-12-05 22:51:18'),
(49, 'Krůtí stehna v medové marinádě', 'Krůtí stehna prošpikujeme slaninou, osolíme, opepříme. Odložíme 100 g oleje. Z ostatních surovin zhotovíme marinádu a důkladně promícháme a vložíme do ní krůtí stehna. V chladu necháme uležet. Stehna naskládáme do pekáče, podlijeme olejem a trochou vody a pečeme do měkka.', 1, 2, 2, 0, 0, 1, NULL, '2020-10-26 11:40:01', '2020-12-05 22:51:18'),
(50, 'Italské kuřecí medailonky s bulgurovým salátem a pažitkovým pestem', 'Kuřecí plátky okořeněné italským kořením, pozvolna upravené, podávané se salátem ze zeleniny a bulguru, doplněné pažitkovým pestem. Italské kuřecí medailonky: Kuřecí stehna nakrájíme na medailonky, okořeníme dětským kořením Itálie a naskládáme do vymazané GN. Povrch masa pokapeme olivovým olejem. Můžeme nechat v chladu chvíli marinovat. Upravujeme ve vyhřátém konvektomatu na kombinovaný provoz při 140 °C asi 20 minut, výpek z masa přidáme do salátu.\nBulgurový salát: Bulgur uvaříme podle návodu v zeleninovém bujónu. Do chladného bulguru přidáme kukuřici, krájená rajčata v tomatové šťávě, překrájená cherry rajčata a výpek z masa. Ochutíme citrónovou šťávou, olivovým olejem a sekanou petrželkou. Zlehka promícháme a vychladíme.\nPažitkové pesto: Panenský olivový olej promícháme s citrónovou šťávou a drobně krájenou pažitkou.\nServis: Při podávání klademe k salátu kuřecí medailonky a doplníme pažitkovým pestem.', 1, 4, 3, 0, 0, 1, NULL, '2020-10-26 11:42:27', '2020-12-05 22:51:18'),
(51, 'Čínské kuře jednoduché', 'Kuřecí maso pokrájíme na kousky a marinujeme v sójové omáčce a citronové šťávě 1-2 hodiny. Maso obalíme v mouce a na rozpáleném oleji pečeme do měkka.', 1, 3, 3, 0, 0, 1, NULL, '2020-10-26 11:43:54', '2020-12-05 22:51:18'),
(52, 'Kuřecí maso na medu a zázvoru s jasmínovou rýží', 'Orestované nudličky kuřecího masa ve sladkokyselé omáčce s medem a zázvorem podávané s jasmínovou rýží. Kuřecí prsa nakrájíme na nudličky 0,5x0,5x5 cm, osolíme (10 g), naložíme do řidšího škrobového těstíčka připraveného ze škrobu (30 g), vody (30 ml), vajec nebo melanže a oleje (100 ml) a necháme odležet. Ve vodě (720 ml) rozmícháme dochucovací omáčku Wok med a zázvor, zahustíme škrobem (70 g) namočeným ve vodě (60 ml) a za stálého míchání vaříme 1 minutu. Zeleninu nakrájíme na nudličky. Naložené maso vložíme do GN (1 GN- 2,1 kg masa) a pečeme v předehřátém konvektomatu při 180°C asi 15 minut. Maso v polovině pečení pomocí kuchařské vidličky oddělíme na jednotlivé kousky a poté dopečeme. Upečené maso promícháme se zeleninou, zalejeme hotovou omáčkou, promícháme a pečeme v konvektomatu před výdejem při 180°C asi 5 minut. Přílohu tvoří jasmínová rýže připravená podle návodu (voda 800 ml, sůl 16 g, olej 50 ml). Při podávání klademe na rýži maso s omáčkou a zeleninou.', 1, 3, 3, 0, 0, 1, NULL, '2020-10-26 11:46:05', '2020-12-05 22:51:18'),
(53, 'Kachní stehýnko zapečené na šťouchaných bramborech s jablky', 'Kachní stehýnka si upečeme na kmínu do měkka. Hotová kachní stehna obereme od kosti. Do gastro nádoby vložíme šťouchané brambory, na závěr obrané kachní maso, ka které dáme okapaná sterilovaná jablka. Vše krátce zapečeme v troubě nebo v konvektomatu tak, aby se chutě spojily. Hotový pokrm v gastro nádobě naporcujeme a servírujeme na talíř.', 1, 2, 2, 0, 0, 1, NULL, '2020-10-26 11:48:53', '2020-12-05 22:51:18'),
(54, 'Kuřecí nudličky v liškové omáčce se spätzlemi', '', 1, 4, 3, 0, 0, 1, NULL, '2020-10-26 11:50:54', '2020-12-05 22:51:38'),
(55, 'Kuřecí nudličky v liškové omáčce s bramborem', 'Brambory připravíme podle návodu, popláchneme studenou vodou a promastíme. Kuřecí prsa nakrájíme na nudličky, okořeníme vegetou, zakápneme olejem a necháme chvíli marinovat. Tepelně upravujeme v předehřátém konvektomatu na kombinovaný provoz s teplotou 140 °C asi 15 minut. Houbovou omáčku z lišek a žampiónů uvaříme podle návodu. Maso vložíme do omáčky a krátce povaříme.\nPři podávání zregenerované spätzle částečně polijeme houbovou omáčkou s masem a sypeme sekanou petrželkou.', 1, 2, 2, 0, 0, 1, NULL, '2020-10-26 11:52:10', '2020-12-05 22:51:18'),
(56, 'Penne s omáčkou Quattro Formaggi a kuřecím masem', 'Kousky kuřecího masa v jemné sýrové omáčce doplněné těstovinou.\nPenne uvaříme podle návodu, promastíme a před podáváním je regenerujeme v konvektomatu. Kuřecí prsa nakrájíme na nudličky, okořeníme vegetou a namarinujeme olejem. Naložené maso vložíme do GN (1 GN maximálně 2,1 kg masa) a tepelně upravujeme v předehřátém konvektomatu na kombinovaný program při 140°C asi 15 minut. Maso po pěti minutách dušení oddělíme kuchařskou vidličkou a poté dodusíme. Omáčku připravíme podle návodu. Tepelně upravené maso vložíme do hotové omáčky a promícháme.\nPři podávání penne přelijeme omáčkou s masem. Pokrm ozdobíme čerstvou bazalkou.', 1, 4, 3, 0, 0, 1, NULL, '2020-10-26 11:54:29', '2020-12-05 22:51:18'),
(57, 'Kanadské kuře', 'Smícháme mouku s paprikou, kmínem a vegetou. V této směsi obalíme po obou stranách nasolené kuře. Dáme na olejem vymazaný plech, porce potřeme máslem. Nepodléváme. Podáváme s bramborovou kaší a salátem.', 1, 2, 2, 0, 0, 1, NULL, '2020-10-26 11:56:49', '2020-12-05 22:51:18'),
(58, 'Krůtí v paprikové omáčce', 'Nakrájené maso na nudličky orestujeme na zlatavé cibulce, přidáme mletou papriku, podlijeme a dusíme do měkka. Dále přidáme čerstvou nakrájenou papriku, zahustíme, provaříme a dochutíme. Nakonec zjemníme smetanou.\n\n', 1, 4, 3, 0, 0, 1, NULL, '2020-10-26 11:58:29', '2020-12-05 22:51:38'),
(59, 'Kuře na rajčatech', 'Kuřecí (krůtí) plátek naklepeme, osolíme, posypeme kořením, opečeme na sucho, podlijeme vodou, přidáme nakrájenou cibuli a dusíme. Rajčata oloupeme, nakrájíme na plátky a přidáme k téměř měkkému masu. Společně dodusíme a šťávu zjemníme tukem.', 1, 2, 2, 0, 0, 1, NULL, '2020-10-26 12:00:21', '2020-12-05 22:51:18'),
(60, 'Kuře dušené', 'Maso osolíme, opečeme nasucho, podlijeme vodou a dusíme do měkka. Šťávu zahustíme nasucho opraženou moukou a zjemníme tukem.', 1, 2, 2, 0, 0, 1, NULL, '2020-10-26 12:01:08', '2020-12-05 22:51:18'),
(61, 'Kuřecí nudličky na pórku', 'Maso nakrájíme na nudličky a zprudka osmahneme na oleji. Do odměřeného množství vody vmícháme směs minutka čína a zvolna vaříme. Připravenou směsí zalijeme maso a vaříme asi 10 minut. Přidáme pórek, vše promícháme a dusíme asi 10 minut.', 1, 3, 3, 0, 0, 1, NULL, '2020-10-26 12:02:31', '2020-12-05 22:51:18'),
(62, 'Kuře pečené na zelenině', 'Do pekáče vložíme naporcované kuře, osolíme, opepříme, posypeme nakrájenou slaninou, cibulí a částí petrželky. Pokapeme olejem a dáme do trouby. Přikryté pečeme asi 20 minut. Potom podlijeme vývarem a pečeme dalších 40 minut. Na posledních 10 – 15 minut přidáme do pekáče zeleninové lečo. Hotové kuře posypeme zbývající petrželkou.', 1, 2, 2, 0, 0, 1, NULL, '2020-10-26 12:03:43', '2020-12-05 22:51:18'),
(63, 'Rýže s kuřetem Hanoi', 'Do horkého oleje vlijeme rozmíchaná vejce a usmažíme je. Poté přidáme na kostičky nakrájenou cibuli, očištěný a opláchnutý pórek, paprikové lusky a na plátky nakrájený česnek, žampióny a společně osmahneme. Potom přidáme na kostičky nakrájené vařené kuřecí maso, osolíme a dochutíme. Směs smícháme s teplou vařenou rýží a krátce společně podusíme.', 1, 3, 3, 0, 0, 1, NULL, '2020-10-26 12:06:37', '2020-12-05 22:51:38'),
(64, 'Hovězí plátky na rozmarýnu s bramborem', 'Hovězí zadní naporcujeme na plátky, naklepeme, osolíme, opepříme a posypeme rozmarýnem. Na rozpáleném tuku zpěníme drobně pokrájenou cibuli, vložíme připravené plátky a opečeme je z obou stran. Podlijeme vývarem a dusíme do měkka. Hotovou šťávu slijeme a propasírujeme.', 1, 10, 2, 0, 0, 1, NULL, '2020-10-26 12:43:33', '2020-12-05 22:51:38'),
(65, 'Hovězí plátky na rozmarýnu s rýží', 'Hovězí zadní naporcujeme na plátky, naklepeme, osolíme, opepříme a posypeme rozmarýnem. Na rozpáleném tuku zpěníme drobně pokrájenou cibuli, vložíme připravené plátky a opečeme je z obou stran. Podlijeme vývarem a dusíme do měkka. Hotovou šťávu slijeme a propasírujeme.', 1, 11, 3, 0, 0, 1, NULL, '2020-10-26 12:44:24', '2020-12-05 22:51:18'),
(66, 'Vepřové plátky na rozmarýnu s rýží', 'Vepřové zadní naporcujeme na plátky, naklepeme, osolíme, opepříme a posypeme rozmarýnem. Na rozpáleném tuku zpěníme drobně pokrájenou cibuli, vložíme připravené plátky a opečeme je z obou stran. Podlijeme vývarem a dusíme do měkka. Hotovou šťávu slijeme a propasírujeme.', 1, 7, 3, 0, 0, 1, NULL, '2020-10-26 12:46:33', '2020-12-05 22:51:18'),
(67, 'Vepřové plátky na rozmarýnu s bramborem', 'Vepřové zadní naporcujeme na plátky, naklepeme, osolíme, opepříme a posypeme rozmarýnem. Na rozpáleném tuku zpěníme drobně pokrájenou cibuli, vložíme připravené plátky a opečeme je z obou stran. Podlijeme vývarem a dusíme do měkka. Hotovou šťávu slijeme a propasírujeme.', 1, 6, 2, 0, 0, 1, NULL, '2020-10-26 12:47:12', '2020-12-05 22:51:18'),
(68, 'Vepřový soudek s kořenovou zeleninou na divoko', 'Z vepřové kýty nakrájíme plátky, naklepeme, okořeníme vegetou, naplníme kořenovou zeleninou krájenou na hranolky a zatočíme. Z okrajů kouká zelenina. Skládáme těsně vedle sebe do vymaštěné GN, povrch masa osolíme, posypeme divokým kořením, kostičkami slaniny a cibule. Nakonec maso potřeme olejem a necháme marinovat. Poté v konvektomatu nastavíme program vaření, vložíme maso a upravujeme 15 minut. Dále si nastavíme kombinovaný program s teplotou 140 °C a upravujeme asi 40 minut doměkka. Během úpravy maso přeléváme výpekem. Měkké maso vyjmeme, výpek použijeme při přípravě vepřové šťávy, kterou uvaříme podle návodu( vodu do šťávy ponížíme o množství výpeku).', 1, 6, 2, 0, 0, 1, NULL, '2020-10-26 12:48:55', '2020-12-05 22:51:38'),
(69, 'Vepřové s mandlemi a jasmínovou rýží', 'Kýtu nakrájíme na plátky o velikosti 3 x 3 cm, osolíme, naložíme do řidšího škrobového těstíčka s olejem (škrob, voda, vejce nebo melanž, olej) a necháme odležet. Minutku Čína připravíme podle návodu a ochutíme zázvorem. Pórek, cibuli a kapii nakrájíme na kostičky. Naložené maso vložíme do GN (1 GN- 2,1 kg masa) a pečeme v předehřátém konvektomatu při 180°C asi 20 minut. Maso po 5 minutách pečení pomocí kuchařské vidličky oddělíme na jednotlivé kousky a poté dopečeme.\nV rozpáleném oleji orestujeme spařené a oloupané mandle. Po orestování mandle vložíme do omáčky. Upečené maso promícháme se zeleninou, zalejeme hotovou omáčkou, promícháme a pečeme v konvektomatu při 180°C asi 5 minut. Dále udržujeme při teplotě do 75°C, aby zelenina zůstala na skus tužší.\nJako přílohu servírujeme jasmínovou rýži připravenou podle návodu.', 1, 7, 3, 0, 0, 1, NULL, '2020-10-26 12:50:43', '2020-12-05 22:51:18'),
(70, 'Vepřová pečeně s liškovou omáčkou a bramborovými špalíčky', 'Vepřovou pečeni bez kosti okořeníme vegetou, pokapeme olejem a necháme chvíli v chladu marinovat. Poté upravujeme ve vyhřátém konvektomatu na kombinovaný provoz 130°C asi 90 minut doměkka nebo můžeme pozvolna péci s trochou vody v elektrické troubě. Maso před podáváním krájíme na porce a regenerujeme.\nHoubovou omáčku z lišek a žampionů uvaříme podle návodu. Mražené lišky necháme částečně rozmrazit, překrájíme je a vložíme do rozpuštěného másla. Pozvolna dusíme doměkka. Podle potřeby zakápneme vodou. Dušené vložíme do omáčky.\nPříloha: Bramborové těsto zamícháme podle návodu, po chvilce vyvalujeme delší váleček, z kterého kartou krájíme kratší špalíčky. Vkládáme je do vymaštěné GN a vaříme v konvektomatu asi 10 minut (podle velikosti). Vařené pomastíme.\nPři podávání klademe špalíčky na omáčku a přikládáme plátek masa. Ozdobíme snítkem petrželové natě.', 1, 6, 2, 0, 0, 1, NULL, '2020-10-26 12:52:16', '2020-12-05 22:51:18'),
(71, 'Zapečené těstoviny s vepřovým masem a květákem', 'Vařené těstoviny promícháme s upečeným na nudličky pokrájeným vepřovým masem. Přidáme nakrájenou osmaženou cibuli, nakrájený a krátce povařený květák, zalijeme vejci rozmíchanými s mlékem a zapečeme v konvektomatu.', 1, 8, NULL, 0, 0, 1, NULL, '2020-10-26 12:56:39', '2020-12-05 22:51:18'),
(72, 'Francouzské brambory', 'Brambory uvaříme ve slupce. Uvařené a oloupané brambory nakrájíme, přidáme nakrájené uzené maso, jemně nakrájenou cibuli osmaženou na tuku, sůl, kmín, hrášek, vše promícháme a v troubě zapečeme. Zalijeme vejci rozšlehanými s mlékem a dopečeme.', 1, 6, NULL, 1, 0, 1, NULL, '2020-10-26 12:58:08', '2020-12-05 22:51:38'),
(73, 'Hovězí maso po stroganovsku', 'Z masa nakrájíme nudličky asi 1 cm široké a 4 cm dlouhé. Na tuku osmahneme cibuli, vložíme maso, osolíme, přidáme koření a dusíme ve vlastní šťávě. Přidáme rajský protlak, chvíli povaříme, pak přidáme mouku rozmíchanou v mléce. Nakonec vmícháme smetanu, vložíme máslo na zjemnění, dochutíme a provaříme. Podáváme s houskovým knedlíkem nebo dušenou rýží.', 1, 11, 3, 0, 0, 1, NULL, '2020-10-26 13:01:40', '2020-12-05 22:51:38'),
(74, 'Barevné vepřové nudličky', 'Vepřovou kýtu nakrájíme na nudličky, osolíme, okořeníme a opečeme na horkém oleji, vyjmeme. Přidáme cibuli a opečeme. Vložíme na nudličky nakrájenou mrkev (nebo baby karotku), fazolové lusky, kečup, podlijeme vodou, dosolíme a krátce podusíme. Ve smetaně rozmícháme mouku, zahustíme, dochutíme, vložíme maso a necháme projít varem.', 1, 7, 3, 0, 0, 1, NULL, '2020-10-26 13:03:33', '2020-12-05 22:51:38'),
(75, 'Kořeněné těstoviny s vepřovým masem', 'Těstoviny uvaříme v osolené vodě, scedíme a promícháme s 10 g oleje. Cibuli nakrájíme nadrobno, osmahneme na oleji, přidáme na malé nudličky nakrájené maso, prolisovaný česnek, bobkový list, koření, oregáno, sůl, rajský protlak, podlijeme vodou a dusíme do měkka. Bobkový list vyjmeme, maso dochutíme, zahustíme moukou a provaříme. Těstoviny poléváme masovou směsí a sypeme nastrouhaným sýrem.', 1, 8, 3, 0, 0, 1, NULL, '2020-10-26 13:05:23', '2020-12-05 22:51:18'),
(76, 'Hovězí dušené kostky po zahradnicku', 'Maso pokrájíme na kostky, opečeme na sucho, podlijeme vývarem a dusíme. Později přidáme nastrouhanou zeleninu a dodusíme. Šťávu zahustíme moukou, zjemníme máslem a dochutíme solí, kořením a zelenou natí. Zeleninu se šťávou rozmixujeme.', 1, 9, 1, 0, 0, 1, NULL, '2020-10-26 13:20:28', '2020-12-05 22:51:18'),
(77, 'Sekaná z rybího filé', 'K rozemletému filé přidáme žemli namočenou ve vodě, vejce a sůl. Vytvarujeme karbanátky a pečeme podlité vodou.', 1, 13, 2, 0, 0, 1, NULL, '2020-10-26 13:25:10', '2020-12-05 22:51:18'),
(78, 'Hovězí guláš moravský s knedlíkem', 'Maso nakrájíme na kostky, opečeme na sucho, osolíme, podlijeme vodou, přidáme protlak a dusíme. K poloměkkému masu přidáme syrové nastrouhané brambory, mletý kmín, majoránku a dodusíme. Nakonec zahustíme moukou, provaříme a zjemníme máslem.', 1, 9, 1, 0, 0, 1, NULL, '2020-10-26 13:28:37', '2020-12-05 22:51:18'),
(79, 'Hovězí pečeně frankfurtská', 'Maso protáhneme šunkou, na sucho opečeme, osolíme, přidáme rajský protlak, podlijeme vodou a dusíme. Šťávu zahustíme moukou, necháme provařit a zjemníme máslem.', 1, 9, 1, 0, 0, 1, NULL, '2020-10-26 13:33:02', '2020-12-05 22:51:18'),
(80, 'Telecí plátek s pomerančovou omáčkou s knedlíkem', 'Plátek masa naklepeme, osolíme a poprášíme moukou. Nasucho opečeme, podlijeme vodou a dusíme do měkka. Šťávu vydusíme a na plátek dáme tuk. Na talíři polijeme omáčkou.\nOmáčka: pomerančovou kůru, džus, džem a med smícháme a vaříme za stálého míchání asi 5 minut.', 1, 9, 1, 0, 0, 1, NULL, '2020-10-26 13:36:34', '2020-12-05 22:51:38'),
(81, 'Telecí plátek s bešamelem', 'Plátek masa naklepeme, opečeme na sucho, osolíme, podlijeme a dusíme do měkka. Opraženou mouku na sucho zalijeme mlékem a uvaříme hustou kaši. Přidáme máslo a necháme vychladnout. Pak vmícháme sůl, žloutek, strouhaný sýr, posekanou petrželovou nať a z bílku sníh. Maso přendáme na vymaštěný pekáč, potřeme bešamelem a ve vodní lázni zapečeme. Před podáním podlijeme přírodní šťávou z masa.', 1, 10, 2, 0, 0, 1, NULL, '2020-10-26 13:44:30', '2020-12-05 22:51:18'),
(82, 'Hovězí maso v zelenině', 'Očištěné maso nakrájíme na kostky, osolíme, opečeme nasucho, podlijeme vodou a dusíme. K poloměkkému masu přidáme očištěnou, jemně nastrouhanou zeleninu. Když je vše měkké, zahustíme zálivkou z nasucho opražené mouky a studené vody. Necháme dobře provařit a zjemníme tukem.', 1, 12, 3, 1, 0, 1, NULL, '2020-10-26 14:24:19', '2020-12-05 22:51:18'),
(83, 'Hovězí maso po italsku', 'Maso osolíme a opečeme na teflonové pánvi. Podlijeme vodou, přidáme rajský protlak a dusíme do měkka. Šťávu zahustíme na sucho opraženou moukou a dobře provaříme. Dochutíme cukrem, citrónovou šťávou a zjemníme tukem.', 1, 12, 3, 0, 0, 1, NULL, '2020-10-26 14:31:02', '2020-12-05 22:51:38'),
(84, 'Hovězí dušené v mrkvi', 'Technologický postup je stejný jako v předchozím receptu.', 1, 12, 3, 0, 0, 1, NULL, '2020-10-26 14:36:12', '2020-12-05 22:51:18'),
(85, 'Sumeček v parmazánovém těstíčku s bramborovou kaší', 'Z mléka, vajec, hladké mouky a parmazánu si připravíme hustější těstíčko, ve kterém obalíme osolený a opepřený filet ze sumečka nakrájený na jednotlivé porce. Opečeme na oleji dozlatova, případně dopečeme v troubě. Bramborovou kaši připravíme podle návodu na obalu a zjemníme kouskem másla.\nHotového sumečka podáváme s bramborovou kaší.', 1, 13, NULL, 0, 0, 1, NULL, '2020-10-26 14:40:26', '2020-12-05 22:51:18'),
(86, 'Sumeček s grilovanou zeleninou a bramborovou kaší', 'Filet ze sumečka nakrájíme na jednotlivé porce. Poté je osolíme, opepříme a opečeme na olivovém oleji z obou stran (můžeme též upéct v konvektomatu na 200 °C po dobu 15min). Na takto opečená filátka přidáme máslo a necháme v teple dojít. Očištěnou zeleninu nakrájíme na drobné kousky (každý druh může být jinak nakrájený, čímž získáme pestřejší vzhled) a orestujeme na olivovém oleji. Zeleninu v průběhu restování ochutíme solí a cukrem, abychom zvýraznili chuť zeleniny. Přidáme kousek másla a čerstvý estragon. Zelenina by měla zůstat tvrdá tak akorát. Bramborovou kaši připravíme dle návodu.\nOpečené filety ze sumečka podáváme na bramborovém pyré.', 1, 13, 2, 0, 0, 1, NULL, '2020-10-26 14:42:38', '2020-12-05 22:51:18'),
(87, 'Treska s kukuřičnými lupínky, lehký bramborový salát', 'Lehký bramborový salát s řapíkatým celerem a jogurtovou majonézou: Oloupané brambory nakrájíme na drobné kostičky a uvaříme v osolené vodě nebo v děrované GN v konvektomatu. Mrkev a petržel uvaříme tak, aby zelenina zůstala pevná. Chladnou zeleninu, řapíkatý celer, kyselé okurky, oloupané a vyjadřincované jablko a cibuli nakrájíme na drobné kostičky. Majonézu promícháme s jogurtem a dijonskou hořčicí. Brambory, hrášek a zeleninu s jablky opatrně promícháme s ochucenou majonézou. Nakonec dle zvyklostí ochutíme solí a pepřem (v případě dětských strávníků doporučujeme vynechat). Treska s kukuřičnými lupínky Vitana: Tresku s kukuřičnými lupínky upečeme podle návodu.\nServis: Při podávání k salátu přikládáme dva kousky ryb a doplňujeme měsíčky citrónu. Pokrm můžeme ozdobit vhodnou bylinkou.', 1, 13, 2, 0, 0, 1, NULL, '2020-10-26 15:03:11', '2020-12-05 22:51:18'),
(88, 'Medailonky z lososa se žampiónovou omáčkou a bramborové noky', 'Medailonky necháme rozmrazit a lehce je dochutíme solí. Cibuli jemně nakrájíme, mírně zpěníme na tuku, přidáme prosátou mouku a připravíme světlou zásmažku. Zalijeme ji prochladlým vývarem (50 ml na 1porci) a mlékem, pečlivě rozšleháme, osolíme a za občasného míchání provaříme.\nKe konci varu zjemníme smetanou, dochutíme octem, procedíme a přidáme žampiony, které předem osmahneme na másle. Omáčku opět provaříme.\nBramborové noky uvaříme v osolené vodě a po uvaření je prohodíme na rozpuštěném másle.\nMedailonky z lososa připravíme v troubě při teplotě 80°C cca 20 minut.', 1, 13, 2, 0, 0, 1, NULL, '2020-10-26 15:06:30', '2020-12-05 22:51:18'),
(89, 'Losos s bylinkovou omáčkou', 'Lososa upečeme na másle v konvektomatu. Připravíme klasickou cibulovou jíšku, zalijeme vývarem, provaříme a přidáme bylinky, dochutíme. Nakonec můžeme zjemnit smetanou.', 1, 13, 2, 0, 0, 1, NULL, '2020-10-26 16:19:35', '2020-12-05 22:51:38'),
(90, 'Filety z aljašské tresky s bylinkovou krustou na zapečených bramborech', 'Tresku pozvolna rozmrazíme. Okořeníme rybím kořením a solí, přendáme na vymazaný smaltovaný plech. Do strouhanky přidáme provensálské bylinky, sůl, vejce a smetanu. Takto připravenou hmotu zamícháme a klademe na filety tresky, poté vložíme do konvektomatu na režim horký vzduch 170°C po dobu cca 10 minut. Brambory si nakrájíme na plátky, přidáme smetanu, mix sýrů, ochutíme solí a pepřem. Takto připravené brambory zapečeme v konvektomatu na smaltovaném plechu na režim horký vzduch 180°C po dobu cca 15 minut.', 1, 13, NULL, 0, 0, 1, NULL, '2020-10-26 16:20:53', '2020-12-05 22:51:38'),
(91, 'Rybí filé se šunkovou omáčkou', 'Filé upečeme klasicky po mlynářsku. Z mouky, másla a mléka si připravíme řidší bešamel, do kterého přidáme na nudličky nakrájenou orestovanou šunku. Provaříme a dochutíme, můžeme přidat nasekanou petrželku.', 1, 13, 2, 0, 0, 1, NULL, '2020-10-26 16:22:17', '2020-12-05 22:51:38'),
(93, 'Bílé fazole s uzeným masem, klobásou a rajčaty', 'Umyté, předem namočené fazole uvaříme ve slané vodě do měkka a slijeme. Uzené maso uvaříme. Na tuku zpěníme nadrobno pokrájenou cibuli, zaprášíme moukou, přidáme papriku, opražíme a rozředíme vývarem z uzeného masa. Vaříme, až vše zhoustne, pak přidáme utřený česnek, kečup, uvařené fazole a na nudličky pokrájené uzené maso, na tenčí kolečka klobásu, které jsme předem orestovali na tuku. Nakonec přidáme rajčata, aby se nerozvařila.', 1, 17, 5, 0, 0, 1, NULL, '2020-10-26 16:28:48', '2020-12-05 22:51:18'),
(95, 'Čočka na kyselo', 'Přebranou a vypranou čočku zalijeme studenou vodou a necháme 1/2 hodiny máčet. Pak ji ve studené vodě uvaříme do poloměkka, zahustíme cibulovou jíškou a dovaříme. Nakonec čočku dochutíme solí,octem a cukrem.\nČočku podáváme jako přílohu nebo jako samostatný pokrm s chlebem a zeleninovým salátem.\nMastíme ji dorůžova osmaženou cibulkou.', 1, 14, 5, 0, 0, 1, NULL, '2020-10-26 16:47:56', '2020-12-05 22:51:18'),
(96, 'Čočka na kyselo s párkem', 'Přebranou a vypranou čočku zalijeme studenou vodou a necháme 1/2 hodiny máčet. Pak ji ve studené vodě uvaříme do poloměkka, zahustíme cibulovou jíškou a dovaříme. Nakonec čočku dochutíme solí,octem a cukrem.\nČočku podáváme jako přílohu nebo jako samostatný pokrm s chlebem a zeleninovým salátem.\nMastíme ji dorůžova osmaženou cibulkou.', 1, 17, 5, 0, 0, 1, NULL, '2020-10-26 16:50:09', '2020-12-05 22:51:38'),
(97, 'Cizrnová kaše  s uzeným masem a okurkou', 'Cizrnovou kaši připravíme podle klasické receptury.\nNakrájenou šunku na nudličky smícháme s ostatními přísadami smažíme klasické omelety.', 1, 17, NULL, 1, 0, 1, NULL, '2020-10-26 16:52:00', '2020-12-05 22:51:38'),
(98, 'Cizrnová kaše s okurkou a pečivem', 'Cizrnovou kaši připravíme podle klasické receptury.', 1, 14, 5, 0, 0, 1, NULL, '2020-10-26 16:52:44', '2020-12-05 22:51:38'),
(99, 'Fazolový salát s vejci a okurkou', 'Uvaříme fazole a vejce, vychladíme a promícháme s nakrájenou zeleninou, vejci a dochutíme.', 1, 14, 5, 0, 0, 1, NULL, '2020-10-26 16:53:43', '2020-12-05 22:51:18'),
(100, 'Fazolový guláš se sojovým masem', 'Namočené fazole uvaříme do měkka. Sojové maso (plátky) uvaříme, necháme vychladnout, nakrájíme na kostky. Dále postupujeme stejně jako u klasického guláše.', 1, 17, 5, 0, 0, 1, NULL, '2020-10-26 16:54:51', '2020-12-05 22:51:18'),
(101, 'Čočka po srbsku s okurkou a rohlíkem', 'Čočku přebereme, propláchneme a uvaříme. Na tuku zpěníme cibuli, přidáme lečo a vše nalijeme do čočky. Zahustíme moukou rozmíchanou ve vodě. Dochutíme a necháme povařit.', 1, 14, 5, 0, 0, 1, NULL, '2020-10-26 16:56:43', '2020-12-05 22:51:18'),
(102, 'Pizza Hawai s trhanými saláty', 'Pizza těsto potřené tomatovou omáčkou, sypané šunkou, ananasem, paprikou a sýrem. Podávané se salátem.\nPizzu Hawai upečeme podle návodu (můžeme po upečením trochu posypat sýrem). Při podávání přikládáme k pizze směs trhaných salátů ochucených olivovým olejem.', 1, 18, NULL, 0, 0, 1, NULL, '2020-10-26 17:00:37', '2020-12-05 22:51:18'),
(103, 'Pizza s rajčaty a cibulí', 'Připravíme pizzu dle běžného receptu.', 1, 18, NULL, 0, 0, 1, NULL, '2020-10-26 17:01:35', '2020-12-05 22:51:18'),
(104, 'Sýrový nákyp', 'Sýr nahrubo nastrouháme, smícháme se žloutky, strouhankou a mlékem. Do této směsi jemně vmícháme sníh z bílků. Pečeme v konvektomatu na 150 ˚C bez páry 25 min.', 1, 18, NULL, 0, 0, 1, NULL, '2020-10-26 17:03:00', '2020-12-05 22:51:38'),
(105, 'Zapečené špagety se žampiony a sýrem', 'Špagety uvaříme v osolené vodě, slijeme, propláchneme studenou vodou a promícháme s olejem. Rozkrájené a umyté žampiony udusíme na cibulce a osolíme. Do vymaštěného pekáče navrstvíme špagety, polijeme zředěným rajčatovým protlakem a další vrstvu vytvoříme z dušených hub. Nahoru dáme opět špagety, posypeme strouhaným sýrem, zalijeme rozpuštěným máslem a dáme zapéct.\nPříloha zeleninový salát.', 1, 18, 6, 0, 0, 1, NULL, '2020-10-26 17:05:54', '2020-12-05 22:51:18'),
(106, 'Jáhlový nákyp s jablky', 'Jáhly předem pečlivě propláchneme třikrát vroucí vodou, aby nebyly hořké. Spařené jáhly uvaříme v mléce se solí a částí cukru. Máslo utřeme s cukrem, vanilkovým cukrem, citrónovou kůrou a žloutky, přidáme prochladlou jáhlovou kaši, sníh z bílků, nakrájená jablka ochucená skořicí, rozinky a vše promícháme. Na vymaštěný pekáč dáme připravenou směs a v troubě upečeme. Nákyp podáváme ocukrovaný prosátým moučkovým cukrem.', 1, 19, NULL, 1, 0, 1, NULL, '2020-10-26 17:08:09', '2020-12-05 22:51:18'),
(107, 'Knedlíky švestkové (bramborové) s tvarohem', 'Předem uvařené brambory ve slupce vychladlé oloupeme a nastrouháme. Švestky omyjeme a osušíme. Jemně nastrouhané brambory osolíme, přidáme vejce, hrubou mouku, krupici a vypracujeme těsto. Z těsta vyválíme váleček, nakrájíme na plátky a plníme švestkami. Vytvarujeme knedlíky, které pozvolna vaříme v mírně osolené vodě 6-8 minut. Vařené knedlíky vyjmeme, necháme okapat a podáváme s nastrouhaným tvarohem, cukrem a mastíme máslem.', 1, 19, NULL, 0, 0, 1, NULL, '2020-10-26 17:10:00', '2020-12-05 17:33:44'),
(108, 'Broskvová mléčná rýžová kaše', 'Mléčnou rýži broskvovou připravíme podle návodu. Při podávání pokrm doplníme plátky kompotových broskví. Můžeme ozdobit meduňkou.', 1, 19, NULL, 0, 0, 1, NULL, '2020-10-26 17:11:03', '2020-12-05 22:51:38'),
(109, 'Lívanečky z kefíru', 'Z podmáslí, mouky, pudinku, soli, vajec, cukru a droždí vypracujeme těsto. Lívanečky smažíme v pánvičce na oleji. Hotové mažeme marmeládou a před podáváním pocukrujeme.\nRecept je z \"Týdne zdravé výživy\" od Jana Procházky.', 1, 19, NULL, 0, 0, 1, NULL, '2020-10-26 17:12:10', '2020-12-05 22:51:38'),
(110, 'Bavorské vdolečky pečené v troubě', 'Do prosáté mouky dáme cukr, sůl, vejce, žloutky, kvásek a vypracujeme těsto. Tvoříme malé bochánky, které klademe na olejem vymazaný plech, necháme chvíli nakynout a pečeme dozlatova v troubě. Po vychladnutí mažeme džemem a zdobíme šlehačkou, kterou jsme ochutili vanilkovým cukrem.', 1, 19, NULL, 0, 0, 1, NULL, '2020-10-26 17:13:09', '2020-12-05 22:51:38'),
(111, 'Jahodové knedlíky', 'Recept na jahodové knedlíky', 1, 19, NULL, 0, 0, 1, NULL, '2020-10-26 17:14:40', '2020-12-05 22:51:38'),
(112, 'Pomeranč', NULL, 7, 39, NULL, 0, 0, 1, NULL, '2020-10-26 19:14:51', '2020-12-05 22:51:18'),
(113, 'Mandarinka', NULL, 7, 39, NULL, 0, 0, 1, NULL, '2020-10-26 19:14:58', '2020-12-05 22:51:18'),
(114, 'Kiwi', NULL, 7, 39, NULL, 0, 0, 1, NULL, '2020-10-26 19:15:00', '2020-12-05 22:51:18'),
(115, 'Nektarinka', NULL, 7, 39, NULL, 0, 0, 1, NULL, '2020-10-26 19:15:07', '2020-12-05 22:51:18'),
(116, 'Rajčatový salát', '', 5, NULL, NULL, 0, 0, 1, NULL, '2020-10-26 19:16:18', '2020-12-05 22:51:18'),
(117, 'Třešňový kompot', '', 5, NULL, NULL, 0, 0, 1, NULL, '2020-10-26 19:17:03', '2020-12-05 22:51:18'),
(118, 'Mandarinkový kompot', '', 5, NULL, NULL, 0, 0, 1, NULL, '2020-10-26 19:17:10', '2020-12-05 22:51:38'),
(119, 'Broskvový kompot', '', 5, NULL, NULL, 0, 0, 1, NULL, '2020-10-26 19:17:17', '2020-12-05 22:51:18'),
(120, 'Jahodový kompot', '', 5, NULL, NULL, 0, 0, 1, NULL, '2020-10-26 19:17:27', '2020-12-05 22:51:18'),
(121, 'Mrkvový salát', '', 5, NULL, NULL, 0, 0, 1, NULL, '2020-10-26 19:17:40', '2020-12-05 22:51:18'),
(122, 'Mrkvový salát s ananasem', '', 5, NULL, NULL, 0, 0, 1, NULL, '2020-10-26 19:17:46', '2020-12-05 22:51:18'),
(123, 'Vanilkový pudink', '', 6, NULL, NULL, 0, 0, 1, NULL, '2020-10-26 19:18:48', '2020-12-05 22:51:18'),
(124, 'Čokoládový pudink', '', 6, NULL, NULL, 0, 0, 1, NULL, '2020-10-26 19:18:57', '2020-12-05 22:51:18'),
(125, 'Vanilkový pudink s jahodami', '', 6, NULL, NULL, 0, 0, 1, NULL, '2020-10-26 19:19:07', '2020-12-05 22:51:18'),
(126, 'Tiramisu', '', 6, NULL, NULL, 0, 0, 1, NULL, '2020-10-26 19:19:18', '2020-12-05 22:51:18'),
(127, 'Míša řezy', '', 6, NULL, NULL, 0, 0, 1, NULL, '2020-10-26 19:19:27', '2020-12-05 22:51:18'),
(128, 'Tomatová polévka', 'Cibuli oloupeme, nakrájíme najemno a orestujeme na oleji dosklovata. Přidáme rajčatový protlak, krátce restujeme, zalijeme vodou na polévku a provaříme. Ochutíme předem namočeným česnekem a vegetou. Do vroucí polévky pustíme rozšlehané vejce, necháme srazit a krátce prošleháme. Nakonec vložíme krájená rajčata. Při servisu zdobíme na nudličky nakrájenou bazalkou.', 2, 20, NULL, 0, 0, 1, NULL, '2020-10-26 19:24:30', '2020-12-05 22:51:18'),
(129, 'Tymiánová hrachová polévka s mrkví a minitoasty', 'Hrachovou polévku připravíme podle návodu. Při vaření vložíme do polévky snítky tymiánu (dvě třetiny), které po uvaření polévky odstraníme. Mrkev očistíme a omyjeme, najemno nastrouháme a orestujeme na rostlinném tuku. Orestovanou mrkev vmícháme do hotové polévky a polévku zjemníme smetanou. Minitoasty před expedicí dáme do gastronádoby (jednu vrstvu) a upravujeme v předehřátém konvektomatu na program pečení při 180°C asi 4 - 5 minut, tak aby měly zlatavou barvu. Opečené minitoasty vkládáme do polévky při expedici a polévku zdobíme lístečky tymiánu.', 2, 28, NULL, 0, 0, 1, NULL, '2020-10-26 19:27:03', '2020-12-05 22:51:18'),
(130, 'Polévka fazolová se zeleninou', 'Fazole zalijeme vodou, namočíme na 3 hodiny, pak slijeme a nalijeme čerstvou vodu a uvaříme doměkka. Z mouky a tuku připravíme světlou jíšku, zředíme ji vývarem z fazolí a vlijeme k fazolím. Povaříme 10 minut. Kořenovou zeleninu a cibuli nakrájíme na kostičky. Slaninu\nnakrájíme na kostičky, rozškvaříme, přidáme drobně pokrájenou cibuli a kořenovou zeleninu, osmahneme do žluta. Zaprášíme paprikou a vsypeme do polévky. Povaříme 10 minut a ochutíme utřeným česnekem, pepřem, octem a solí. Sypeme petrželkou.', 2, 24, NULL, 0, 0, 1, NULL, '2020-10-26 19:28:56', '2020-12-05 22:51:18'),
(131, 'Polévka krémová z červené řepy', 'Řepu a brambory omyjeme, oloupeme a nakrájíme na kostičky. Zalijeme vodou, osolíme a vaříme, až zelenina zcela změkne. Hladce rozmixujeme. Dolijeme mlékem a ještě krátce provaříme, velmi mírně okyselíme octem a okořeníme. Servírujeme se lžící bílého jogurtu a sypeme opranou pokrájenou pažitkou.', 2, 20, NULL, 0, 0, 1, NULL, '2020-10-26 19:30:07', '2020-12-05 22:51:18'),
(132, 'Celerová polévka se sýrem', 'Celer orestujeme a poté přidáme cibuli a restujeme. Poté polévku zalijeme vývarem a nebo vodou a vaříme do měkka. Když je celer měkký, polévku zjemníme smetanou a rozmixujeme. Nakonec přidáme nastrouhaný sýr a polévku dochutíme.', 2, 20, NULL, 1, 0, 1, NULL, '2020-10-26 19:31:41', '2020-12-05 22:51:18'),
(133, 'Fazolová krémová', 'Fazole zalijeme vodou, namočíme na 3 hodiny, pak slijeme a nalijeme čerstvou vodu a uvaříme doměkka. Cibuli nakrájíme na kostičky, na tuku osmahneme do sklovita, zaprášíme moukou a usmažíme světlou cibulovou jíšku. Zalijeme vodou, rozšleháme, osolíme a provaříme 20 minut. Přidáme fazole prolisované na masovém strojku, prolisovaný česnek, majoránku a polévku ještě 20 minut vaříme. Podáváme s nakrájenou, na sucho opraženou žemlí.', 2, 28, NULL, 0, 0, 1, NULL, '2020-10-26 19:32:39', '2020-12-05 22:51:38'),
(134, 'Krémová cizrnová polévka se sumečkem', 'Cizrnu uvaříme do měkka, přidáme na drobno nakrájeného orestovaného sumečka a společně rozmixujeme. Přidáme na jemno nastrouhanou kořenovou zeleninu, prolisovaný česnek, zeleninový bujón a připravenou zásmažku z másla a mouky. Povaříme cca 20 minut, zjemníme smetanou a dle chuti dochutíme solí a pepřem. Při podávání zdobíme sekanou petrželkou.\nTěsně před podáváním přidáme krutony.', 2, 30, NULL, 0, 0, 1, NULL, '2020-10-26 19:34:48', '2020-12-05 22:51:18'),
(135, 'Čočková', 'Čočku hodinu předem namočíme do studené vody. Vodu slijeme a čočku zalijeme čerstvou vodou. Přidáme bobkový list a vaříme do měkka. Cibuli, mrkev a celer nakrájíme na malé kostičky. Na sádle je společně opečeme do zlatova. Když je čočka měkká, třetinu odebereme a hladce rozmixujeme. Vlijeme zpět do polévky, tím se polévka zahustí. Přidáme osmaženou zeleninu, ochutíme třeným česnekem, solí a majoránkou.', 2, 28, NULL, 0, 0, 1, NULL, '2020-10-26 19:36:34', '2020-12-05 22:51:18'),
(136, 'Čočková s párkem', 'Čočku hodinu předem namočíme do studené vody. Vodu slijeme a čočku zalijeme čerstvou vodou. Přidáme bobkový list a vaříme do měkka. Cibuli, mrkev a celer nakrájíme na malé kostičky. Na sádle je společně opečeme do zlatova. Když je čočka měkká, třetinu odebereme a hladce rozmixujeme. Vlijeme zpět do polévky, tím se polévka zahustí. Přidáme osmaženou zeleninu, ochutíme třeným česnekem, solí a majoránkou.', 2, 29, NULL, 0, 0, 1, NULL, '2020-10-26 19:37:08', '2020-12-05 22:51:18'),
(137, 'Maďarská rybí polévka', 'Rybu očistíme, nakrájíme na větší kostky a v troše osolené vody podusíme. Rybu můžeme také krátce povařit v konvektomatu a po částečném vychladnutí rozebrat na kousky. Na másle orestujeme pokrájenou cibuli, zaprášíme moukou a opražíme. Zředíme vývarem z koření. Dále přidáme překrájené kysané zelí a vaříme do měkka. Do polévky dáme zavařit zeleninovou směs djuvech. Dochutíme Podravka Naturou, můžeme lehce přisladit.\r\nNa talíř položíme kousky ryby, petrželku a lžičku zakysané smetany, zalijeme horkou\r\npolévkou.', 2, 30, NULL, 0, 0, 1, NULL, '2020-10-26 19:38:50', '2020-12-05 22:51:18'),
(138, 'Kukuřičná polévka se smaženým hráškem', 'Cibuli oloupeme, nakrájíme najemno a orestujeme na oleji dosklovata. Přidáme kurkumu a necháme krátce zpěnit na cibulovém základu. Zalijeme vodou, přidáme okapanou kukuřici, na kostičky nakrájené brambory, zeleninový bujón a muškátový květ. Vaříme do změknutí brambor a potom suroviny ve vývaru rozmixujeme tyčovým mixérem, podle potřeby osolíme. Při servisu vkládáme do polévky smažený hrášek a zdobíme vhodnou bylinkou.', 2, 24, NULL, 0, 0, 1, NULL, '2020-10-26 19:42:32', '2020-12-05 22:51:18'),
(139, 'Hovězí vývar se zeleninou a vaječným svítkem', 'Vaječný svítek připravíme podle návodu, rozetřeme do vymaštěné a strouhanou houskou vysypané GN. Vložíme do vyhřátého konvektomatu a pečeme při 150 °C 15 - 20 minut. Upečený chladný svítek krájíme na požadované tvary . Hovězí vývar vsypeme do vody a přidáme na nudličky krájenou mrkev, celer a petržel. Mírně vaříme do změknutí zeleniny. Při podávání vkládáme do polévky kostičky svítku a sypeme sekanou petrželkou.', 2, 27, NULL, 0, 0, 1, NULL, '2020-10-26 19:44:35', '2020-12-05 22:51:18'),
(140, 'Slepičí polévka s rýžovými nudlemi', 'Zeleninu očistíme. Mrkev, pórek a dětskou šunku nakrájíme na tenké dlouhé nudličky. Na pánvi rozehřejeme olej a nakrájenou zeleninu orestujeme do poloměkka. Čerstvý zázvor oloupeme, nastroháme najemno a přidáme k orestované zelenině. Houževnatec jedlý namočíme a poté uvaříme doměkka, překrájíme na čtvrtky. Rýžové nudle uvaříme podle návodu. Slepičí vývar připravíme podle návodu. Do vroucího vývaru vložíme orestovanou zeleninu se zázvorem, šunku a překájené houby.\nPřed podáváním vkládáme do polévky rýžové nudle. Polévku ozdobíme snítky petrželové natě.', 2, 27, NULL, 0, 0, 1, NULL, '2020-10-26 19:45:51', '2020-12-05 22:51:38'),
(141, 'Bramboračka', 'Oloupané, na kostky nakrájené brambory a opláchnuté sušené houby zalijeme vodou, přidáme kmín, sůl a vaříme do poloměkka. Přidáme zeleninu osmaženou na oleji a dále povaříme. Před koncem varu přidáme uvařenou nebo sterilovanou sóju, česnekovou pastu nebo prolisovaný česnek, majoránku a nadrobno nakrájenou petrželovou nať. Dochutíme solí a vegetou.', 2, 22, NULL, 0, 0, 1, NULL, '2020-10-26 19:47:52', '2020-12-05 22:51:18'),
(142, 'Polévka kapustová s uzeným masem', 'Připravíme si základní vývar. Na oleji osmahneme cibuli a na kostky nakrájenou slaninku. Přidáme mraženou nařezanou kapustu, podusíme a podlijeme vývarem. Okořeníme prolisovaným česnekem a majoránkou, vložíme nakrájené uzené maso. Vše provaříme, osolíme a dochutíme.', 2, 21, NULL, 0, 0, 1, NULL, '2020-10-26 19:49:19', '2020-12-05 22:51:18'),
(143, 'Polévka z kostí s těstovinou', 'Do osoleného vývaru z kostí dáme vařit zeleninu. Po změknutí zeleniny zavaříme těstoviny (rýži). Dochutíme sekanou zelenou natí.', 2, 27, NULL, 0, 0, 1, NULL, '2020-10-26 19:51:08', '2020-12-05 22:51:18'),
(144, 'Kyselka', 'Kyselé mléko a mouku uvaříme. Nakrájené a uvařené brambory přidáme do polévky, osolíme, zjemníme tukem a přidáme nasekaný kopr.', 2, 32, NULL, 1, 0, 1, NULL, '2020-10-26 19:52:41', '2020-12-05 22:51:18'),
(145, 'Polévka kastelánská', 'Z fazolek odstraníme tuhá vlákna, omyjeme a nakrájíme na proužky. Zalijeme vývarem, přidáme rajčatový protlak, sůl a vaříme do měkka. Když jsou fazolky měkké, zahustíme polévku zálivkou z nasucho opražené mouky a vody. Za stálého míchání mouku dobře provaříme. Hotovou polévku zjemníme tukem a dochutíme zelenou natí.', 2, 20, NULL, 0, 0, 1, NULL, '2020-10-26 19:54:56', '2020-12-05 22:51:18'),
(146, 'Hovězí vývar s rýží', 'Do osoleného vývaru dáme vařit zeleninu. K téměř měkké zelenině přidáme těstoviny nebo jinou vložku a přidáme zelenou nať.', 2, 26, NULL, 0, 0, 1, NULL, '2020-10-26 19:56:48', '2020-12-05 22:51:18'),
(147, 'Polévka rajská s rýží', 'Očištěnou, omytou zeleninu jemně nastrouháme a zalijeme osolenou vodou. Přidáme opranou rýži a rajský protlak a vše vaříme do měkka. Do hotové polévky přidáme tuk a zelenou nať.', 2, 26, NULL, 0, 0, 1, NULL, '2020-10-26 19:58:09', '2020-12-05 22:51:38'),
(148, 'Jihočeská zelnice', 'Brambory a nakrájené hlávkové zelí vaříme v osolené vodě s kmínem asi 10, min. . Zahustíme moukou rozmíchanou ve vodě azjemnímesmetanou. Provaříme.', 2, 22, NULL, 0, 0, 1, NULL, '2020-10-26 19:59:48', '2020-12-05 22:51:38'),
(149, 'Mlíkovka s těstovinou', '0,50 l mléko polotučné\n1 špetka mořská sůl není nutná\n1,50 lžička krupice není nutná\n1 hrst polévkové nudle nebo drobení', 2, 31, NULL, 1, 0, 1, NULL, '2020-10-26 20:01:27', '2020-12-05 22:51:18'),
(150, 'Polévka špenátová s bramborem a zeleninou', 'Mraženou nebo kořenovou zeleninu podusíme. Na oleji zpěníme cibulku, zaprášíme moukou a připravíme jíšku, kterou zalijeme vodou. Přidáme brambory a vaříme asi 10 minut. Pak přidáme zeleninu, špenát azalijeme mlékem s rozšlehanými vejci a opět provaříme. Dochutíme česnekemamůžeme zjemnit máslem.', 2, 22, NULL, 0, 0, 1, NULL, '2020-10-26 20:04:00', '2020-12-05 22:51:18'),
(151, 'Česnečka s krutóny a bramborem', 'Tři větší brambory, pokrájené, 7–8 stroužků česneku, na plátky a lžičku kmínu zalijte 750 ml vody a vařte, dokud brambory nezměknou; pak přisypte 2 lžičky sušené majoránky a vylovte brambory. Jednu cibuli, pokrájenou, osmahněte na sádle nebo oleji, přidejte brambory, sůl a pepř a trochu rozmačkejte. Buď je vraťte do polévky, nebo je dávejte na dno talířů a zalévejte česnečkou.', 2, 22, NULL, 0, 0, 1, NULL, '2020-10-26 20:07:58', '2020-12-05 22:52:41');
INSERT INTO `dish` (`id`, `name`, `recipe`, `type_id`, `category_id`, `side_id`, `likes`, `frequency`, `enabled`, `last_used_at`, `created_at`, `updated_at`) VALUES
(152, 'Zelňačka s bramborem a vepřevým masem', 'Na sádle osmažíme cibulku. Vložíme drobně nakrájenou krkovici a orestujeme. ... Vložíme pokrájené zelí a brambory, masox a zeleninový vývar, bobkový list, bylinky s majoránkou a mírným varem vaříme za občasného zamíchání asi 3/4 hodiny. Nakonec vlijeme smetanu a dosolíme. Polévka je hodně vydatná.', 2, 23, NULL, 0, 0, 1, NULL, '2020-10-26 20:09:33', '2020-12-05 22:51:18'),
(153, 'Hovězí s hráškem', 'Umyté hovězí maso a kosti dáme do studené vody vařit. Po hodinovém varu přidáme očištěnou, pokrájenou zeleninu, menší cibuli, sůl, černé koření. Až je maso měkké, polévku scedíme a vložíme do ní uvařené maso pokrájené na kostky, konzervovaný hrášek a samostatně uvařenou a scezenou rýži.', 2, 25, NULL, 0, 0, 1, NULL, '2020-10-26 20:11:05', '2020-12-05 22:51:18'),
(154, 'Špenátová', 'Cibuli osmažíme na sádle dozlatova, zalijeme převařenou vodou. Počkáme než se začne vařit a přidáme špenát, masox, sůl, pepř a trochu maggi, povaříme.\nPřidáme slaninu nakrájenou na kostičky, česnek, vmícháme vajíčka a nakonec smetanu. Krátce ještě povaříme a můžete podávat.', 2, 20, NULL, 0, 0, 1, NULL, '2020-10-27 07:48:28', '2020-12-05 22:51:18'),
(155, 'Mrkvový krém', 'Příprava krémových polévek je velmi snadná a rychlá. Stačí nakrájet suroviny, uvařit je, dochutit, a pak už potřebujete jen výkonný mixér, který vše promění v lahodnou polévku.\n\nJá jsem pracovala s robotem , který má mixér s nádobou s velkým objemem 1,25 litru s odnímatelným nožem, který se díky tomu snadno čistí.\n\nOcenila jsem i 7 rychlostí a pulzní režim, díky kterému máte nad mixováním maximální kontrolu.', 2, 20, NULL, 0, 0, 1, NULL, '2020-10-27 07:50:01', '2020-12-05 22:51:18'),
(156, 'Polévka barevná s pohankou', 'Na polovině oleje osmahneme pohanku, zalijeme vodou a přivedeme k varu. Přidáme na zbylém oleji osmahnutou, na jemno nastrouhanou mrkev, pórek nakrájený na nudličky, osolíme, zalijeme vodou a vaříme do měkka. Sypeme na drobno nakrájenou petrželovou natí.', 2, 20, NULL, 0, 0, 1, NULL, '2020-10-27 07:52:00', '2020-12-05 22:51:18'),
(157, 'Kuřecí vývar', 'Do hrnce nalijeme 2 l studené vody. Kuřecí skelet a křídla omyjeme, vložíme do hrnce. Koření nasypeme do sítka na koření a spolu s bobkovými listy přidáme ke kuřeti.\n\nVodu přivedeme k varu, stáhneme teplotu a necháme pomalu asi 2 hodiny probublávat. Pokud se bude voda příliš vyvařovat, dolijeme vařící. Po 2 hodinách očistíme zeleninu, nakrájíme ji a povaříme ve vývaru do změknutí.\n\nČásti kuřete vyjmeme, necháme chvíli vychladnout a maso obereme. Maso vložíme zpět do kuřecího vývaru. Dochutíme solí, případně i pepřem.\n\nKuřecí vývar můžeme na talíři dozdobit čerstvou pažitkou či petrželkou.', 2, 21, NULL, 0, 0, 1, NULL, '2020-10-27 07:55:13', '2020-12-05 22:51:38'),
(158, 'Domácí hovězí polévka', '1.Hovězí maso dejte do hrnce, zalijte dvěma litry studené vody, přidejte oloupanou, přepůlenou cibuli, svazek bylinek, 1 oloupanou a přepůlenou mrkev, celer, pepř i nové koření a přiveďte k varu. Pomalu vařte dvě hodiny, aby maso úplně změklo.\n2.Maso vyjměte, nechte mírně zchladnout a pak jej oberte a nakrájejte. Cibuli, bylinky i mrkev vyhoďte. Celer pokrájejte na malé kostičky. Vývar přelijte přes cedník vyložený namočenou a vyždímanou utěrkou – bude zcela čirý. Druhou mrkev nastrouhejte nahrubo a osmahněte ji na másle do zkřehnutí. Pak ji spolu s masem a ce­lerem přidejte do polévky. Ozdobte petrželkou.', 2, 21, NULL, 0, 0, 1, NULL, '2020-10-27 08:04:36', '2020-12-05 22:51:18'),
(159, 'Gulášová polévka', 'Nadrobno nakrájenou cibulku zpěníme na oleji a osmahneme. Poprášíme mletou paprikou, pepřem, osolíme, podlijeme trochou vody a dusíme téměř doměkka. Pak přidáme na kostky nakrájené brambory, prolisovaný česnek, dolijeme zbývající vroucí vodu a krátce povaříme. Polévku zahustíme moukou rozmíchanou v malém množství vody a dobře provaříme.', 2, 22, NULL, 0, 0, 1, NULL, '2020-10-27 08:06:52', '2020-12-05 22:51:38'),
(160, 'Savojská bramborová polévka', 'Cibuli oloupeme a nakrájíme najemno. Brambory očistíme, oloupeme a nakrájíme na kostky. Mrkev očistíme a jemně nastrouháme. Pažitku usekáme.\n\nNa másle osmažíme cibulku dorůžova. Potom přidáme nastrouhanou mrkev a za stálého míchání podusíme. Zaprášíme moukou, ještě asi 2 minuty restujeme. Potom vlijeme vývar, vložíme brambory, osolíme a okořeníme strouhaným muškátovým oříškem. Vaříme na mírném ohni, dokud brambory nezměknou.\n\nSavojskou bramborovou polévku těsně před podáváním posypeme čerstvě usekanou pažitkou.', 2, 22, NULL, 0, 0, 1, NULL, '2020-10-27 08:08:34', '2020-12-05 22:51:18'),
(161, 'Pórková s bramborem', 'Cibuli očistíme a nakrájíme na kostičky, česnek rozdrtíme nebo nasekáme nožem. Očištěný, omytý pórek zbavíme konce a nakrájíme na nudličky. Nejlepší je středová světlá část pórku až k cibulce. Brambory oloupeme a nakrájíme na kostky cca 1 až 2cm dle vlastní libosti.\n\nBrambory uvaříme v osolené vodě do měkka.\n\nCibuli orestujeme do sklovata v hrnci s rozpáleným olejem, poté přidáme česnek a pórek.\n\nVše restujeme ještě asi 2 minuty a zalijeme vývarem.\n\nPřidáme koření (bobkový list, nové koření), doporučujeme používat sítko na koření.\n\nMuškátový ořech nastouháme přímo do polévky.\n\nPřilijeme smetanu a pozvolna vaříme cca 15 minut.\n\nUvařené brambory scedíme, třetinu rozmixujeme a vložíme do polévky spolu s ostatními uvařenými bramborami.\n\nDochutíme solí a pepřem, necháme zavařit.', 2, 22, NULL, 0, 0, 1, NULL, '2020-10-27 08:10:18', '2020-12-05 22:51:18'),
(162, 'Polévka ze sladkých brambor a zázvoru', 'Batáty alias sladké brambory - cenný zdroj vitaminů, minerálů a vlákniny, který má v kuchyni široké využití. Můžete je uvařit jako skvělou přílohu, upéct se zeleninou a masem jako hlavní chod nebo použít k přípravě dezertu. V tomto díle Kulinářské akademie Lidlu vám Roman Paulus ukáže, jak z této skvělé suroviny, pocházející ze Střední a Jižní Ameriky, uvařit polévku. Podívejte se na náš video recept a dozvíte se, jak připravit krémovou polévku ze sladkých brambor, ochucenou jablky a voňavými bylinkami.', 2, 22, NULL, 0, 0, 1, NULL, '2020-10-27 08:13:38', '2020-12-05 22:51:18'),
(163, 'Polévka z brambor, cibule a mrkve', 'Postup\nKrok 1: Cibuli nakrájejte nahrubo a osmahněte ji na kousku másla dozlatova.\n\nKrok 2: Pak k ní přidejte nahrubo nastrouhanou mrkev a chvilku ji poduste. Přidejte na kousky pokrájené oškrábané brambory, zalijte vodou nebo vývarem, osolte a vařte doměkka. Na závěr polévku můžete ochutit trochou zázvoru.', 2, 22, NULL, 0, 0, 1, NULL, '2020-10-27 08:15:17', '2020-12-05 22:51:18'),
(164, 'Zeleninová polévka s bramborami', 'Na oleji zpěníme nakrájenou cibulku a poprášíme ji červenou paprikou. Zalijeme vařící vodou z konvice, přidáme mrkev, petržel a brambory.\nVaříme dokud zelenina nezměkne. Pro zahuštění můžeme přidat ovesné vločky. Ozdobíme petrželkou.', 2, 22, NULL, 0, 0, 1, NULL, '2020-10-27 08:16:10', '2020-12-05 22:51:18'),
(165, 'Gulášová polévka s masem', 'Gulášová polévka je tradiční druh uherské polévky, která obsahuje rozemleté maso, cibuli a vařené brambory. Typická je její červená barva, výrazná chuť a vysoká hustota. Připravuje se buď přípravou všech ingrediencí a nebo se dá zakoupit v podobě předpřipraveného instantního obsahu, který se nasype do vroucí vody.Gulášová polévka je tradiční druh uherské polévky, která obsahuje rozemleté maso, cibuli a vařené brambory. Typická je její červená barva, výrazná chuť a vysoká hustota. Připravuje se buď přípravou všech ingrediencí a nebo se dá zakoupit v podobě předpřipraveného instantního obsahu, který se nasype do vroucí vody.', 2, 23, NULL, 0, 0, 1, NULL, '2020-10-27 08:17:31', '2020-12-05 22:51:18'),
(166, 'Kuřecí polévka s bramborami', 'Kuře vložit do hrnce a zalijeme vodou, přidáme citr. šťávu, sojovou omáčku, kostku bujónu, pepř a bobkový list (dala jsem to do vajíčka na čaj).\n\nVaříme asi 45 minut. Vyjmeme maso a přecedíme.\n\nVývar přivedeme k varu, dáme zeleninu, brambory a vaříme do měkka.\n\nMaso obereme a dáme do polévky. Dochutíme solí, pepřem, můžem přidat petrželku nebo jarní cibulku.', 2, 23, NULL, 0, 0, 1, NULL, '2020-10-27 08:20:37', '2020-12-05 22:51:18'),
(167, 'Zeleninová polévka s luštěninami', '1. Sušené bílé fazole namočíme den předem do studené vody. Druhý den vodu z fazolí slijeme. Zalijeme je znovu studenou vodou a uvaříme doměkka. Červené sterilované fazole dáme do síta a propláchneme studenou vodou.\n\n\n2. Mrkev a petržel oškrábeme a nakrájíme na kusy. V hrnci rozpustíme polovinu másla a krátce na něm podusíme mrkev s petrželí. Pak posypeme karí, asi půl minuty orestujeme a zalijeme zeleninovým vývarem. Přivedeme k varu a vaříme, až bude zelenina měkká. Pak přidáme polovinu vařených bílých fazolí a rozmixujeme ponorným mixérem dohladka.\n\n\n3. Na zbylém másle orestujeme na kolečka nakrájený pórek a lístky šalvěje. Přidáme do polévky společně se zbylými bílými fazolemi i červenými fazolemi. Polévku dochutíme solí a pepřem.', 2, 24, NULL, 0, 0, 1, NULL, '2020-10-27 08:23:59', '2020-12-05 22:51:38'),
(168, 'Kuřecí polévka s mrkví a hráškem', 'Do tlakáče dáme kuřecí polévkovou směs, mrkev, petržel, cibuli, libeček a pár kuliček pepře a nového koření. Vaříme asi 1,5 hod. a pak polévku slejeme přes cedník. Mrkev pokrájíme na kostičky a dáme jí do vývaru. Ze směsi obereme maso, které vložíme do vývaru a přivedeme k varu. Do vroucí polévky zavaříme nudle a přidáme hrášek. Nakonec polévku dochutíme sekanou petrželkou a solí.', 2, 25, NULL, 0, 0, 1, NULL, '2020-10-27 08:34:17', '2020-12-05 22:51:18'),
(169, 'Kuřecí polévka s kukuřicí', 'Do hrnce nalijeme vodu, přidáme na velké kusy pokrájenou očištěnou kořenovou zeleninu (mrkev, celer a petržel), cibuli i se slupkou překrojenou napůl, divoké koření (bobkový list, nové koření a celý černý pepř), kuřecí stehna, osolíme a vaříme asi 2 hodiny. Přibližně v půlce vaření přidáme i oloupané stejně velké brambory tak, aby se uvařily, ale nerozvařily.\n\nVývar scedíme pomocí kovového sítka, brambory a kuře si opatrně dáme zvlášť do talíře.\n\nSlaninu nakrájíme na nudličky, pomalu vypečeme v hrnci s kouskem másla a dáme stranou na papírovou utěrku. Vypečený tuk necháme v hrnci, přidáme najemno nakrájenou cibuli, česnek na plátky, řapíkatý celer na kostičky, zaprášíme moukou, zarestujeme a poté podlijeme 1,2 l vývaru. Vývar přidáváme po malých dávkách a hned metličkou rozmícháváme, abychom neměli hrudky. Necháme provařit a zjemníme smetanou, přidáme pálivou papriku, osolíme a opepříme.\n\nDo polévky přidáme kukuřici, nakrájené vařené brambory, obrané maso ze stehen a nasekanou listovou petržel.\n\nPodáváme v hlubokém talíři posypané lahůdkovou cibulkou, vypraženou slaninou a podle chuti i špetkou pálivé papriky.', 2, 25, NULL, 0, 0, 1, NULL, '2020-10-27 08:35:24', '2020-12-05 22:51:18'),
(170, 'Hrachová polévka s uzeným masem', 'Hrách si namočíme do studené vody, nejlépe přes noc nebo aspoň na pár hodin. Ale dá se uvařit i bez namočení, jen to trochu déle trvá.\nUvaříme ho v té samé vodě do měkka. Osolíme na závěr.\nUzenou krkovičku si uvaříme v osolené vodě s divokým kořením také do měkka.\nV kastrolu opečeme na sádle cibuli nakrájenou na kostky do zlatova.\nPřisypeme hladkou mouku a připravíme jíšku. Tu rozšleháme v trošce studené vody a přilijeme přecezený vývar, ve kterém jsme uvařili uzené.\nRozmícháme, přivedeme k varu a necháme 5 - 10 minut provařit. Přidáme drcený pepř, prolisovaný česnek a uvařený, částečně zcezený hrách.\nJeště pár minut společně povaříme, dle chuti přisolíme či připepříme a nasypeme špetku majoránky.\nUzené maso nakrájíme na kostky a dáme do polévky.\nA je to, rozdělíme na talíře, a necháme si chutnat.', 2, 29, NULL, 0, 0, 1, NULL, '2020-10-27 08:42:07', '2020-12-05 22:51:18'),
(171, 'Fazolová polévka s klobásou', '1 Na oleji ve větším hrnci opečte klobásu nakrájenou na kolečka dozlatova a dokřupava. Opečenou klobásu vyjměte a odložte stranou.\n\n2 Do uvolněného hrnce na tuk vypečený z klobás vsypte cibuli a mrkev, krátce je osmahněte, přidejte česnek, opět mírně osmahněte, přidejte protlak, opečte ho a vše zalijte vývarem. Osolte, opepřete, přiveďte k varu a na mírném ohni vařte asi 15 minut, potom přidejte kapii a obojí fazole a vařte, dokud nebude paprika měkká a fazole dokonale prohřáté.\n\n3 Do hotové polévky vsypte odloženou opečenou klobásu, nechte ji ještě chvilku prohřát a podávejte se lžící zakysané smetany a ozdobené čerstvou petrželkou. ', 2, 29, NULL, 0, 0, 1, NULL, '2020-10-27 08:43:17', '2020-12-05 22:51:18'),
(172, 'Tradiční rybí polévka', '1. vývar. 500 g rybích kostí 200 g kořenové zeleniny (mrkev, celer, petržel) 50 g cibule. 5 kuliček celého černého pepře. 5 kuliček nového koření ...\npolévka. 300 g kořenové zeleniny. 100 g cibule. 50 g másla. 50 g hladké mouky. 1,5 l rybího vývaru. ...\n2. krutónky. 150 g veky. 1 stroužek česneku. 3 jarní cibulky. 30 g másla. sůl.', 2, 30, NULL, 0, 0, 1, NULL, '2020-10-27 08:45:52', '2020-12-05 22:51:18'),
(173, 'Mléčná polévka se strouháním', 'Do hrnce dáme svařit mléko s vodou, solí a cukrem.\n\nPřidáme kousek másla.\n\nDo vroucí polévky zavaříme strouhání, poté co vyplave nahoru, je polévka hotová.\n\nStrouhání: v mističce smícháme mouku s vejcem a trošičkou vody, utvoříme těsto tuhé konzistence.\n\nNa hrubém struhadle nastrouháme a necháme chvilku proschnout.Komu se nechce dělat strouhání, může použít uvařené vlasové nudličky.', 2, 31, NULL, 0, 0, 1, NULL, '2020-10-27 08:49:30', '2020-12-05 22:51:18'),
(174, 'Jihočeská kulajda', 'V kastrolu rozpustíme vrchovatou lžíci másla a přidáme ještě trochu oleje, tak abychom měli celé dno pokryté tukem. Máslo zaprášíme 2–3 lžícemi hladké mouky podle toho, jak moc chceme mít polévku hustou, jíšku stále mícháme a restujeme, než začne vonět a směs lehce zhnědne. Do rozpálené jíšky  přilijeme vařící vývar (vodu) a metličkou vše rozšleháme.\nDo směsi přidáme nakrájené brambory, očištěné houby, kmín, bobkový list a nové koření. Houby předem v rozpálené pánvi orestujte, a až potom je přidejte do polévky. Vaříme, dokud brambory nejsou měkké, to trvá asi 10–15 minut.\nVyndáme bobkový list a nové koření. Do polévky přilijeme opravdu tučnou smetanu, aby se polévka nesrazila. Osolíme, opepříme, přidáme kopr, dochutíme cukrem a octem. Poměr cukru a octa je skutečná alchymie, já dávám asi 2 lžíce octa a 2 lžíce cukru, záleží ale opravdu a pouze na vaší chuti, radši ze začátku ochuťte méně a případně ještě poupravte.\nJá vejce vařím natvrdo ve skořápce vedle, tradičně je můžete připravit i jako ztracená.', 2, 32, NULL, 0, 0, 1, NULL, '2020-10-27 08:51:20', '2020-12-05 22:51:18'),
(175, 'Jahodové mléko', '', 3, 36, NULL, 0, 0, 1, NULL, '2020-10-27 08:53:12', '2020-12-05 22:51:18'),
(176, 'Vanilkové mléko', '', 3, 36, NULL, 0, 0, 1, NULL, '2020-10-27 08:53:21', '2020-12-05 22:51:18'),
(177, 'Kakao', '', 3, 36, NULL, 0, 0, 1, NULL, '2020-10-27 08:53:28', '2020-12-05 22:51:18'),
(178, 'Šťáva', '', 3, 34, NULL, 0, 0, 1, NULL, '2020-10-27 08:53:54', '2020-12-05 22:51:18'),
(179, 'Černý čaj', '', 3, 33, NULL, 0, 0, 1, NULL, '2020-10-27 08:54:06', '2020-12-05 22:51:18'),
(180, 'Pomelo', NULL, 7, 39, NULL, 0, 0, 1, NULL, '2020-11-05 21:20:39', '2020-12-05 22:51:18'),
(181, 'Milánské špagety', 'Drobně pokrájenou cibuli necháme na oleji osmažit, až zesklovatí. Přidáme mleté kuřecí (případně vepřové) maso, osolíme, opepříme a restujeme. Maso od sebe rozdělíme vařečkou. Poté přidáme jemně nasekaný nebo utřený česnek, rajčatovou pastu a ještě krátce restujeme. Pak drobně pokrájená rajčata i se šťávou a oregano dle chuti. Krátce vše podusíme, jen do změknutí rajčat, případně ještě dochutíme solí a pepřem.\n\nŠpagety uvaříme dle návodu v osolené vodě, scedíme a dáme na talíře. Na špagety rozdělíme milánskou směs, posypeme sýrem dle chuti a podáváme.\n\nPokud máme rajčatovou pastu a rajčata kyselejší, můžeme přidat maličko cukru.', 1, 4, NULL, 0, 0, 1, NULL, '2020-11-05 22:09:39', '2020-12-05 22:51:38'),
(185, 'Chléb s lučinovou pomazánkou a s rajčetem', '', 7, 38, NULL, 0, 0, 1, NULL, '2020-12-04 12:25:33', '2020-12-05 22:51:18'),
(186, 'Ovoce', '', 7, 39, NULL, 0, 0, 1, NULL, '2020-12-04 12:27:00', '2020-12-05 22:51:18'),
(187, 'Zelenina', '', 7, 40, NULL, 0, 0, 1, NULL, '2020-12-04 12:27:08', '2020-12-05 22:51:18'),
(188, 'Rohlík s medovým tvarohem', '', 7, 38, NULL, 0, 0, 1, NULL, '2020-12-04 12:27:23', '2020-12-05 22:51:18'),
(189, 'Chléb s rybí tomatovou pomazánkou', '', 7, 38, NULL, 0, 0, 1, NULL, '2020-12-04 12:29:29', '2020-12-05 22:51:18'),
(190, 'Chléb se strouhaným sýrem a mrkvičkou', '', 7, 38, NULL, 0, 0, 1, NULL, '2020-12-04 12:29:48', '2020-12-05 22:51:18'),
(191, 'Chléb s pórkovou pomazánkou', '', 7, 38, NULL, 0, 0, 1, NULL, '2020-12-04 12:31:00', '2020-12-05 22:51:18'),
(192, 'Rohlík s čočkovou pomazánkou', '', 7, 38, NULL, 0, 0, 1, NULL, '2020-12-04 12:31:48', '2020-12-05 22:51:18'),
(193, 'Houska s pomazánkou z kořenové zeleniny', '', 7, 38, NULL, 0, 0, 1, NULL, '2020-12-04 12:32:08', '2020-12-05 22:51:18'),
(194, 'Domácí mramorový moučník s tvarohem', '', 6, NULL, NULL, 0, 0, 1, NULL, '2020-12-04 12:33:12', '2020-12-05 17:33:44'),
(195, 'Jáhlová kaše', '', 6, NULL, NULL, 0, 0, 1, NULL, '2020-12-04 12:33:29', '2020-12-05 22:43:41'),
(196, 'Vanilkový termix s piškotky', '', 6, NULL, NULL, 0, 0, 1, NULL, '2020-12-04 12:33:52', '2020-12-05 17:33:44');

-- --------------------------------------------------------

--
-- Table structure for table `menu_day`
--

CREATE TABLE `menu_day` (
  `id` int(10) UNSIGNED NOT NULL,
  `date` date NOT NULL,
  `number` tinyint(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `menu_week`
--

CREATE TABLE `menu_week` (
  `id` int(10) UNSIGNED NOT NULL,
  `number` tinyint(3) UNSIGNED NOT NULL,
  `year` int(10) UNSIGNED NOT NULL,
  `build_set` int(11) NOT NULL,
  `background` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `recommendations_basket`
--

CREATE TABLE `recommendations_basket` (
  `id` smallint(5) UNSIGNED NOT NULL,
  `name` varchar(255) NOT NULL,
  `translation` varchar(255) NOT NULL,
  `type_id` smallint(5) UNSIGNED NOT NULL,
  `age_group` enum('3-6 let','7-10 let','11-14 let','15-18 let') NOT NULL,
  `value` int(10) UNSIGNED NOT NULL,
  `restriction` enum('min','max') DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `recommendations_basket`
--

INSERT INTO `recommendations_basket` (`id`, `name`, `translation`, `type_id`, `age_group`, `value`, `restriction`) VALUES
(1, 'Maso', 'meat', 1, '7-10 let', 900, 'min'),
(2, 'Ryby', 'fish', 1, '7-10 let', 210, 'min'),
(3, 'Luštěniny', 'legumes', 1, '7-10 let', 90, 'min'),
(4, 'Obiloviny', 'cereals', 1, '7-10 let', 420, 'min'),
(5, 'Brambory', 'potatoes', 1, '7-10 let', 1900, 'min'),
(6, 'Zelenina', 'vegetables', 1, '7-10 let', 1200, 'min'),
(7, 'Tuk', 'fat', 1, '7-10 let', 160, 'max'),
(8, 'Sůl', 'salt', 1, '7-10 let', 27, 'max'),
(9, 'Maso', 'meat', 2, '7-10 let', 80, 'min'),
(10, 'Ryby', 'fish', 2, '7-10 let', 35, 'min'),
(11, 'Luštěniny', 'legumes', 2, '7-10 let', 70, 'min'),
(12, 'Obiloviny', 'cereals', 2, '7-10 let', 30, 'min'),
(13, 'Brambory', 'potatoes', 2, '7-10 let', 200, 'min'),
(14, 'Zelenina', 'vegetables', 2, '7-10 let', 540, 'min'),
(15, 'Tuk', 'fat', 2, '7-10 let', 80, 'max'),
(16, 'Sůl', 'salt', 2, '7-10 let', 20, 'max'),
(17, 'Maso', 'meat', 1, '3-6 let', 780, 'min'),
(18, 'Ryby', 'fish', 1, '3-6 let', 150, 'min'),
(19, 'Luštěniny', 'legumes', 1, '3-6 let', 80, 'min'),
(20, 'Obiloviny', 'cereals', 1, '3-6 let', 315, 'min'),
(21, 'Brambory', 'potatoes', 1, '3-6 let', 1600, 'min'),
(22, 'Zelenina', 'vegetables', 1, '3-6 let', 840, 'min'),
(23, 'Tuk', 'fat', 1, '3-6 let', 160, 'max'),
(24, 'Sůl', 'salt', 1, '3-6 let', 18, 'max'),
(25, 'Maso', 'meat', 2, '3-6 let', 80, 'min'),
(26, 'Ryby', 'fish', 2, '3-6 let', 25, 'min'),
(27, 'Luštěniny', 'legumes', 2, '3-6 let', 50, 'min'),
(28, 'Obiloviny', 'cereals', 2, '3-6 let', 30, 'min'),
(29, 'Brambory', 'potatoes', 2, '3-6 let', 160, 'min'),
(30, 'Zelenina', 'vegetables', 2, '3-6 let', 440, 'min'),
(31, 'Tuk', 'fat', 2, '3-6 let', 80, 'max'),
(32, 'Sůl', 'salt', 2, '3-6 let', 20, 'max'),
(33, 'Maso', 'meat', 1, '11-14 let', 960, 'min'),
(34, 'Ryby', 'fish', 1, '11-14 let', 240, 'min'),
(35, 'Luštěniny', 'legumes', 1, '11-14 let', 100, 'min'),
(36, 'Obiloviny', 'cereals', 1, '11-14 let', 490, 'min'),
(37, 'Brambory', 'potatoes', 1, '11-14 let', 2300, 'min'),
(38, 'Zelenina', 'vegetables', 1, '11-14 let', 1280, 'min'),
(39, 'Tuk', 'fat', 1, '11-14 let', 200, 'max'),
(40, 'Sůl', 'salt', 1, '11-14 let', 27, 'max'),
(41, 'Maso', 'meat', 2, '11-14 let', 100, 'min'),
(42, 'Ryby', 'fish', 2, '11-14 let', 40, 'min'),
(43, 'Luštěniny', 'legumes', 2, '11-14 let', 90, 'min'),
(44, 'Obiloviny', 'cereals', 2, '11-14 let', 30, 'min'),
(45, 'Brambory', 'potatoes', 2, '11-14 let', 280, 'min'),
(46, 'Zelenina', 'vegetables', 2, '11-14 let', 650, 'min'),
(47, 'Tuk', 'fat', 2, '11-14 let', 100, 'max'),
(48, 'Sůl', 'salt', 2, '11-14 let', 30, 'max'),
(49, 'Ryby', 'fish', 1, '15-18 let', 300, 'min'),
(50, 'Luštěniny', 'legumes', 1, '15-18 let', 100, 'min'),
(51, 'Obiloviny', 'cereals', 1, '15-18 let', 560, 'min'),
(52, 'Brambory', 'potatoes', 1, '15-18 let', 2620, 'min'),
(53, 'Zelenina', 'vegetables', 1, '15-18 let', 1560, 'min'),
(54, 'Tuk', 'fat', 1, '15-18 let', 240, 'max'),
(55, 'Sůl', 'salt', 1, '15-18 let', 36, 'max'),
(56, 'Maso', 'meat', 2, '15-18 let', 100, 'min'),
(57, 'Ryby', 'fish', 2, '15-18 let', 50, 'min'),
(58, 'Luštěniny', 'legumes', 2, '15-18 let', 90, 'min'),
(59, 'Obiloviny', 'cereals', 2, '15-18 let', 40, 'min'),
(60, 'Brambory', 'potatoes', 2, '15-18 let', 320, 'min'),
(61, 'Zelenina', 'vegetables', 2, '15-18 let', 760, 'min'),
(62, 'Tuk', 'fat', 2, '15-18 let', 100, 'max'),
(63, 'Sůl', 'salt', 2, '15-18 let', 30, 'max'),
(64, 'Maso', 'meat', 1, '15-18 let', 1200, 'min');

-- --------------------------------------------------------

--
-- Table structure for table `recommendations_nutri`
--

CREATE TABLE `recommendations_nutri` (
  `id` smallint(5) UNSIGNED NOT NULL,
  `type_id` smallint(5) UNSIGNED NOT NULL,
  `category_id` smallint(5) UNSIGNED NOT NULL,
  `value` int(10) UNSIGNED NOT NULL,
  `restriction` enum('min','max') DEFAULT NULL,
  `message` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `recommendations_nutri`
--

INSERT INTO `recommendations_nutri` (`id`, `type_id`, `category_id`, `value`, `restriction`, `message`) VALUES
(1, 1, 1, 0, NULL, ''),
(2, 1, 2, 4, NULL, ''),
(3, 1, 3, 1, NULL, ''),
(4, 1, 4, 1, NULL, ''),
(5, 1, 5, 0, NULL, ''),
(6, 1, 6, 1, NULL, ''),
(7, 1, 7, 1, NULL, ''),
(8, 1, 8, 1, NULL, ''),
(9, 1, 9, 1, NULL, ''),
(10, 1, 10, 1, NULL, ''),
(11, 1, 11, 1, NULL, ''),
(12, 1, 12, 1, NULL, ''),
(13, 1, 13, 2, NULL, ''),
(14, 1, 14, 1, NULL, ''),
(15, 1, 15, 0, NULL, ''),
(16, 1, 16, 0, NULL, ''),
(17, 1, 17, 1, NULL, ''),
(18, 1, 18, 1, NULL, ''),
(19, 1, 19, 2, NULL, ''),
(20, 2, 20, 3, NULL, ''),
(21, 2, 21, 1, NULL, ''),
(22, 2, 22, 6, NULL, ''),
(23, 2, 23, 1, NULL, ''),
(24, 2, 24, 1, NULL, ''),
(25, 2, 25, 1, NULL, ''),
(26, 2, 26, 1, NULL, ''),
(27, 2, 27, 1, NULL, ''),
(28, 2, 28, 1, NULL, ''),
(29, 2, 29, 1, NULL, ''),
(30, 2, 30, 1, NULL, ''),
(31, 2, 31, 1, NULL, ''),
(32, 2, 32, 1, NULL, '');

-- --------------------------------------------------------

--
-- Table structure for table `relation_dish_allergen`
--

CREATE TABLE `relation_dish_allergen` (
  `id` int(11) UNSIGNED NOT NULL,
  `dish_id` int(11) UNSIGNED NOT NULL,
  `allergen_id` int(11) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `relation_dish_allergen`
--

INSERT INTO `relation_dish_allergen` (`id`, `dish_id`, `allergen_id`) VALUES
(16, 9, 7),
(91, 44, 1),
(92, 44, 3),
(93, 44, 7),
(106, 43, 1),
(130, 46, 1),
(131, 46, 6),
(132, 46, 9),
(136, 41, 1),
(137, 41, 3),
(148, 11, 3),
(149, 11, 7),
(150, 3, 1),
(154, 1, 7),
(155, 2, 1),
(156, 2, 4),
(157, 12, 3),
(158, 12, 7),
(159, 16, 1),
(160, 17, 1),
(161, 17, 7),
(162, 27, 1),
(163, 39, 1),
(164, 39, 7),
(165, 45, 1),
(166, 45, 7),
(167, 45, 9),
(168, 47, 1),
(169, 47, 7),
(182, 48, 1),
(183, 48, 3),
(184, 48, 6),
(185, 48, 7),
(186, 48, 9),
(187, 48, 12),
(188, 49, 6),
(189, 49, 7),
(190, 50, 1),
(191, 50, 9),
(192, 51, 1),
(193, 51, 6),
(194, 52, 1),
(195, 52, 3),
(196, 52, 7),
(197, 52, 9),
(198, 53, 7),
(199, 54, 1),
(200, 54, 7),
(201, 55, 7),
(202, 56, 1),
(203, 56, 7),
(204, 57, 1),
(205, 57, 2),
(206, 57, 3),
(207, 57, 7),
(208, 58, 1),
(209, 58, 3),
(210, 58, 7),
(211, 61, 1),
(212, 63, 1),
(213, 63, 3),
(214, 63, 7),
(215, 69, 1),
(216, 69, 3),
(217, 69, 7),
(218, 70, 1),
(219, 70, 7),
(220, 71, 1),
(221, 71, 3),
(222, 71, 7),
(223, 72, 3),
(224, 72, 7),
(225, 73, 1),
(226, 73, 7),
(227, 74, 7),
(228, 75, 1),
(229, 75, 7),
(230, 76, 1),
(231, 76, 3),
(232, 76, 7),
(233, 77, 3),
(234, 77, 4),
(235, 78, 1),
(236, 78, 3),
(237, 78, 7),
(238, 79, 1),
(239, 79, 3),
(240, 79, 7),
(241, 80, 1),
(242, 80, 3),
(243, 81, 1),
(244, 81, 3),
(245, 81, 7),
(246, 82, 1),
(247, 82, 3),
(248, 82, 7),
(249, 83, 1),
(250, 83, 3),
(251, 83, 7),
(252, 84, 1),
(253, 84, 3),
(254, 84, 7),
(255, 85, 1),
(256, 85, 3),
(257, 85, 4),
(258, 85, 7),
(259, 85, 12),
(260, 86, 4),
(261, 86, 7),
(262, 86, 9),
(263, 86, 12),
(264, 87, 1),
(265, 87, 3),
(266, 87, 4),
(267, 87, 7),
(268, 87, 9),
(269, 87, 10),
(270, 88, 1),
(271, 88, 4),
(272, 88, 7),
(273, 89, 1),
(274, 89, 4),
(275, 89, 7),
(276, 90, 1),
(277, 90, 3),
(278, 90, 4),
(279, 90, 7),
(280, 91, 1),
(281, 91, 4),
(282, 91, 7),
(287, 93, 1),
(288, 93, 6),
(289, 93, 7),
(290, 95, 1),
(291, 96, 1),
(292, 97, 1),
(293, 98, 1),
(294, 99, 1),
(295, 99, 3),
(296, 100, 1),
(297, 100, 6),
(298, 101, 1),
(299, 102, 1),
(300, 102, 3),
(301, 103, 1),
(302, 103, 3),
(303, 104, 1),
(304, 104, 3),
(305, 104, 7),
(306, 105, 1),
(307, 105, 3),
(308, 105, 7),
(309, 106, 3),
(310, 106, 7),
(311, 107, 1),
(312, 107, 3),
(313, 107, 7),
(314, 108, 7),
(315, 109, 1),
(316, 109, 3),
(317, 109, 7),
(318, 110, 1),
(319, 110, 3),
(320, 110, 7),
(321, 111, 1),
(322, 111, 3),
(323, 111, 7),
(324, 123, 7),
(325, 124, 7),
(326, 125, 7),
(327, 126, 1),
(328, 126, 3),
(329, 126, 7),
(330, 127, 1),
(331, 127, 3),
(332, 127, 7),
(333, 128, 3),
(334, 128, 9),
(335, 128, 12),
(336, 129, 1),
(337, 129, 6),
(338, 129, 7),
(342, 130, 1),
(343, 130, 9),
(344, 132, 7),
(345, 132, 9),
(346, 133, 1),
(347, 134, 1),
(348, 134, 4),
(349, 134, 7),
(350, 134, 9),
(351, 135, 9),
(352, 136, 9),
(353, 137, 1),
(354, 137, 4),
(355, 137, 9),
(356, 137, 12),
(357, 138, 1),
(358, 138, 3),
(359, 138, 7),
(360, 138, 9),
(361, 139, 1),
(362, 139, 3),
(363, 139, 9),
(364, 140, 3),
(365, 140, 9),
(366, 141, 1),
(367, 141, 3),
(368, 141, 6),
(369, 141, 9),
(370, 142, 1),
(371, 142, 9),
(372, 143, 1),
(373, 143, 3),
(374, 143, 9),
(375, 144, 7),
(376, 145, 1),
(377, 146, 1),
(378, 146, 9),
(379, 147, 1),
(380, 149, 1),
(381, 149, 7),
(382, 153, 9),
(383, 154, 3),
(384, 155, 9),
(385, 131, 7),
(386, 157, 9),
(387, 158, 9),
(388, 160, 9),
(389, 162, 9),
(390, 168, 9),
(391, 169, 9),
(393, 171, 1),
(394, 170, 1),
(395, 172, 1),
(396, 172, 4),
(397, 173, 1),
(398, 173, 7),
(399, 174, 1),
(400, 174, 7),
(402, 176, 7),
(403, 177, 7),
(415, 175, 7),
(430, 185, 1),
(431, 185, 7),
(432, 188, 1),
(433, 188, 7),
(434, 189, 1),
(435, 189, 4),
(436, 189, 6),
(437, 189, 7),
(438, 190, 1),
(439, 190, 7),
(440, 191, 1),
(441, 191, 7),
(442, 192, 1),
(443, 192, 7),
(444, 193, 1),
(445, 193, 7),
(446, 193, 9),
(447, 194, 1),
(448, 194, 3),
(449, 194, 7),
(450, 195, 1),
(451, 195, 7),
(452, 196, 1),
(453, 196, 3),
(454, 196, 7),
(469, 181, 1);

-- --------------------------------------------------------

--
-- Table structure for table `relation_dish_basket`
--

CREATE TABLE `relation_dish_basket` (
  `id` int(11) UNSIGNED NOT NULL,
  `dish_id` int(11) UNSIGNED NOT NULL,
  `basket_id` int(11) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `relation_dish_basket`
--

INSERT INTO `relation_dish_basket` (`id`, `dish_id`, `basket_id`) VALUES
(1, 27, 1),
(2, 17, 3),
(3, 1, 4),
(4, 30, 5),
(5, 39, 13),
(6, 41, 14),
(7, 45, 15),
(8, 46, 16),
(9, 47, 17),
(10, 48, 18),
(11, 16, 19),
(12, 15, 20),
(13, 12, 21),
(14, 11, 22),
(15, 3, 23),
(16, 2, 24),
(17, 49, 25),
(18, 50, 26),
(19, 51, 27),
(20, 52, 28),
(21, 53, 29),
(22, 54, 30),
(23, 55, 31),
(24, 56, 32),
(25, 57, 33),
(26, 58, 34),
(27, 59, 35),
(28, 60, 36),
(29, 61, 37),
(30, 62, 38),
(31, 63, 39),
(32, 64, 40),
(33, 65, 41),
(34, 66, 42),
(35, 67, 43),
(36, 68, 44),
(37, 69, 45),
(38, 70, 46),
(39, 71, 47),
(40, 72, 48),
(41, 73, 49),
(42, 74, 50),
(43, 75, 51),
(44, 76, 52),
(45, 77, 53),
(46, 78, 54),
(47, 79, 55),
(48, 80, 56),
(49, 81, 57),
(50, 82, 58),
(51, 83, 59),
(52, 84, 60),
(53, 85, 61),
(54, 86, 62),
(55, 87, 63),
(56, 88, 64),
(57, 89, 65),
(58, 90, 66),
(59, 91, 67),
(60, 93, 69),
(61, 95, 71),
(62, 96, 72),
(63, 97, 73),
(64, 98, 74),
(65, 99, 75),
(66, 100, 76),
(67, 101, 77),
(68, 102, 78),
(69, 103, 79),
(70, 104, 80),
(71, 105, 81),
(72, 106, 82),
(73, 107, 83),
(74, 108, 84),
(75, 109, 85),
(76, 110, 86),
(77, 111, 87),
(78, 128, 88),
(79, 129, 89),
(80, 130, 90),
(81, 131, 91),
(82, 132, 92),
(83, 133, 93),
(84, 134, 94),
(85, 135, 95),
(86, 136, 96),
(87, 137, 97),
(88, 138, 98),
(89, 139, 99),
(90, 140, 100),
(91, 141, 101),
(92, 142, 102),
(93, 143, 103),
(94, 144, 104),
(95, 145, 105),
(96, 146, 106),
(97, 147, 107),
(98, 148, 108),
(99, 149, 109),
(100, 150, 110),
(101, 151, 111),
(102, 152, 112),
(103, 153, 113),
(104, 154, 114),
(105, 155, 115),
(106, 156, 116),
(107, 157, 117),
(108, 158, 118),
(109, 159, 119),
(110, 160, 120),
(111, 161, 121),
(112, 162, 122),
(113, 163, 123),
(114, 164, 124),
(115, 165, 125),
(116, 166, 126),
(117, 167, 127),
(118, 168, 128),
(119, 169, 129),
(120, 170, 130),
(121, 171, 131),
(122, 172, 132),
(123, 173, 133),
(124, 174, 134),
(125, 181, 135),
(132, 181, 142),
(133, 181, 143),
(134, 181, 144),
(139, 1, 149),
(140, 1, 150),
(141, 1, 151),
(142, 2, 152),
(143, 2, 153),
(144, 2, 154),
(145, 11, 155),
(146, 11, 156),
(147, 11, 157),
(148, 12, 158),
(149, 12, 159),
(150, 12, 160),
(151, 16, 161),
(152, 16, 162),
(153, 16, 163),
(154, 17, 164),
(155, 17, 165),
(156, 17, 166),
(157, 27, 167),
(158, 27, 168),
(159, 27, 169),
(160, 30, 170),
(161, 30, 171),
(162, 30, 172),
(163, 39, 173),
(164, 39, 174),
(165, 39, 175),
(166, 45, 176),
(167, 45, 177),
(168, 45, 178),
(169, 47, 179),
(170, 47, 180),
(171, 47, 181),
(172, 48, 182),
(173, 48, 183),
(174, 48, 184),
(175, 49, 185),
(176, 49, 186),
(177, 49, 187),
(178, 50, 188),
(179, 50, 189),
(180, 50, 190),
(181, 51, 191),
(182, 51, 192),
(183, 51, 193),
(184, 52, 194),
(185, 52, 195),
(186, 52, 196),
(187, 53, 197),
(188, 53, 198),
(189, 53, 199),
(190, 54, 200),
(191, 54, 201),
(192, 54, 202),
(193, 55, 203),
(194, 55, 204),
(195, 55, 205),
(196, 56, 206),
(197, 56, 207),
(198, 56, 208),
(199, 57, 209),
(200, 57, 210),
(201, 57, 211),
(202, 58, 212),
(203, 58, 213),
(204, 58, 214),
(205, 59, 215),
(206, 59, 216),
(207, 59, 217),
(208, 60, 218),
(209, 60, 219),
(210, 60, 220),
(211, 61, 221),
(212, 61, 222),
(213, 61, 223),
(214, 62, 224),
(215, 62, 225),
(216, 62, 226),
(217, 63, 227),
(218, 63, 228),
(219, 63, 229),
(220, 64, 230),
(221, 64, 231),
(222, 64, 232),
(223, 65, 233),
(224, 65, 234),
(225, 65, 235),
(226, 66, 236),
(227, 66, 237),
(228, 66, 238),
(229, 67, 239),
(230, 67, 240),
(231, 67, 241),
(232, 68, 242),
(233, 68, 243),
(234, 68, 244),
(235, 69, 245),
(236, 69, 246),
(237, 69, 247),
(238, 70, 248),
(239, 70, 249),
(240, 70, 250),
(241, 71, 251),
(242, 71, 252),
(243, 71, 253),
(244, 72, 254),
(245, 72, 255),
(246, 72, 256),
(247, 73, 257),
(248, 73, 258),
(249, 73, 259),
(250, 74, 260),
(251, 74, 261),
(252, 74, 262),
(253, 75, 263),
(254, 75, 264),
(255, 75, 265),
(256, 76, 266),
(257, 76, 267),
(258, 76, 268),
(259, 77, 269),
(260, 77, 270),
(261, 77, 271),
(262, 78, 272),
(263, 78, 273),
(264, 78, 274),
(265, 79, 275),
(266, 79, 276),
(267, 79, 277),
(268, 80, 278),
(269, 80, 279),
(270, 80, 280),
(271, 81, 281),
(272, 81, 282),
(273, 81, 283),
(274, 82, 284),
(275, 82, 285),
(276, 82, 286),
(277, 83, 287),
(278, 83, 288),
(279, 83, 289),
(280, 84, 290),
(281, 84, 291),
(282, 84, 292),
(283, 85, 293),
(284, 85, 294),
(285, 85, 295),
(286, 86, 296),
(287, 86, 297),
(288, 86, 298),
(289, 87, 299),
(290, 87, 300),
(291, 87, 301),
(292, 88, 302),
(293, 88, 303),
(294, 88, 304),
(295, 89, 305),
(296, 89, 306),
(297, 89, 307),
(298, 90, 308),
(299, 90, 309),
(300, 90, 310),
(301, 91, 311),
(302, 91, 312),
(303, 91, 313),
(304, 93, 314),
(305, 93, 315),
(306, 93, 316),
(307, 95, 317),
(308, 95, 318),
(309, 95, 319),
(310, 96, 320),
(311, 96, 321),
(312, 96, 322),
(313, 97, 323),
(314, 97, 324),
(315, 97, 325),
(316, 98, 326),
(317, 98, 327),
(318, 98, 328),
(319, 99, 329),
(320, 99, 330),
(321, 99, 331),
(322, 100, 332),
(323, 100, 333),
(324, 100, 334),
(325, 101, 335),
(326, 101, 336),
(327, 101, 337),
(328, 102, 338),
(329, 102, 339),
(330, 102, 340),
(331, 103, 341),
(332, 103, 342),
(333, 103, 343),
(334, 104, 344),
(335, 104, 345),
(336, 104, 346),
(337, 105, 347),
(338, 105, 348),
(339, 105, 349),
(340, 106, 350),
(341, 106, 351),
(342, 106, 352),
(343, 107, 353),
(344, 107, 354),
(345, 107, 355),
(346, 108, 356),
(347, 108, 357),
(348, 108, 358),
(349, 109, 359),
(350, 109, 360),
(351, 109, 361),
(352, 110, 362),
(353, 110, 363),
(354, 110, 364),
(355, 111, 365),
(356, 111, 366),
(357, 111, 367),
(358, 181, 368),
(359, 181, 369),
(360, 181, 370),
(361, 3, 371),
(362, 3, 372),
(363, 3, 373),
(364, 15, 374),
(365, 15, 375),
(366, 15, 376),
(367, 41, 377),
(368, 41, 378),
(369, 41, 379),
(370, 46, 380),
(371, 46, 381),
(372, 46, 382),
(373, 128, 383),
(374, 128, 384),
(375, 128, 385),
(376, 129, 386),
(377, 129, 387),
(378, 129, 388),
(379, 130, 389),
(380, 130, 390),
(381, 130, 391),
(382, 131, 392),
(383, 131, 393),
(384, 131, 394),
(385, 132, 395),
(386, 132, 396),
(387, 132, 397),
(388, 133, 398),
(389, 133, 399),
(390, 133, 400),
(391, 134, 401),
(392, 134, 402),
(393, 134, 403),
(394, 135, 404),
(395, 135, 405),
(396, 135, 406),
(397, 136, 407),
(398, 136, 408),
(399, 136, 409),
(400, 137, 410),
(401, 137, 411),
(402, 137, 412),
(403, 138, 413),
(404, 138, 414),
(405, 138, 415),
(406, 139, 416),
(407, 139, 417),
(408, 139, 418),
(409, 140, 419),
(410, 140, 420),
(411, 140, 421),
(412, 141, 422),
(413, 141, 423),
(414, 141, 424),
(415, 142, 425),
(416, 142, 426),
(417, 142, 427),
(418, 143, 428),
(419, 143, 429),
(420, 143, 430),
(421, 144, 431),
(422, 144, 432),
(423, 144, 433),
(424, 145, 434),
(425, 145, 435),
(426, 145, 436),
(427, 146, 437),
(428, 146, 438),
(429, 146, 439),
(430, 147, 440),
(431, 147, 441),
(432, 147, 442),
(433, 148, 443),
(434, 148, 444),
(435, 148, 445),
(436, 149, 446),
(437, 149, 447),
(438, 149, 448),
(439, 150, 449),
(440, 150, 450),
(441, 150, 451),
(442, 151, 452),
(443, 151, 453),
(444, 151, 454),
(445, 152, 455),
(446, 152, 456),
(447, 152, 457),
(448, 153, 458),
(449, 153, 459),
(450, 153, 460),
(451, 154, 461),
(452, 154, 462),
(453, 154, 463),
(454, 155, 464),
(455, 155, 465),
(456, 155, 466),
(457, 156, 467),
(458, 156, 468),
(459, 156, 469),
(460, 157, 470),
(461, 157, 471),
(462, 157, 472),
(463, 158, 473),
(464, 158, 474),
(465, 158, 475),
(466, 159, 476),
(467, 159, 477),
(468, 159, 478),
(469, 160, 479),
(470, 160, 480),
(471, 160, 481),
(472, 161, 482),
(473, 161, 483),
(474, 161, 484),
(475, 162, 485),
(476, 162, 486),
(477, 162, 487),
(478, 163, 488),
(479, 163, 489),
(480, 163, 490),
(481, 164, 491),
(482, 164, 492),
(483, 164, 493),
(484, 165, 494),
(485, 165, 495),
(486, 165, 496),
(487, 166, 497),
(488, 166, 498),
(489, 166, 499),
(490, 167, 500),
(491, 167, 501),
(492, 167, 502),
(493, 168, 503),
(494, 168, 504),
(495, 168, 505),
(496, 169, 506),
(497, 169, 507),
(498, 169, 508),
(499, 170, 509),
(500, 170, 510),
(501, 170, 511),
(502, 171, 512),
(503, 171, 513),
(504, 171, 514),
(505, 172, 515),
(506, 172, 516),
(507, 172, 517),
(508, 173, 518),
(509, 173, 519),
(510, 173, 520),
(511, 174, 521),
(512, 174, 522),
(513, 174, 523);

-- --------------------------------------------------------

--
-- Table structure for table `relation_dish_menu_day`
--

CREATE TABLE `relation_dish_menu_day` (
  `id` int(10) UNSIGNED NOT NULL,
  `dish_id` int(10) UNSIGNED DEFAULT NULL,
  `menu_day_id` int(10) UNSIGNED NOT NULL,
  `is_main2` tinyint(1) NOT NULL DEFAULT 0,
  `is_snack1` tinyint(1) NOT NULL DEFAULT 0,
  `is_snack2` tinyint(1) NOT NULL DEFAULT 0,
  `age_group` enum('3-6 let','7-10 let','11-14 let','15-18 let','others') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `relation_menu_day_menu_week`
--

CREATE TABLE `relation_menu_day_menu_week` (
  `id` int(10) UNSIGNED NOT NULL,
  `menu_day_id` int(10) UNSIGNED NOT NULL,
  `menu_week_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `settings`
--

CREATE TABLE `settings` (
  `id` int(1) UNSIGNED NOT NULL,
  `school_name` varchar(255) DEFAULT NULL,
  `school_url` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `phone` varchar(255) DEFAULT NULL,
  `supervisor` varchar(255) DEFAULT NULL,
  `cookers` varchar(255) DEFAULT NULL,
  `cookbook_url` varchar(255) DEFAULT NULL,
  `closed` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL,
  `age_group` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL,
  `menu_actived_at_day` varchar(255) NOT NULL DEFAULT 'Friday',
  `menu_actived_at_time` varchar(255) NOT NULL DEFAULT '16:00',
  `has_two_main_dishes` tinyint(1) NOT NULL DEFAULT 0,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `settings`
--

INSERT INTO `settings` (`id`, `school_name`, `school_url`, `email`, `phone`, `supervisor`, `cookers`, `cookbook_url`, `closed`, `age_group`, `menu_actived_at_day`, `menu_actived_at_time`, `has_two_main_dishes`, `created_at`, `updated_at`) VALUES
(1, 'Název školy', 'http://adamreznicek.eu/', 'jidelna@nazev-skoly.cz', '+420 123 456 789', 'Adam Řezníček', '', 'https://www.jidelny.cz/recepty.aspx', '[]', '[1,2,3,4,5]', 'Friday', '14:00', 1, '2020-06-12 10:43:13', '2020-12-06 18:22:29');

-- --------------------------------------------------------

--
-- Table structure for table `side`
--

CREATE TABLE `side` (
  `id` smallint(5) UNSIGNED NOT NULL,
  `name` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `side`
--

INSERT INTO `side` (`id`, `name`) VALUES
(1, 'Knedlík'),
(2, 'Brambor'),
(3, 'Obiloviny (rýže, těstoviny atp.)'),
(4, 'Ostatní'),
(5, 'Pečivo'),
(6, 'Zelenina');

-- --------------------------------------------------------

--
-- Table structure for table `type`
--

CREATE TABLE `type` (
  `id` smallint(11) UNSIGNED NOT NULL,
  `name` varchar(255) NOT NULL,
  `has_side` tinyint(1) DEFAULT 0,
  `has_basket` tinyint(1) DEFAULT 0,
  `has_category` tinyint(1) DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `type`
--

INSERT INTO `type` (`id`, `name`, `has_side`, `has_basket`, `has_category`) VALUES
(1, 'Hlavní chod', 1, 1, 1),
(2, 'Polévka', 0, 1, 1),
(3, 'Nápoj', 0, 0, 1),
(5, 'Salát', 0, 0, 0),
(6, 'Moučník/dezert', 0, 0, 0),
(7, 'Ostatní', 0, 0, 1);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `allergen`
--
ALTER TABLE `allergen`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `number` (`number`);

--
-- Indexes for table `basket`
--
ALTER TABLE `basket`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `category`
--
ALTER TABLE `category`
  ADD PRIMARY KEY (`id`),
  ADD KEY `type_id` (`type_id`);

--
-- Indexes for table `dish`
--
ALTER TABLE `dish`
  ADD PRIMARY KEY (`id`),
  ADD KEY `name` (`name`),
  ADD KEY `type_id` (`type_id`),
  ADD KEY `category_id` (`category_id`),
  ADD KEY `side_id` (`side_id`);

--
-- Indexes for table `menu_day`
--
ALTER TABLE `menu_day`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `menu_week`
--
ALTER TABLE `menu_week`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `recommendations_basket`
--
ALTER TABLE `recommendations_basket`
  ADD PRIMARY KEY (`id`),
  ADD KEY `type_id` (`type_id`);

--
-- Indexes for table `recommendations_nutri`
--
ALTER TABLE `recommendations_nutri`
  ADD PRIMARY KEY (`id`),
  ADD KEY `type_id` (`type_id`),
  ADD KEY `category_id` (`category_id`);

--
-- Indexes for table `relation_dish_allergen`
--
ALTER TABLE `relation_dish_allergen`
  ADD PRIMARY KEY (`id`),
  ADD KEY `dish_id` (`dish_id`),
  ADD KEY `allergen_id` (`allergen_id`);

--
-- Indexes for table `relation_dish_basket`
--
ALTER TABLE `relation_dish_basket`
  ADD PRIMARY KEY (`id`),
  ADD KEY `dish_id` (`dish_id`),
  ADD KEY `basket_id` (`basket_id`);

--
-- Indexes for table `relation_dish_menu_day`
--
ALTER TABLE `relation_dish_menu_day`
  ADD PRIMARY KEY (`id`),
  ADD KEY `dish_id` (`dish_id`),
  ADD KEY `menu_day_id` (`menu_day_id`);

--
-- Indexes for table `relation_menu_day_menu_week`
--
ALTER TABLE `relation_menu_day_menu_week`
  ADD PRIMARY KEY (`id`),
  ADD KEY `menu_day_id` (`menu_day_id`),
  ADD KEY `menu_week_id` (`menu_week_id`);

--
-- Indexes for table `settings`
--
ALTER TABLE `settings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `side`
--
ALTER TABLE `side`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `type`
--
ALTER TABLE `type`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `allergen`
--
ALTER TABLE `allergen`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `basket`
--
ALTER TABLE `basket`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=524;

--
-- AUTO_INCREMENT for table `category`
--
ALTER TABLE `category`
  MODIFY `id` smallint(5) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=41;

--
-- AUTO_INCREMENT for table `dish`
--
ALTER TABLE `dish`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=201;

--
-- AUTO_INCREMENT for table `menu_day`
--
ALTER TABLE `menu_day`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `menu_week`
--
ALTER TABLE `menu_week`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `recommendations_basket`
--
ALTER TABLE `recommendations_basket`
  MODIFY `id` smallint(5) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=65;

--
-- AUTO_INCREMENT for table `recommendations_nutri`
--
ALTER TABLE `recommendations_nutri`
  MODIFY `id` smallint(5) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=33;

--
-- AUTO_INCREMENT for table `relation_dish_allergen`
--
ALTER TABLE `relation_dish_allergen`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=470;

--
-- AUTO_INCREMENT for table `relation_dish_basket`
--
ALTER TABLE `relation_dish_basket`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=514;

--
-- AUTO_INCREMENT for table `relation_dish_menu_day`
--
ALTER TABLE `relation_dish_menu_day`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `relation_menu_day_menu_week`
--
ALTER TABLE `relation_menu_day_menu_week`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `settings`
--
ALTER TABLE `settings`
  MODIFY `id` int(1) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `side`
--
ALTER TABLE `side`
  MODIFY `id` smallint(5) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `type`
--
ALTER TABLE `type`
  MODIFY `id` smallint(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `category`
--
ALTER TABLE `category`
  ADD CONSTRAINT `category_ibfk_1` FOREIGN KEY (`type_id`) REFERENCES `type` (`id`);

--
-- Constraints for table `dish`
--
ALTER TABLE `dish`
  ADD CONSTRAINT `dish_ibfk_2` FOREIGN KEY (`type_id`) REFERENCES `type` (`id`),
  ADD CONSTRAINT `dish_ibfk_3` FOREIGN KEY (`category_id`) REFERENCES `category` (`id`),
  ADD CONSTRAINT `dish_ibfk_4` FOREIGN KEY (`side_id`) REFERENCES `side` (`id`);

--
-- Constraints for table `recommendations_basket`
--
ALTER TABLE `recommendations_basket`
  ADD CONSTRAINT `recommendations_basket_ibfk_1` FOREIGN KEY (`type_id`) REFERENCES `type` (`id`);

--
-- Constraints for table `recommendations_nutri`
--
ALTER TABLE `recommendations_nutri`
  ADD CONSTRAINT `recommendations_nutri_ibfk_1` FOREIGN KEY (`type_id`) REFERENCES `type` (`id`),
  ADD CONSTRAINT `recommendations_nutri_ibfk_2` FOREIGN KEY (`category_id`) REFERENCES `category` (`id`);

--
-- Constraints for table `relation_dish_allergen`
--
ALTER TABLE `relation_dish_allergen`
  ADD CONSTRAINT `relation_dish_allergen_ibfk_1` FOREIGN KEY (`dish_id`) REFERENCES `dish` (`id`),
  ADD CONSTRAINT `relation_dish_allergen_ibfk_2` FOREIGN KEY (`allergen_id`) REFERENCES `allergen` (`id`);

--
-- Constraints for table `relation_dish_basket`
--
ALTER TABLE `relation_dish_basket`
  ADD CONSTRAINT `relation_dish_basket_ibfk_1` FOREIGN KEY (`dish_id`) REFERENCES `dish` (`id`),
  ADD CONSTRAINT `relation_dish_basket_ibfk_2` FOREIGN KEY (`basket_id`) REFERENCES `basket` (`id`);

--
-- Constraints for table `relation_dish_menu_day`
--
ALTER TABLE `relation_dish_menu_day`
  ADD CONSTRAINT `relation_dish_menu_day_ibfk_1` FOREIGN KEY (`dish_id`) REFERENCES `dish` (`id`),
  ADD CONSTRAINT `relation_dish_menu_day_ibfk_2` FOREIGN KEY (`menu_day_id`) REFERENCES `menu_day` (`id`);

--
-- Constraints for table `relation_menu_day_menu_week`
--
ALTER TABLE `relation_menu_day_menu_week`
  ADD CONSTRAINT `relation_menu_day_menu_week_ibfk_1` FOREIGN KEY (`menu_day_id`) REFERENCES `menu_day` (`id`),
  ADD CONSTRAINT `relation_menu_day_menu_week_ibfk_2` FOREIGN KEY (`menu_week_id`) REFERENCES `menu_week` (`id`);
SET FOREIGN_KEY_CHECKS=1;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
