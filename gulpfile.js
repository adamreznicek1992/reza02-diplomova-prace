/*global -$ */
'use strict';

const settingsFile = require('./gulp/settings');
// Settings
const settings = {
  static: false,
};

// Paths
const appDevDir = 'app';
const templateDevDir = 'www';
const distDir = 'dist';

const cssDevDir = templateDevDir + '/styles';
const cssDistDir = distDir + '/styles';
const jsDevDir = templateDevDir + '/scripts';
const jsDistDir = distDir + '/scripts';
const imgDevDir = templateDevDir + '/images';
const fontsDevDir = templateDevDir + '/fonts';
const fontsDistDir = distDir + '/fonts';
const dataDevDir = templateDevDir + '/datas';

// generated on 2015-12-13 using generator-leswigul 0.3.3
var gulp = require('gulp');
var $ = require('gulp-load-plugins')();
var inlineimg = require('gulp-inline-image-html');
var open = require('open');

// includes
const server = require('./gulp/server');
const styles = require('./gulp/styles');
const scripts = require('./gulp/scripts');

gulp.task('images', function () {
  return gulp.src(imgDevDir + '/**/*')
    // .pipe($.cache($.imagemin({
    //   progressive: true,
    //   interlaced: true,
    //   // don't remove IDs from SVGs, they are often used
    //   // as hooks for embedding and styling
    //   svgoPlugins: [{cleanupIDs: false}],
    //   use: [pngquant({quality: '65-80', speed: 4})]
    // })))
    .pipe(gulp.dest(distDir + '/' + imgDevDir));
});

gulp.task('fonts', function () {
  // others
  gulp.src([
    fontsDevDir + '/**/*',
  ], {
    dot: true
  }).pipe(gulp.dest(fontsDistDir));
});


gulp.task('extras', function () {
  // others
  gulp.src([
    templateDevDir + '/*',
  ], {
    dot: true
  }).pipe(gulp.dest(distDir));
});

gulp.task('clean', require('del').bind(null, [distDir]));

gulp.task('watch', ['connect', 'watchify'],  function(){
  
  // Watch for changes - auto reload
  gulp.watch([
    appDevDir + '/modules/**/*.latte',
    appDevDir + '/modules/**/*.php',
    cssDevDir + '/**/*.css',
    imgDevDir + '/**/*',
  ], function (event) {
    return gulp.src(event.path)
      .pipe($.connect.reload());
  });

  // Watch for changes - build CSS
  gulp.watch(cssDevDir + '/**/*.scss', ['styles']);
});



gulp.task('build', ['styles', 'buildScripts', 'images', 'fonts', 'extras']);

gulp.task('default', ['clean'], function () {
  gulp.start('build');
});
