Diplomová práce - Systém pro tvorbu jídelníčků ve školní jídelně
=========

Aplikace vznikla jako diplomová práce na Vysoké škole ekonomické v Praze.

Tato práce se zabývá vývojem webové aplikace, která si klade za cíl usnadnit zaměstnancům
stravovacích zařízení práci spojenou s tvorbou jídelníčku a také zvýšit atraktivitu nabízených
jídel pro samotné strávníky.


Jak začít vyvíjet?
--------

#### Instalace dev-stack:

	composer install                (nainstaluje balíčky vendors)
    npm install nebo yarn install   (nainstaluje balíčky node_modules)
    gulp                            (pro počáteční sestavení stylů a skriptů)
    
Startovní databáze je v adresáři "db", který naimportujte do svého lokálního databázového stroje a přístupy nastavte v souboru app/config/local.neon.

Přístupy do administračního rozhraní nastavíte v souboru app/config/common.neon.

#### Spuštění dev-stack:

	Spusťte PHP server 			    (instalace XAMPP)
    gulp watch                      (pro sledování změn při vývoji na localhostu)
    gulp build                      (pro minifikaci stylů a skriptů - adresář dist)